/**
 * Convert attribute to array
 * @param model
 * @param attribute
 * @returns {Array}
 */
let convertAttributeToArr = function (model, attribute) {
  let attributeArr = [];
  if (Array.isArray(model)) {
    model.map(function (item) {
      let items = item[attribute].replace(/\s/g, '').split(',');
      attributeArr = attributeArr.concat(...items);
    })
  } else {
    let items = model[attribute].replace(/\s/g, '').split(',');
    attributeArr = attributeArr.concat(...items);
  }

  return attributeArr;
}

export default convertAttributeToArr;