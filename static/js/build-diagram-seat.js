/**
 * File build-diagram-seat.js
 * Build diagram seat and handle event
 *
 * @author Rikkei.DuyN + Rikkei.ThienNB
 * @date 2018-10-10
 */


// Start color lib
const isNumberAndInRange = (input, min, max) => {
  return typeof value === 'number' && value >= min && value <= max;
};

const formatNumber = (number, format) => {
  if (format === 10) {
    return number;
  }
  else if (format === 16) {
    let output = Math.round(number).toString(16);
    if (output.length === 1) output = `0${output}`;
    return output;
  }
  else if (format === 'percent') {
    return Math.round(number / 255 * 1000) / 1000;
  }
  else {
    throw new Error('Format is invalid.');
  }
};

const parseRGBA = (color) => {
  let r, g, b, a = 1;

  if (typeof color === 'string' && color.match(/^rgb\(\d+\s*,\s*\d+\s*,\s*\d+\s*\)$/i)) {
    const matches = color.match(/rgb\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/i)
    r = matches[1];
    g = matches[2];
    b = matches[3];
  }
  else if (typeof color === 'string' && color.match(/^rgba\(\d+\s*,\s*\d+\s*,\s*\d+\s*,\s*(0|1|0\.\d+)\s*\)$/i)) {
    const matches = color.match(/rgba\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*,\s*(0|1|0\.\d+)\s*\)/i)
    r = matches[1];
    g = matches[2];
    b = matches[3];
    a = matches[4];
  }

  if (r === undefined || g === undefined || b === undefined || a === undefined) {
    throw new Error('Not a rgba string.');
    return;
  }

  r = parseInt(r, 10);
  g = parseInt(g, 10);
  b = parseInt(b, 10);
  a = parseFloat(a) * 255;

  return {
    red: r,
    green: g,
    blue: b,
    alpha: a
  }
};

const parseHex = (color) => {
  if (!color.match(/^#([0-9a-f]{6}|[0-9a-f]{8})$/i)) {
    throw new Error('Not a hex string.');
    return;
  }

  color = color.replace(/^#/, '');

  const convert = (single) => {
    return parseInt(single, 16);
  };

  const r = convert(color.substr(0, 2));
  const g = convert(color.substr(2, 2));
  const b = convert(color.substr(4, 2));
  const a = convert(color.substr(6, 2) || 'FF');

  return {
    red: r,
    green: g,
    blue: b,
    alpha: a
  }
};

class Color {

  constructor(color) {
    this.channel = undefined;

    if (!this.channel) {
      try {
        this.channel = parseHex(color);
      } catch (e) {
      }
    }
    if (!this.channel) {
      try {
        this.channel = parseRGBA(color);
      } catch (e) {
      }
    }

    if (!this.channel) {
      throw new Error("Can't parse color.");
    }
  }

  setRed(value) {
    if (!isNumberAndInRange(value, 0, 255)) {
      throw new Error('Please pass in a number between 0 ~ 255.');
    }

    this.channel.red = parseInt(value, 10);
  }

  setGreen(value) {
    if (!isNumberAndInRange(value, 0, 255)) {
      throw new Error('Please pass in a number between 0 ~ 255.');
    }

    this.channel.green = parseInt(value, 10);
  }

  setBlue(value) {
    if (!isNumberAndInRange(value, 0, 255)) {
      throw new Error('Please pass in a number between 0 ~ 255.');
    }

    this.channel.blue = parseInt(value, 10);
  }

  setAlpha(value) {
    if (!isNumberAndInRange(value, 0, 1)) {
      throw new Error('Please pass in a number between 0 ~ 1.');
    }

    this.channel.alpha = parseInt(value * 255, 10);
  }

  getRed(format = 10) {
    return formatNumber(this.channel.red, format);
  }

  getGreen(format = 10) {
    return formatNumber(this.channel.green, format);
  }

  getBlue(format = 10) {
    return formatNumber(this.channel.blue, format);
  }

  getAlpha(format = 'percent') {
    return formatNumber(this.channel.alpha, format);
  }

  toFormat(format) {
    if (!typeof format === 'string') {
      throw new Error('Format must be a string.');
      return;
    }

    return format
        .replace(/\$r/i, this.getRed())
        .replace(/\$g/i, this.getGreen())
        .replace(/\$b/i, this.getBlue())
        .replace(/\$a/i, this.getAlpha())
        .replace(/\$0xR/, this.getRed(16).toUpperCase())
        .replace(/\$0xr/, this.getRed(16).toLowerCase())
        .replace(/\$0xG/, this.getGreen(16).toUpperCase())
        .replace(/\$0xg/, this.getGreen(16).toLowerCase())
        .replace(/\$0xB/, this.getBlue(16).toUpperCase())
        .replace(/\$0xb/, this.getBlue(16).toLowerCase())
        .replace(/\$0xA/, this.getAlpha(16).toUpperCase())
        .replace(/\$0xa/, this.getAlpha(16).toLowerCase());
  }

  toRGB() {
    return this.toFormat('rgb($r, $g, $b)');
  }

  toRGBA() {
    return this.toFormat('rgba($r, $g, $b, $a)');
  }

  toHex() {
    return this.toFormat('#$0xR$0xG$0xB');
  }

  toHexA() {
    return this.toFormat('#$0xR$0xG$0xB$0xA');
  }

}
//end Color lib

var matrix = {};

/**
 * custom event to scale with two fingers in smart phone
 * @type {{haltEventListeners: string[], init: eventsHandler.init, destroy: eventsHandler.destroy}}
 */
var eventsHandler = {
  haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel']
  , init: function (options) {
    var instance = options.instance
        , initialScale = 1
        , pannedX = 0
        , pannedY = 0;

    // Init Hammer
    // Listen only for pointer and touch events
    this.hammer = Hammer(options.svgElement, {
      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
    });

    // Enable pinch
    this.hammer.get('pinch').set({enable: true});

    // Handle double tap
    this.hammer.on('doubletap', function (ev) {
      instance.zoomIn()
    });

    // Handle pan
    this.hammer.on('panstart panmove', function (ev) {
      // On pan start reset panned variables
      if (ev.type === 'panstart') {
        pannedX = 0;
        pannedY = 0;
      }

      // Pan only the difference
      instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY});
      pannedX = ev.deltaX;
      pannedY = ev.deltaY;
    });

    // Handle pinch
    this.hammer.on('pinchstart pinchmove', function (ev) {
      // On pinch start remember initial zoom
      if (ev.type === 'pinchstart') {
        initialScale = instance.getZoom();
        instance.zoom(initialScale * ev.scale)
      }

      instance.zoom(initialScale * ev.scale)

    });
    // Prevent moving the page on some devices when panning over SVG
    options.svgElement.addEventListener('touchmove', function (e) {
      e.preventDefault();
    });

    // Check if scroll change var isScroll
    options.svgElement.addEventListener('wheel', function (e) {
      isScroll = true;
    });

    /**
     * check if key down Ctrl
     * @param e
     */
    window.addEventListener('keydown', function (e) {
      if (e.ctrlKey) {
        isCtrl = true;
      } else {
        isCtrl = false;
      }
    }, false);

    window.addEventListener('keyup', function () {
      isCtrl = false;
    }, false);

    // //TODO: right click zoom
    // window.oncontextmenu = function(e) {
    //   e.preventDefault();
    // };
    //
    // window.onmousedown = function(e) {
    //   if(e.which == 3) {
    //     isRightClick = true;
    //   }
    // };
    //
    // window.onmouseup = function(e) {
    //   isRightClick = false;
    // };
    //
    // Setup isScrolling variable
    var isScrolling;

    // Listen for scroll events when not ctrl
    window.addEventListener('wheel', function (event) {
      if (isCtrl) {
        return;
      }

      $('#DiagramDetail').addClass('add');
      // Clear our timeout throughout the scroll
      window.clearTimeout(isScrolling);

      // Set a timeout to run after scrolling ends
      isScrolling = setTimeout(function () {
        $('#DiagramDetail').removeClass('add');
      }, 66);
    }, false);

    $('#DiagramDetail').click();
  }
  , destroy: function () {
    this.hammer.destroy()
  }
};

var isScroll = false;
var isRightClick = false;
var isCtrl = false;

/**
 * config object of panzoom
 * @type Object
 * fit: boolean
 * center: boolean
 * minZoom: number
 * maxZoom: number
 * zoomScaleSensitivity: number
 * controlIconsEnabled: boolean
 * contain: boolean
 * ustomEventsHandler: {haltEventListeners: string[], init: eventsHandler.init, destroy: eventsHandler.destroy}
 * beforeZoom: eventScrollWithKey
 */
const configPanzoom = {
  fit: true,
  center: true,
  minZoom: 1,
  maxZoom: 5.5,
  zoomScaleSensitivity: 0.5,
  controlIconsEnabled: false,
  dblClickZoomEnabled: false,
  contain: true,
  customEventsHandler: eventsHandler
};

/**
 * default seat
 */
const seat = {
  id: 0,
  x: 0,
  y: 0,
  status: "available",
  color: {
    available: "#ffffff",
    unavailable: "#D1D1D1",
    selected: "#00008D"
  },
  angle: 0,
  size: 9,
  border: {
    width: 1.5,
    color: {
      available: "#000000",
      unavailable: "#8080FF",
      selected: "#000000"
    }
  },
  number: {
    key: "1",
    size: 7,
    x: 3,
    y: 8,
    color: {
      available: "#000000",
      unavailable: "#fafa11",
      selected: "#ffffff"
    }
  },
  seat_nm: '',
  seat_type_nm: '',
  seat_type_no: 0,
  number_specified_flg: 0
};
var panZoomInstance;

/**
 * template html for seat
 * @param seat
 * @returns {string}
 */
function templateRect(seat) {

  seat.color = {
    available: new Color(seat.color.available).toRGB(),
    selected: new Color(seat.color.selected).toRGB(),
    unavailable: new Color(seat.color.unavailable).toRGB(),
  };
  seat.border.color = {
    available: new Color(seat.border.color.available).toRGB(),
    selected: new Color(seat.border.color.selected).toRGB(),
    unavailable: new Color(seat.border.color.unavailable).toRGB(),
  };
  let className = 'designate-seat';
  let seatName = seat.seat_nm.match(/[(.*?)列(\s+)](\d+)[版]/)[1];

  if (seat.number_specified_flg == 1) {
    className = 'free-seat';
  }
  return '<g data-id="' + seat.id + '">' +
      '<rect x="' + seat.x + '" y="' + seat.y + '" ' +
      'width="' + seat.size + '" height="' + seat.size + '" ' +
      'stroke="' + seat.border.color[seat.status] + '" ' + 'stroke-width="' + seat.border.width + '" ' +
      'fill="' + seat.color[seat.status] + '" ' +
      'transform="rotate(' + -seat.angle + ", " + (seat.x) + ", " + (seat.y) + ')" ' +
      'data-seat_no="' + seat.number.key + '" ' +
      'class="' + className + '">' +
      '</rect>' +
      '<text alignment-baseline="middle" text-anchor="middle"' +
      'class="' + className + '"' +
      'x="' + (seat.x + seat.size / 2) + '" y="' + (seat.y + seat.size / 2) + '" ' +
      'font-family="" ' + 'font-size="' + seat.number.size + '" ' +
      'fill="' + seat.number.color[seat.status] + '" ' +
      'style="cursor: default;" ' +
      '>' +
      seatName +
      '</text>' +
      '</g>';
}

/**
 * template for graphic (g)
 * @param image
 * @param matrix
 * @returns {string}
 */
function templateGraphic(image, matrix) {
  return '<svg id="diagram" width="100%" height="100%" version="1.1"><g fill="black" stroke="black" id="map-bg" transform="matrix(1, 0, 0, 1, 0, 0)"> ' +
      image +
      matrix +
      '</g></svg>';
}

/**
 * build map digram svg from data
 * @param seatList
 * @returns {string}
 */
function buildMapMatrix(seatList) {
  let matrixDiagram = '';
  matrix.map(function (seat_info, key) {
    let seat_tmp = Object.assign([], seat);
    seat_tmp.number.key = seat_info.seat_no;
    Object.keys(sample).map(function (sample_key) {
      if (sample_key == 'status') {
        // if seat have sale flag and seat designated
        if (seat_info[sample[sample_key]] == 1 && seat_info['number_specified_flg'] == 0) {
          seat_tmp[sample_key] = 'available';
        } else {
          seat_tmp[sample_key] = 'unavailable';
        }

      } else if (sample_key == 'color') {
        seat_tmp[sample_key] = {
          available: seat_info[sample[sample_key]],
          selected: seat.color.selected,
          unavailable: seat.color.unavailable
        };
        seat_tmp.border.color = {
          available: seat.border.color.available,
          selected: seat.border.color.selected,
          unavailable: seat_info[sample[sample_key]]
        };
      } else {
        seat_tmp[sample_key] = seat_info[sample[sample_key]];
      }
    });
    seat_tmp.id = key;
    seatList.push(seat_tmp);
    matrixDiagram += templateRect(seat_tmp);
  });
  return matrixDiagram;
}

/**
 * add event to seat
 */
function event(seatList) {

  $(document).off('click touchend', 'rect,text').on('click touchend', 'rect,text', function () {
    let id = $(this).parent().attr('data-id');
    if (seatList[id].status == 'available') {
      $(this).parent().children('rect').attr('fill', seatList[id].color.selected);
      // $(this).parent().children('rect').attr('stroke', seatList[id].color.available);
      seatList[id].status = 'selected';
    } else if (seatList[id].status == 'selected') {
      $(this).parent().children('rect').attr('fill', seatList[id].color.available);
      // $(this).parent().children('rect').attr('stroke', seatList[id].border.color.available);
      seatList[id].status = 'available';
    }
    $('#select-seat').val(JSON.stringify(getAllSelectedSeat(seatList))).trigger("change");
    // localStorage.setItem('selectSeat', JSON.stringify(getAllSelectedSeat(seatList) ));
  });
  // $(document).on('click', 'text', function () {
  //     $(this).parent().children('rect').click();
  // });
  // message in click on free seat
  $(document).off('click touchend', '.free-seat').on('click touchend', '.free-seat', function () {

    alert('枚数選択をしてください。');
  });

  $(document).off('load-select-seat').on('load-select-seat', function () {

    let selectedSeat = JSON.parse($('#select-seat').val());
    selectedSeat.forEach(function (itemSeat) {
      changeStatusSeat(seatList, itemSeat.seat_no, 'selected');
    });

  });
  $(document).off('click touchend', '.btn-cancel-all').on('click touchend', '.btn-cancel-all', function () {
    let confirmBox = confirm("ホントに？");
    if (confirmBox == true) {
      removeAllSelectedSeat(seatList);
      $('#select-seat').val(JSON.stringify(getAllSelectedSeat(seatList))).trigger("change");
    }
  });

  $(document).off('click touchend', '.btn-cancel').on('click touchend', '.btn-cancel', function (event) {

    var seatNo = 0;
    seatNo = $(event.target).attr('data-seat_no');

    let confirmBox = confirm("ホントに？");
    if (confirmBox == true) {

      changeStatusSeat(seatList, seatNo, 'available')
      // $(this).attr('fill', seatList[seatNo].color.available);
      // $(this).attr('stroke', seatList[seatNo].border.color.available);
      $('#select-seat').val(JSON.stringify(getAllSelectedSeat(seatList))).trigger("change");
    }
  });
  $('#panzoom-in').click(function () {
    isScroll = false;
    isCtrl = false;
    panZoomInstance.zoomIn();
  });
  $('#panzoom-out').click(function () {
    isScroll = false;
    isCtrl = false;
    panZoomInstance.zoomOut();
  });
  $('#panzoom-reset').click(function () {
    isScroll = false;
    isCtrl = false;
    panZoomInstance.reset();
  });
  $('#get-seat').click(function () {
    changeStatusSeat(seatList, 7, 'selected');
    console.log(JSON.stringify(getAllSelectedSeat(seatList)));
  });
  $(document).on('real-time', function (event, seatNo) {
    //delete selected seat in map seat
    changeStatusSeatSuccess(seatList, seatNo, 'unavailable');
  });
  $(document).on('real-time-delete', function (event) {
    //delete selected seat
    $('#select-seat').val(JSON.stringify(getAllSelectedSeat(seatList))).trigger("change");
  });

  $(document).on('real-time-alert', function (event, seatNo) {
    // alert seat allready book by other
    $('#select-seat').val(JSON.stringify(getAllSelectedSeat(seatList))).trigger("change");
    let listNoSeat = JSON.parse($('#select-seat').val());
    listNoSeat.forEach((ele) => {
      if (ele.seat_no == seatNo) {
        alert(ele.seat_nm + "のお席は確保できませんでした。");
      }
    });
  });
}

/**
 * get seat no
 * @param idSeat
 * @returns {*}
 */
function getSeatNo(idSeat) {
  return matrix[idSeat].seat_nm;
}

/**
 * get all selected seat
 * @param seatList
 * @returns {{seat_no: *, status: string}[]}
 */
function getAllSelectedSeat(seatList) {

  let selectedSeats = matrix.map(function (seat) {

    return {
      seat_no: seat.seat_no,
      seat_nm: seat.seat_nm,
      seat_type_no: seat.seat_type_no,
      seat_type_nm: seat.seat_type_nm,
      sales_kb: seat.sales_kb
    };
  }).filter(function (seat, index) {
    return seatList[index].status == 'selected';
  });
  return selectedSeats;
}

/**
 * remove all selection in seat
 * @param seatList
 */
function removeAllSelectedSeat(seatList) {
  seatList.map(function (seat, index) {
    if (seat.status == 'selected') {
      seat.status = 'available';
      $('g[data-id=' + index + ']').children('rect').attr('fill', seat.color.available);
      $('g[data-id=' + index + ']').children('rect').attr('stroke', seat.border.color.available);
    }
  });
}

/**
 * change status of seat
 * @param seatList
 * @param seatNo
 * @param status ('available', 'selected')
 */
function changeStatusSeat(seatList, seatNo, status) {
  if (status != 'available' && status != 'selected' && status != 'unavailable') return;

  seatList.map(function (seat, index) {
    let checkSeatNo;
    if (Array.isArray(seatNo)) {
      checkSeatNo = seatNo.some(function (item) {
        return item == matrix[index].seat_no;
      })
    } else {
      checkSeatNo = matrix[index].seat_no == seatNo;
    }
    if (checkSeatNo) {

      seat.status = status;
      $('g[data-id=' + index + ']').children('rect').attr('fill', seat.color[status]);
      $('g[data-id=' + index + ']').children('rect').attr('stroke', seat.border.color[status]);

    }
  })
}

/**\
 * Change status success
 * @param seatList
 * @param seatNo
 * @param status
 */
function changeStatusSeatSuccess(seatList, seatNo, status) {
  if (status != 'available' && status != 'selected' && status != 'unavailable') return;

  seatList.map(function (seatL, index) {
    let checkSeatNo;
    if (Array.isArray(seatNo)) {
      checkSeatNo = seatNo.some(function (item) {
        return item == matrix[index].seat_no;
      })
    } else {
      checkSeatNo = matrix[index].seat_no == seatNo;
    }
    if (checkSeatNo && seatL.status != 'unavailable') {
      seatL.status = status;
      let cp1 = new Color(seatL.color['available']).toHex();
      let cp2 = new Color(new Color(seat.color.available).toRGBA()).toHex();
      if (cp1 == cp2) {
        $('g[data-id=' + index + ']').children('rect').attr('stroke', seatL.border.color[status]);
      } else {
        $('g[data-id=' + index + ']').children('rect').attr('stroke', seatL.color['available']);
      }
      $('g[data-id=' + index + ']').children('rect').attr('fill', seatL.color[status]);
    }
  })
}

/**
 * check IE
 * @returns {boolean}
 */
function detectIE() {
  var ua = window.navigator.userAgent;

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}

/**
 * define attribute custom seat
 * @type {{x: string, y: string, angle: string}}
 */
let sample = {
  x: "x_zahyo",
  y: "y_zahyo",
  angle: "angle",
  status: "sales_seat_flg",
  size: "seat_size",
  color: "seat_type_color",
  seat_nm: "seat_nm",
  seat_type_nm: "seat_type_nm",
  seat_type_no: "seat_type_no",
  seat_type_kb: "seat_type_kb",
  sales_kb: "sales_kb",
  number_specified_flg: "number_specified_flg"

}

/**
 * build panzoom instance to handle UI svg
 * @param configPanzoom
 */
function buildPanZoomInstance(configPanzoom) {

  if (!$('#diagram').find('#map-bg')) {
    setTimeout(function () {
      buildPanZoomInstance(configPanzoom);
    }, 300);
  } else {

    panZoomInstance = svgPanZoom('#diagram', configPanzoom);
  }

};


/**
 * build theatre
 * @param matrix (seats)
 * @param img (background)
 */
function buildDiagram(matrix, img) {
  // let diagram = $('svg');
  const MOBILE_WIDTH = 420;
  let diagram = $('#diagram_wrap');
  let panzoom = $('.panzoom');
  let seatList = [];

  let matrixDiagram = buildMapMatrix(seatList);
  var customPan = {};
  /**
   * Dom svg
   * @type {HTMLImageElement}
   */
  let realImage = new Image();
  realImage.src = img.src;
  realImage.onload = function () {
    let widthView = $(window).width();
    let width = realImage.naturalWidth;
    let height = realImage.naturalHeight;

    let image = '<image preserveAspectRatio="xMinYMin slice" x="0" y="0" width="' + width + '" height="' + (height) + '" xlink:href="' + img.src + '"></image>';

    if (widthView < MOBILE_WIDTH) {
      height = height * $(window).width() / width;
      width = '100vw';
    }

    panzoom.width(width);
    diagram.width(width);
    if (detectIE()) {
      // 100px to fix IE
      panzoom.height(height - 100);
      diagram.height(height - 100);
    } else {
      panzoom.height(height);
      diagram.height(height);
    }

    /**
     * config limit pan
     */
    if (widthView < MOBILE_WIDTH) {
      var beforePan = function (oldPan, newPan) {
        var gutterWidth = $(window).width() - 10
            , gutterHeight = height
        // Computed variables
            , sizes = this.getSizes()
            , leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth
            , rightLimit = sizes.width - gutterWidth - (sizes.viewBox.x * sizes.realZoom)
            , topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight
            , bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom);

        customPan = {};
        customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x));
        customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y));

        return customPan;
      };
    } else {
      var beforePan = function (oldPan, newPan) {
        if (!isCtrl && isScroll) {
          isScroll = false;
          return false;
        }

        let widthPan = width;
        let heightPan = height;
        if (detectIE()) {
          widthPan -= 75; // 70px to fix IE
          heightPan -= 100; // 100px to fix IE
        }
        var sizes = this.getSizes()
            , gutterWidth = sizes.viewBox.width
            , gutterHeight = sizes.viewBox.height;
        // Computed variables

        customPan = {};
        if (widthPan < gutterWidth) {
          gutterWidth = widthPan;
        }
        if (heightPan < gutterHeight) {
          gutterHeight = heightPan;
        }
        let leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth
            , rightLimit = sizes.width - sizes.viewBox.width - (sizes.viewBox.x * sizes.realZoom)
            , topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight
            , bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom);
        customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x));
        customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y));
        return customPan;
      };
    }

    /**
     * custom event to zoom with ctrl + wheel
     * @param oldZoom
     * @param newZoom
     * @returns {boolean}
     */
    var eventScrollScale = function (oldZoom, newZoom) {

      // if(isRightClick && isScroll) {
      //   isRightClick = false;
      //   return newZoom;
      // }

      if ((!isRightClick && !isCtrl) && isScroll) {
        return false;
      }
      if (isCtrl && isScroll) {
        return newZoom;
      }
      // if(!isRightClick && isScroll) {
      //   isScroll = false;
      //   return false;
      // }

      return newZoom;
    };

    /**
     * config panzoom
     */
    configPanzoom.beforePan = beforePan;
    configPanzoom.beforeZoom = eventScrollScale;
    $(function () {
      setTimeout(function () {
        buildPanZoomInstance(configPanzoom);
      }, 1000);

    });

    diagram.html(templateGraphic(image, matrixDiagram));
  };
  event(seatList);

}

/**
 * init diagram
 * @param srcImage
 * @param matrixApi
 */
function initDiagram(srcImage = "", matrixApi = {}) {
  matrix = matrixApi;

  buildDiagram(matrix, {
    src: srcImage
  });
}

function getSeatList() {
  // this.$store.dispatch("post/increment");
//    return seatList
}

export default {
  buildDiagram,
  initDiagram,
  changeStatusSeat,
  removeAllSelectedSeat,
  getAllSelectedSeat,
  getSeatNo,
  getSeatList
}


