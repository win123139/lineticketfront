/**
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  query listSeats {
    listSeats {
      items {
        id
        show_group_id
        show_no
        sales_no
        seat_no
        client_id
      }
    }
  }
`