/**
 * Created by DuyN on 12/14/2018.
 */
/**
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  query listTransacitons {
    listTransacitons {
      items {
        client_id
        transaction_id
        created_at
        status
      }
    }
  }
`