/**
 * Created by DuyN on 12/14/2018.
 */
/**
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  subscription NewTransacitonSub {
    onCreateTransacitons {
      client_id
      transaction_id
    }
  }
`