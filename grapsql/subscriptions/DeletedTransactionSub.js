/**
 * Created by DuyN on 12/18/2018.
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  subscription DeletedTransacitonSub {
    onDeleteTransacitons {
      client_id
      transaction_id
    }
  }
`