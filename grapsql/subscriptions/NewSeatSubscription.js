/**
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  subscription NewSeatSub {
    onCreateSeat {
      show_group_id
      show_no
      sales_no
      seat_no
      client_id
    }
  }
`