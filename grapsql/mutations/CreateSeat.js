/**
 * Graphql language
 */
import gql from 'graphql-tag';

export default gql`
  mutation createSeat($show_group_id: String!, $show_no: String!, $sales_no: String!, $seat_no: String!, $client_id: String! ,$member_id: String!) {
    createSeat(
      input: {
        show_group_id: $show_group_id, 
        show_no: $show_no, 
        sales_no: $sales_no, 
        seat_no: $seat_no, 
        client_id: $client_id,
        member_id: $member_id
      }
    ) {
      id
      show_group_id
      show_no
      sales_no
      seat_no
      client_id,
      member_id
    }
  }
`