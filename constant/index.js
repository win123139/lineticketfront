import router from '@/constant/router';
import api from '@/constant/api';
import config from '@/constant/config';
import http from '@/constant/http_code';
import appsync_local from '@/constant/appsync_local';
import appsync_server from '@/constant/appsync_server';

export default {
  router,
  api,
  config,
  http,
  appsync_local,
  appsync_server
}
