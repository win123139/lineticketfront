/**
 * File http_code.js
 * Define http code response
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-22
 */

const HTTP_CODE = {
  SUCCESS: 200,
  ERROR: 500,
  UNAUTHORIZED: 401,
  NOT_FOUND: 404,
  UNAUTHENTICATED: 403,
  VALIDATOR_ERROR: 422,
  BAD_GATEWAY: 502
};

export default HTTP_CODE;
