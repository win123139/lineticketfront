/**
 * File appsync.js
 * Define constant appsync in server
 *
 * @author Rikkei.DatDM
 * @date 2018-12-03
 */

const APP_SYNC = {
    graphqlendpoint: "https://tgvhz5v7ijfmbnlu6c2ubkxsku.appsync-api.ap-northeast-1.amazonaws.com/graphql",
    region: "ap-northeast-1",
    authenticationType: "API_KEY",
    apiKey: "da2-3rrytx6c5bbzbnsqv3fcoujxhm"
};

export default APP_SYNC;