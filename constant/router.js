/**
 * File router.js
 * Define router name for nuxt link
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-05
 */

const CONST = {
  SHOW_TOP: 'client_id',
  SELECT_TICKET_NAME: 'client_id-booking',
  SELECT_SEAT_NAME: 'client_id-booking-seat',
  CART: 'client_id-cart',
  LOGIN: '/login',
  LOGIN_NAME: 'client_id-login',
  LOGIN_ADMIN_NAME: 'client_id-login-admin',
  BASE_URL_NAME: 'client_id',
  COMPLETE_SEND_EMAIL_PASS: 'client_id-login-complete-send-email-reset-password',
  COMPLETE_SETTING_PASSWORD: 'client_id-login-complete-setting-password',
  TERMS: 'client_id-terms',
  REGISTER_INPUT: 'client_id-register-input',
  REGISTER_CONFIRM: 'client_id-register-confirm',
  FORGOTPASSWORD: 'login/forgot-password',
  // Name router of list show home page
  LISTPERFORM: 'client_id',
  ERROR_NAME: 'client_id-error',
  ERROR: 'error',
  ERROR_URL_EXPIRED: 'client_id-error-url-expired',
  ERROR_BLACK_CD: 'client_id-error-black-cd',
  // Name router list schedule show
  SHOW_SCHEDULE_LIST: 'client_id-show-show_group_id-schedule',
  REGISTER_COMPLETE_TEMPORARY: 'client_id-register-complete-temporary',
  BOOKING_DETAIL: 'client_id-booking-detail',
  MY_PAGE: 'client_id-my-page',
  GUIDE: 'client_id-guide',
  FAQ: 'client_id-help',
  HISTORY_ORDER: 'client_id-my-page-history-order',
  PRIVATE: 'client_id-privacy',
  KIYAKU: 'client_id-kiyaku',
  LAW: 'client_id-law',
  SPEC: 'client_id-spec',
  UPDATE_INFOMATION: 'client_id-my-page-update-infomation',
  CONFIRM_UPDATE_INFOMATION: 'client_id-my-page-update-infomation-confirm',
  CART_PAYMENT_METHOD: "client_id-cart-payment-method",
  COMPLETE_UPDATE_INFOMATION: 'client_id-my-page-complete-update',
  // REGISTER MEMBERSHIP
  MEMBERSHIP_REGISTER: 'client_id-my-page-membership-register',
  MEMBERSHIP_CONFIRM: 'client_id-my-page-membership-confirm',
  BOOKING_COMPLETE: 'client_id-cart-booking-complete',
  BOOKING_COMPLETE_OUTSIDE: 'client_id-cart-booking-complete-outside',
  MEMBERSHIP_COMPLETE: 'client_id-my-page-membership-complete'
};

export default CONST;
