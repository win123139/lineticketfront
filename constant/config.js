const CONFIG = {
  // Limit record show on a page
  RECORD_PER_PAGE: 10,
  // In list show, when user load max record then fixed footer to display footer on screen
  MAX_RECORD_FIXED_FOOTER: 10,

  // File logo show on header
  IMG_LOGO: 'logo_image',
  // Main image of show on page list show
  IMG_SHOW_MAIN: 'internet_pic0_image',
  // Main image of show on page list schedule show
  IMG_SHOW_MAIN_SCHEDULE: 'internet_pic1_image',
  // Main image hall pic from order ticket
  IMG_HALL_PIC: 'hall_pic',

  // Path image from S3
  PATH_IMG_SHOW_MAIN: '/:client_id/event/:show_group_id/internet_pic0_image',
  PATH_IMG_SHOW_MAIN_SCHEDULE: '/:client_id/event/:show_group_id/internet_pic1_image',
  // Seat type
  SEAT_DESIGNATED: 1,
  SEAT_FREE: 2,
  // Seat type
  NUMBER_TICKET: 1,
  NOT_NUMBER_TICKET: 0,
  // Type of layout select seat
  LAYOUT_WITH_SEAT: 1,
  LAYOUT_NO_SEAT: 0,
  // Path bg seat map from S3
  PATH_IMG_SEAT_MAP: '/:client_id/layout/:hall_no/:hall_layout_no/hall_pic',

  INS_PG_ID : 'TEST',
  UPD_PG_ID : 'TEST',
  ACCOUNT_ID: 'TEST',

  MALE: 1,
  FEMALE: 2,
  NOT_GET: 0,
  GET: 1,
  COMBINI: '103',
  EXISTS: 1,
  NOT_EXISTS: 0,
  // Code of payment method delivery is 202
  PAYMENT_METHOD_DELIVERY_CODE: 202,
  // Code of payment method Family mart is 203
  PAYMENT_METHOD_FAMILYMART_CODE: 203,
  // Code of payment method credit card is 102
  PAYMENT_METHOD_CREDIT_CARD_CODE: 102,
  // Code of payment method credit card is 102
  PAYMENT_METHOD_COMBINI_CODE: 103,
  // My page
  ACTION_NEW_JOIN: 0,
  ACTION_UPDATE_JOIN: 1
};

export default CONFIG;
