/**
 * File redirect_if_is_admin.js
 * Middleware check not allow admin access
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-28
 * @export default
 * @param {*} { store, redirect, route }
 * @returns {redirect}
 */
import constant from '../constant';

export default function ({ store, redirect, route }) {
  // Check user has already login, redirect to Home page
  let admin = store.state.auth.admin;
  let isObjectAdmin = typeof admin === 'object';
  let isNotEmptyAdmin = isObjectAdmin && Object.keys(admin).length;

  // Redirect to page 570 if is admin
  if (store.state.auth.authenticated && isNotEmptyAdmin) {

    let path = $nuxt.$router.resolve({
      name: constant.router.ERROR_NAME,
      params: { client_id: $nuxt.$route.params.client_id }
    });

    store.dispatch('auth/setError', [
      $nuxt.$t('message.msg073_my_page_admin')
    ]);

    $nuxt.$router.push(path.href);
  }
}
