/**
 * Import all language available
 *
 * @version 1.0
 * @author Rikkei.TriHNM
 * @date 2018-09-26
 */

import ja from '@/locale/ja';
import en from '@/locale/en';

export default {
  ja,
  en
}
