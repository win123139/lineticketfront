/**
 * Register file language
 *
 * @version 1.0
 * @author Rikkei.TriHNM
 * @date 2018-09-26
 */

import common from '@/locale/ja/common.json';
import terms from '@/locale/ja/terms.json';
import login from '@/locale/ja/login.json';
import message from '@/locale/ja/message.json';
import register from '@/locale/ja/register.json';
import booking from '@/locale/ja/booking.json';
import validation from '@/locale/ja/validation.json';
import show from '@/locale/ja/show.json';
import myPage from '@/locale/ja/my_page.json';
import historyOrder from '@/locale/ja/history_order.json';
import booking_detail from '@/locale/ja/booking_detail.json';
import cart from '@/locale/ja/cart.json';
import updateInfomation from '@/locale/ja/update_infomation.json';
import member from '@/locale/ja/member.json';
import card_payment from '@/locale/ja/card_payment.json';

export default {
  common,
  terms,
  login,
  message,
  register,
  booking,
  validation,
  show,
  booking_detail,
  myPage,
  historyOrder,
  cart,
  member,
  updateInfomation,
  card_payment
}
