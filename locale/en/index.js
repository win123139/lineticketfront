/**
 * Register file language
 *
 * @version 1.0
 * @author Rikkei.TriHNM
 * @date 2018-09-26
 */


import common from '@/locale/en/common.json';
import terms from '@/locale/en/terms.json';

export default {
  common,
  terms
}
