/**
 * File MyPageBusiness.js
 * Show MyPage
 *
 * @author Rikkei.DucVN
 * @date 2018-10-12
 */

import constant from '@/constant';
import { get } from '@/plugins/api';
import { mapState } from 'vuex';

export default {
  name: 'MaintenanceBusiness',
  layout: 'default',
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('common.links.my_page')
    }
  },
  data: () => ({
    routerNameHistoryOrder: constant.router.HISTORY_ORDER,
    routerNameUpdateInfomation: constant.router.UPDATE_INFOMATION,
    dataInit: {
      listHistory: [],
      member: {},
      historyMember: [],
      memberCheck: {},
      orderCheck: {}
    },
    dataShow: [],
    flagShowRegisterMember: false,
    isCombiniType: false,
    flagShowMemberInfo: 0,
    flagShowButtonRegisterMember: false,
    flagShowMessage: false,
    message: '',
    flagShowHistoryMember: false,
    paymentCombini: constant.config.COMBINI,
    isLoadFinish: false,
    errorFrom480: []
  }),
  beforeRouteEnter (to, from, next) {
    if (from.name == constant.router.MEMBERSHIP_CONFIRM) {
      localStorage.setItem('error_480', true);
    } else {
      localStorage.removeItem('error_480');
    }

    next();
  },
  mounted () {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
    })
  },
  created() {
    this.initPage();

    if (!!localStorage.getItem('error_480')) {
      this.errorFrom480 = this.auth.error;
      this.$store.dispatch('auth/setError', []);
    } else {
      this.$store.dispatch('auth/setError', []);
    }
  },
  computed: {
    ...mapState({
      auth: state => state.auth
    })
  },
  methods: {
    /**
     * Function init page
     *
     * @returns {void}
     */
    initPage() {
      get(constant.api.MY_PAGE_INIT, { client_id: this.$route.params.client_id, member_id: this.auth.user.member_id })
        .then(res => {
          this.dataInit.listHistory = res.data.data.member_list_information;
          this.dataInit.member = res.data.data.member_information;
          this.dataInit.memberCheck = res.data.data.member_check;
          this.dataInit.orderCheck = res.data.data.check_exists_order;

          // Get first 5 record
          for (let index = 0; index < 5; index++) {
            if (this.dataInit.listHistory[index]) {
              this.dataInit.historyMember.push(this.dataInit.listHistory[index]);


            }
          }

          this.getInfToShowMember();
          this.flagShowMemberInfo = this.dataShow.length != 0;
          this.flagShowHistoryMember = this.dataInit.historyMember.length != 0;
          this.checkShowBtnJoinGroup();

          this.$nuxt.$loading.finish();
          this.isLoadFinish = true;
        }).catch(err => {
          let error = err.response.data;

          if (error.statusCode == constant.http.UNAUTHENTICATED) {
            // Add message error
            this.$store.dispatch('auth/setError', [
              this.$t('message.msg085_exception.line_1'),
              this.$t('message.msg085_exception.line_2'),
            ]);
          } else {
            // Add message error
            this.$store.dispatch('auth/setError', [
              this.$t('message.msg003_exception.line_1'),
              this.$t('message.msg003_exception.line_2'),
              this.$t('message.msg003_exception.line_3')
            ]);
          }

          this.$nuxt.$loading.finish();

          // Redirect to page error 570
          let path = this.$router.resolve({
            name: constant.router.ERROR_NAME,
            params: { client_id: this.$route.params.client_id }
          });

          this.$router.push(path.href);
        });
    },

    /**
     * Function split string to date
     *
     * @param {String} str
     * @param {Boolean} option
     * @returns {String}
     */
    splitDate(str, option = false) {
      if (!str) return str;

      if (option) {
        return {
          year: str.slice(0, 4),
          month: str.slice(4, 6),
          day: str.slice(6, 8)
        }
      }

      return str.slice(0, 4) + '/' + str.slice(4, 6) + '/' + str.slice(6, 8);
    },

    /**
     * Function return status history member
     *
     * @param {String} startDate
     * @param {String} endDate
     * @param {String} nyukinFlg
     * @returns {String}
     */
    returnStatus(startDate, endDate, nyukinFlg) {
      let now = new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, ' ').replace(/[-]+/g, '');

      if (parseInt(now) > parseInt(endDate)) {
        return {
          status: false,
          text: this.$t('myPage.lb_expired')
        };
      }

      if (parseInt(startDate) <= parseInt(now) && parseInt(now) <= parseInt(endDate) && nyukinFlg == '1') {
        // return this.$t('myPage.lb_member_enable');
        return {
          status: false,
          text: ''
        };
      }

      if (parseInt(now) <= parseInt(endDate) && nyukinFlg == '0') {
        return {
          status: true,
          text: this.$t('myPage.lb_had_register')
        };
      }

      if (parseInt(now) <= parseInt(startDate) && nyukinFlg == '1') {
        return {
          status: false,
          text: this.$t('myPage.lb_had_updated')
        };
      }
    },

    /**
     * Function add member inf to array
     *
     * @return {void}
     */
    getInfToShowMember() {
      let now = parseInt(new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, ' ').replace(/[-]+/g, ''));

      this.dataInit.listHistory.forEach(member => {
        let start = parseInt(member.member_start_date);
        let end = parseInt(member.member_end_date);

        if ((start <= now && now <= end)
          && (member.condition_kb == '1' || member.condition_kb == '2' || member.condition_kb == '3')
        ) {
          this.dataShow.push(member);
          this.isCombiniType = member.settle_cd == constant.config.COMBINI;
        }
      });
    },

    /**
     * Function check show button join group
     *
     * @returns {boolean}
     */
    checkShowBtnJoinGroup() {
      // Is not have join group
      if (!this.dataInit.listHistory.length) {
        this.flagShowButtonRegisterMember = true;

        return this.flagShowButtonRegisterMember;
      }

      let now = parseInt(new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, ' ').replace(/[-]+/g, ''));

      let isExpired = true;
      this.dataInit.listHistory.forEach(el => {
        let start = parseInt(el.member_start_date);
        let end = parseInt(el.member_end_date);

        if (el.condition_kb != '0' && !(!(start > now) && (end < now))) isExpired = false;
      });

      if (isExpired) {
        this.flagShowButtonRegisterMember = true;
        return this.flagShowButtonRegisterMember;
      }

      // Check is update or expired
      this.dataShow.forEach(history => {
        let start = parseInt(history.member_start_date);
        let end = parseInt(history.member_end_date);
        let isCanUpdate = (start <= now && now <= end && history.condition_kb == '2');

        if (isCanUpdate) {
          this.flagShowButtonRegisterMember = true;
        }
      });

      return this.flagShowButtonRegisterMember;
    },

    /**
     * Function return name button join group
     *
     * @returns {String}
     */
    renderNameButton(groupName) {
      // Is not have join group
      if (!this.dataInit.listHistory.length) {
        this.$store.dispatch('member/updateAction', constant.config.ACTION_NEW_JOIN);

        return groupName + this.$t('myPage.lb_register_join_group');
      }

      let str = '';
      let now = parseInt(new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, ' ').replace(/[-]+/g, ''));

      let isExpired = true;
      this.dataInit.listHistory.forEach(el => {
        let start = parseInt(el.member_start_date);
        let end = parseInt(el.member_end_date);

        if (el.condition_kb != '0' && !(!(start > now) && (end < now))) isExpired = false;
      });

      // Check is expired member
      if (isExpired) {
        this.$store.dispatch('member/updateAction', constant.config.ACTION_NEW_JOIN);
        return groupName + this.$t('myPage.lb_register_join_group');
      }

      // Check is update
      this.dataShow.forEach(history => {
        let start = parseInt(history.member_start_date);
        let end = parseInt(history.member_end_date);

        let isCanUpdate = (start <= now && now <= end && history.condition_kb == '2');

        if (isCanUpdate) {
          this.$store.dispatch('member/updateAction', constant.config.ACTION_UPDATE_JOIN);
          str = this.$t('myPage.lb_register_update');
          return;
        }
      });

      return groupName + str;
    },

    /**
     * Function check member before join group
     *
     * @returns {String|void}
     */
    checkMember() {
      const IS_EXISTS = '0';

      if (this.onlyAcceptCondition() && this.dataInit.memberCheck.new_flg == '0') {
        this.message = this.$t('message.msg082_can_not_join_group', { group: this.dataInit.member.disp_member_nm });
        this.flagShowMessage = true;

        return;
      } else if (!this.onlyAcceptCondition() && this.dataInit.memberCheck.continue_flg == '0') {
        this.message = this.$t('message.msg083_can_not_update', { group: this.dataInit.member.disp_member_nm });
        this.flagShowMessage = true;

        return;
      } else if (this.dataInit.orderCheck.check_flg == IS_EXISTS) {
        $('#dialog-error').modal('show');
      } else {
        // Setting can access page 470
        this.$store.dispatch('member/updateStepAccess', true);

        // Redirect to page register into membership
        let path = this.$router.resolve({
          name: constant.router.MEMBERSHIP_REGISTER,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
      }
    },

    /**
     * Function check is accept only type condition kb
     *
     * @param {Boolean} status
     * @return {Boolean}
     */
    onlyAcceptCondition() {
      let status = true;

      if (!this.dataInit.listHistory.length) return true;

      this.dataInit.listHistory.forEach(el => {
        if (el.condition_kb != '0') status = false;
      });

      return status;
    }
  }
}
