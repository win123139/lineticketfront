/**
 * File ConfirmBusiness.js
 * Handler business in page confirm user infomation
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-10
 */

import { mapState } from 'vuex';
import constant from '@/constant';
import { post, get } from '@/plugins/api';

const DEFAULT_GENRE_INLINE = 4;
const IS_EXISTS = 1;
const IS_EXISTS_ORDER = 0;

export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('updateInfomation.lb_title_confirm_inf')
    }
  },
  data: () => ({
    model: {},
    dataInit: {
      countLineGenre: 0,
      indexLineGenre: DEFAULT_GENRE_INLINE - 1,
      listGenre: [],
      memberInf: ''
    },
    isMale: constant.config.MALE,
    isFemale: constant.config.FEMALE,
    get: constant.config.GET,
    notGet: constant.config.NOT_GET,
    dataShowBirthday: {
      birthday: '',
      year: '',
      month: '',
      day: ''
    },
    isClickUpdate: false
  }),
  created() {
    // Redirect to page 430 update infomation
    if (Object.keys(this.member.member_info).length === 0 && this.member.member_info.constructor === Object) {
      let path = this.$router.resolve({
        name: constant.router.UPDATE_INFOMATION,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    } else {
      this.initPage();
    }

  },
  computed: {
    ...mapState({
      member: state => state.member,
      client: state => state.client.clientInfo
    })
  },
  methods: {
    /**
     * Function go back to prev page
     *
     * @returns {void}
     */
    backToPrevPage: function () {
      let path = this.$router.resolve({
        name: constant.router.UPDATE_INFOMATION,
        params: { client_id: this.$route.params.client_id }
      });
      this.$router.push(path.href);
    },
    /**
     * Function init page 130 confirm user infomation
     *
     * @returns {void}
     */
    initPage: function () {
      this.model = this.member.member_info;
      this.splitPostCode(this.model.post_no);
      this.splitBirthday(this.model.birthday);

      if (this.model.flagShowGenre) {
        this.dataInit.listGenre = this.member.listGenreInit;
        this.dataInit.countLineGenre = this.dataInit.listGenre.length;
      }

      if (!!localStorage.getItem('member_inf')) {
        this.dataInit.memberInf = JSON.parse(localStorage.getItem('member_inf'));
      } else {
        this.dataInit.memberInf = {
          client_id: this.client.client_id
        }
      }
    },
    /**
     * Function split post code
     *
     * @return {void}
     */
    splitPostCode: function (postCode) {
      this.model.postCode1 = postCode.substring(0, 3);
      this.model.postCode2 = postCode.substring(3, postCode.length);
    },
    /**
     * Function split birthday
     *
     * @return {void}
     */
    splitBirthday: function (birthday) {
      this.dataShowBirthday.year = birthday.substring(0, 4);
      this.dataShowBirthday.month = birthday.substring(4, 6);
      this.dataShowBirthday.day = birthday.substring(6, birthday.length);
      this.dataShowBirthday.birthday = this.dataShowBirthday.year
        + '-' + this.dataShowBirthday.month
        + '-' + this.dataShowBirthday.day;
    },
    /**
     * Function redirect to page error
     *
     * @returns {void}
     */
    redirectToError: function () {
      let path = this.$router.resolve({
        name: constant.router.ERROR_NAME,
        params: { client_id: this.$route.params.client_id }
      });
      this.$router.push(path.href);
    },
    /**
     * Function click call api change infomation
     *
     * @return {void}
     */
    updateInf: function () {
      // Check click button
      if (this.isClickUpdate) {
        return;
      }

      this.isClickUpdate = true;
      this.$nuxt.$loading.start();

      get(constant.api.CHECK_EXISTS_ORDER, {
        client_id: this.dataInit.memberInf.client_id,
        member_id: this.member.member_info.member_id
      }).then(response => {
        // Check exists order then update
        if (response.data.data.exists_order.check_flg == IS_EXISTS_ORDER) {
          $('#dialog-error').modal('show');
          this.$nuxt.$loading.finish();
          this.isClickUpdate = false;

          return;
        }
        // Call api update infomation
        post(constant.api.POST_UPDATE_INFOMATION, {
          client_id: this.dataInit.memberInf.client_id,
          member_id: this.model.member_id,
          mail: this.model.mail_address,
          full_name: this.model.member_nm,
          furigana: this.model.member_kn,
          password: this.model.password,
          post_no: this.model.post_no,
          prefecture: this.model.prefecture,
          municipality: this.model.municipality,
          address1: this.model.address1,
          address2: this.model.address2,
          tel_no: this.model.tel_no,
          mobile_no: this.model.mobile_no,
          mail_send_flg: this.model.mail_send_flg,
          post_send_flg: this.model.post_send_flg,
          sex_type: this.model.sex_type,
          birthday: this.dataShowBirthday.birthday,
          list_genre: this.model.list_genre,
          login_id: this.model.login_id,
          member_code: this.model.member_code_input
        }).then(result => {
          // Reset infomation member
          localStorage.setItem("token", result.data.data.token);

          // Reset state after login
          this.$store.dispatch('auth/setUser', result.data.data.userInf);
          this.$store.dispatch('member/unsetMember');
          this.$store.dispatch('member/unsetError');
          this.$nuxt.$loading.finish();
          this.isClickUpdate = false;

          return true;
        }).then(status => {
          // Redirect to page 450
          let path = this.$router.resolve({
            name: constant.router.COMPLETE_UPDATE_INFOMATION,
            params: { client_id: this.$route.params.client_id }
          });

          this.$nuxt.$loading.finish();
          this.$router.push(path.href);
        }).catch(err => {
          this.$nuxt.$loading.finish();
          let response = err.response;

          if (response.status == constant.http.ERROR) {
            // Redirect to page 570
            this.$store.dispatch('auth/setError', [
              this.$t('message.msg003_exception.line_1'),
              this.$t('message.msg003_exception.line_2'),
              this.$t('message.msg003_exception.line_3')
            ]);

            let path = this.$router.resolve({
              name: constant.router.ERROR_NAME,
              params: { client_id: this.$route.params.client_id }
            });

            this.$router.push(path.href);
          }

          if (response.status == constant.http.VALIDATOR_ERROR) {
            let errors = response.data.data.errors;
            let errorKey = Object.keys(errors);
            let message = [];
            errorKey.forEach(key => {
              if (key == 'mail') {
                message.push(this.$t('validation.unique', { field: this.$t('updateInfomation.lb_mail') }));
              }

              if (key == 'member_code') {
                message.push(this.$t('message.msg065_not_exists_member_code'));
              }

              if (key == 'login_id') {
                message.push(this.$t('validation.unique', { field: this.$t('updateInfomation.lb_login_id') }));
              }
            });

            this.$store.dispatch('member/setError', message);

            let path = this.$router.resolve({
              name: constant.router.UPDATE_INFOMATION,
              params: { client_id: this.$route.params.client_id }
            });

            this.$router.push(path.href);
          }
        });
      }).catch(err => {
        let error = err.response.data;

        if (error.statusCode == constant.http.UNAUTHENTICATED) {
          // Add message error
          this.$store.dispatch('auth/setError', [
            this.$t('message.msg085_exception.line_1'),
            this.$t('message.msg085_exception.line_2'),
          ]);
        } else {
          this.$store.dispatch('auth/setError', [
            this.$t('message.msg003_exception.line_1'),
            this.$t('message.msg003_exception.line_2'),
            this.$t('message.msg003_exception.line_3')
          ]);
        }

        // Redirect to page 570
        let path = this.$router.resolve({
          name: constant.router.ERROR_NAME,
          params: { client_id: this.$route.params.client_id }
        });
      });
    }
  }
}
