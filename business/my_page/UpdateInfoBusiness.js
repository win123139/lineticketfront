/**
 * File UpdateInfoBusiness.js
 * Handle business when update infomation
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-06
 */

import { mapState } from 'vuex';
import constant from '@/constant';
import { get } from '@/plugins/api';

const DEFAULT_GENRE_INLINE = 4;
const MALE = 0;
const GET = 1;
const NOT_GET = 0;
const ENTER_SPACE = 1;
const REMOVE_SPACE = 2;
const ENTER_DASH = 1;
const REMOVE_DASH = 2;

export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('updateInfomation.lb_title_update_infomation')
    }
  },
  data: () => ({
    errorsMsg: [],
    flagRequiredWith: true,
    flagValidate: false,
    statusInputPhoneNumber: 0,
    statusInputName: 0,
    now: new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, ''),
    dataInit: {
      clientId: '',
      city: [],
      listGenre: [],
      countLineGenre: 0
    },
    model: {
      loginId: '',
      mail: '',
      confirmedMail: '',
      fullName: '',
      furigana: '',
      phoneNumber: '',
      cellPhone: '',
      postCode: '',
      postCode1: '',
      postCode2: '',
      slbCity: [],
      district: '',
      address: '',
      buildingRoom: '',
      birthday: '',
      year: '',
      month: '',
      day: '',
      gender: '',
      password: '',
      confirmedPassword: '',
      memberCode: '',
      flagShowMemberCode: 0,
      flagShowMagazineMail: 0,
      flagShowDirectMail: 0,
      magazineMail: '',
      directMail: '',
      flagShowGenre: false,
      listGenre: [],
      listGenreName: [],
      memberCodeInput: ''
    },
    memberCodeText: '',
    clientId: '',
    routerToMyPage: constant.router.MY_PAGE,
  }),
  beforeRouteEnter(to, from, next) {
    // Reload or go from other page != page 440
    if (from.name !== constant.router.CONFIRM_UPDATE_INFOMATION) {
      localStorage.removeItem('check_error');
    } else {
      // Check show message error when back from page 440 to page 130
      localStorage.setItem('check_error', true);
    }

    next();
  },
  mounted () {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();

      setTimeout(() => this.$nuxt.$loading.finish(), 500);
    })
  },
  watch: {
    'model.year': function (newYear, oldYear) {
      if (this.model.year.length > 4) {
        this.model.year = this.model.year.substring(0, 4);
      }
    },

    'model.month': function (newMonth, oldMonth) {
      if (this.model.month.length > 2) {
        this.model.month = this.model.month.substring(0, 2);
      }
    },

    'model.day': function (newDay, oldDay) {
      if (this.model.day.length > 2) {
        this.model.day = this.model.day.substring(0, 2);
      }
    },

    'model.postCode1': function (newVal, oldVal) {
      if (this.model.postCode1.length > 3) {
        this.model.postCode1 = this.model.postCode1.substring(0, 3);
      }
    },

    'model.postCode2': function (newVal, oldVal) {
      if (this.model.postCode2.length > 4) {
        this.model.postCode2 = this.model.postCode2.substring(0, 4);
      }
    }
  },
  created() {
    this.setModel();

    // If register fail in page 440 then show error in page 430
    if (!!localStorage.getItem('check_error') && this.member.errors.length) {
      this.errorsMsg = this.member.errors;
    } else {
      this.$store.dispatch('member/unsetError');
    }
  },
  computed: {
    ...mapState({
      auth: state => state.auth,
      client: state => state.client,
      member: state => state.member
    })
  },
  methods: {
    /**
     * Function set model when load page
     *
     * @return {void}
     */
    setModel: function () {
      let clientInf = this.client.clientInfo;
      this.clientId = clientInf.client_id;

      this.statusInputName = clientInf.member_nm_kb;
      this.statusInputPhoneNumber = clientInf.tel_no_kb;
      this.model.flagShowMemberCode = clientInf.member_id_input_disp_kb == 1;
      this.memberCodeText = clientInf.member_id_input_text;
      this.model.flagShowMagazineMail = clientInf.mail_send_disp_kb == 1;
      this.model.flagShowDirectMail = clientInf.post_send_disp_kb == 1;

      let isEmptyState = Object.keys(this.member.member_info).length === 0 && this.member.member_info.constructor === Object;

      let userInf = isEmptyState ? this.auth.user : this.member.member_info;

      this.model.loginId = userInf.login_id;
      this.model.mail = userInf.mail_address;
      this.model.confirmedMail = userInf.mail_address;
      this.model.fullName = userInf.member_nm;
      this.model.furigana = userInf.member_kn;
      this.model.phoneNumber = userInf.tel_no;
      this.model.cellPhone = userInf.mobile_no;
      this.model.postCode = userInf.post_no;
      this.splitPostCode(userInf.post_no);
      this.initPage();
      this.model.slbCity = userInf.prefecture;
      this.model.district = userInf.municipality;
      this.model.address = userInf.address1;
      this.model.buildingRoom = userInf.address2;
      this.splitBirthday(userInf.birthday);
      this.model.gender = userInf.sex_type == constant.config.MALE ? 'male': 'female';
      this.model.memberCode = userInf.member_id;
      this.model.magazineMail = userInf.mail_send_flg;
      this.model.directMail = userInf.post_send_flg;

      if (!isEmptyState) {
        this.model.listGenre = userInf.list_genre;
        this.model.memberCodeInput = userInf.member_code_input;
      }

      this.renderMsgErr();
    },

    /**
     * Function init page update infomation
     *
     * @return {void}
     */
    initPage: function () {
      get(constant.api.INIT_UPDATE_PROFILE, { client_id: this.clientId, member_id: this.auth.user.member_id })
        .then(res => {
          this.dataInit.city = res.data.data.list_city;

          if (res.data.data.list_gener.length) {
            this.model.flagShowGenre = true;

            let data = res.data.data.list_gener;
            let from = 0;
            let to = DEFAULT_GENRE_INLINE - 1;
            let arrGenre = [];
            let obj = [];

            // Loop every genre and add to array
            data.forEach((element, index) => {
              // If in the line is not full genre then add genre to sub array
              if (from <= index && index < to) {
                obj.push(element);
              } else if (index == to && index < data.length - 1) {
                // If in the line is full genre then add sub array to array

                // Reset from, to
                from = to + 1;
                to = to + DEFAULT_GENRE_INLINE;

                // Push element to sub array
                obj.push(element);

                // Push sub array to array
                arrGenre.push(obj);

                // Reset sub array
                obj = [];
              }

              // Push sub array to array if element is end of genre
              if (index == data.length - 1) {
                arrGenre.push(obj);
              }
            });

            this.dataInit.listGenre = arrGenre;
            this.dataInit.countLineGenre = this.dataInit.listGenre.length;

            let isEmptyState = Object.keys(this.member.member_info).length === 0 && this.member.member_info.constructor === Object;

            // Check is back form 440 or not
            if (isEmptyState) {
              res.data.data.list_gener_member.forEach(el => {
                this.model.listGenre.push(el.genre_no);
              });
            }
          }
        }).catch(err => {
          let error = err.response.data;

          if (error.statusCode == constant.http.UNAUTHENTICATED) {
            // Add message error
            this.$store.dispatch('auth/setError', [
              this.$t('message.msg085_exception.line_1'),
              this.$t('message.msg085_exception.line_2'),
            ]);
          } else {
            this.$store.dispatch('auth/setError', [
              this.$t('message.msg003_exception.line_1'),
              this.$t('message.msg003_exception.line_2'),
              this.$t('message.msg003_exception.line_3')
            ]);
          }

          // Redirect to page 570
          let path = this.$router.resolve({
            name: constant.router.ERROR_NAME,
            params: { client_id: this.$route.params.client_id }
          });

          this.$router.push(path.href);
        });
    },

    /**
     * Function split post code
     *
     * @return {void}
     */
    splitPostCode: function (postCode) {
      this.model.postCode1 = postCode.substring(0, 3);
      this.model.postCode2 = postCode.substring(3, postCode.length);
    },

    /**
     * Function split birthday
     *
     * @return {void}
     */
    splitBirthday: function (birthday) {
      this.model.year = birthday.substring(0, 4);
      this.model.month = birthday.substring(4, 6);
      this.model.day = birthday.substring(6, birthday.length);

      this.model.birthday = this.model.year + '-' + this.model.month + '-' + this.model.day;
    },

    /**
     * Function change birthday if onchange in year, mon, day
     *
     * @returns {void}
     */
    watchBirthday: function() {
      if (this.model.year == '' && this.model.month == '' && this.model.day == '') {
        return this.model.birthday = '';
      }

      let year = Number(this.model.year);
      let month = Number(this.model.month);
      let day = Number(this.model.day);

      if (Number.isInteger(month) && parseInt(month) < 10) {
        month = '0' + parseInt(month);
      }

      if (Number.isInteger(day) && parseInt(day) < 10) {
        day = '0' + parseInt(day);
      }

      this.model.birthday = year + '-' + month + '-' + day;
    },

    /**
     * Function search post code get address
     *
     * @param {string} code_1
     * @param {string} code_2
     * @returns {Array|null}
     */
    searchPostCode: function(code_1, code_2) {
      get(constant.api.SEARCH_POST_CODE, { post_code_1: code_1, post_code_2: code_2 })
        .then(res => {
          let result = res.data.data;

          // Api response have result
          if (result.errors === undefined && result.list_address.length) {
            let city = this.dataInit.city.find(element => element.code_nm === result.list_address[0].todofuken_nm);

            this.model.slbCity = city.code_nm;
            this.model.district = result.list_address[0].shikuchoson_nm;
            this.model.address = result.list_address[0].choiki_nm;
            this.model.buildingRoom = '';
          } else {
            // Api response empty result
            this.model.slbCity = '';
            this.model.district = '';
            this.model.address = '';
            this.model.buildingRoom = '';

            $('#confirm-error-search-post-code').modal('show');
          }
        })
        .catch(err => {
          this.model.slbCity = '';
          this.model.district = '';
          this.model.address = '';
          this.model.buildingRoom = '';

          $('#confirm-error-search-post-code').modal('show');
        });
    },

    /**
     * Function change postcode if onchange in post code 1 and post code 2
     *
     * @returns {void}
     */
    watchPostCode: function() {
      this.model.postCode = this.model.postCode1 + this.model.postCode2;
    },

    /**
     * Function check validation require with
     *
     * @return {boolean}
     */
    checkRequireWith: function () {
      if (this.model.phoneNumber == '' && this.model.cellPhone == '') {
        this.flagRequiredWith = false;

        return this.flagRequiredWith;
      }
    },

    /**
     * Function go to next page
     *
     * @return {void}
     */
    goToNext: function () {
      this.$validator.validate().then(result => {
        // Validator check required with when validator all input
        this.checkRequireWith();
        this.$store.dispatch('auth/setError', []);
        this.$store.dispatch('member/unsetError');
        this.errorsMsg = [];

        if (!result) {
          this.flagValidate = true;
          window.scrollTo(0, 0);

          return;
        }

        // Reset validator is false to hidden error
        this.flagValidate = false;

        this.$store.dispatch('member/setMember', {
          member:  this.model,
          listGenre: this.dataInit.listGenre
        });

        let path = this.$router.resolve({
          name: constant.router.CONFIRM_UPDATE_INFOMATION,
          params: { client_id: this.$route.params.client_id }
        });

        // Remove localstorage if user not enter member code
        if (this.model.memberCodeInput == '') {
          localStorage.removeItem('member_inf');
        }

        this.$router.push(path.href);
      });
    },

    /**
     * Function overider message validator
     *
     * @returns {void}
     */
    renderMsgErr: function () {
      const dict = {
        custom: {
          login_id: {
            existsLoginId: this.$t('validation.unique', { field: this.$t('updateInfomation.lb_login_id') }),
            textNumberHaftSize: this.$t('validation.textNumberHaftSize', { field: this.$t('updateInfomation.lb_login_id') })
          },
          mail: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_mail') }),
            mail: this.$t('validation.email', { field: this.$t('updateInfomation.lb_mail') }),
            confirmed: this.$t('validation.confirmed', { field: this.$t('updateInfomation.lb_mail') }),
            existsMailUpdate: this.$t('validation.unique', { field: this.$t('updateInfomation.lb_mail') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_mail'), value: 200 }),
          },
          confirmed_mail: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_confirm_mail') }),
            mail: this.$t('validation.email', { field: this.$t('updateInfomation.lb_confirm_mail') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_confirm_mail'), value: 200 }),
          },
          full_name: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_full_name') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_full_name'), value: 200 }),
            spaceFullSize: this.statusInputName == ENTER_SPACE
              ? this.$t('message.msg010_enter_space_full_size', { field: this.$t('updateInfomation.lb_full_name') })
              : this.$t('message.msg011_remove_space_full_size', { field: this.$t('updateInfomation.lb_full_name') }),
            fullsize: this.$t('message.msg012_input_full_size', { field: this.$t('updateInfomation.lb_full_name') }),
          },
          furigana: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_furigana') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_furigana'), value: 200 }),
            kanaFullSize: this.statusInputName == ENTER_SPACE
              ? this.$t('message.msg010_enter_space_full_size', { field: this.$t('updateInfomation.lb_furigana') })
              : (this.statusInputName == REMOVE_SPACE
                  ? this.$t('message.msg011_remove_space_full_size', { field: this.$t('updateInfomation.lb_furigana') })
                  : this.$t('message.msg012_input_full_size', { field: this.$t('updateInfomation.lb_furigana') })
                ),
          },
          phone_number: {
            phoneNumber: this.statusInputPhoneNumber == ENTER_DASH
              ? this.$tc('message.msg013_enter_dash', false, { field: this.$t('updateInfomation.lb_phone_number'), digit: 10 })
              : (this.statusInputPhoneNumber == REMOVE_DASH
                ? this.$tc('message.msg014_remote_dash', false, { field: this.$t('updateInfomation.lb_phone_number'), digit: 10 })
                : this.$t('validation.format', { field: this.$t('updateInfomation.lb_phone_number') })
              )
          },
          cell_phone: {
            phoneNumber: this.statusInputPhoneNumber == ENTER_DASH
              ? this.$tc('message.msg013_enter_dash', false, { field: this.$t('updateInfomation.lb_cell_phone'), digit: 11 })
              : (this.statusInputPhoneNumber == REMOVE_DASH
                ? this.$tc('message.msg014_remote_dash', false, { field: this.$t('updateInfomation.lb_cell_phone'), digit: 11 })
                : this.$t('validation.format', { field: this.$t('updateInfomation.lb_cell_phone') })
              )
          },
          post_code: {
            numeric: this.$t('validation.numeric', { field: this.$t('updateInfomation.lb_zipcode') }),
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_zipcode') }),
            length: this.$t('validation.length', { field: this.$t('updateInfomation.lb_zipcode'), number: 7 }),
          },
          post_code_1: {
            length: this.$t('validation.length', { field: this.$t('updateInfomation.lb_zipcode_3'), number: 3 }),
          },
          post_code_2: {
            length: this.$t('validation.length', { field: this.$t('updateInfomation.lb_zipcode_4'), number: 4 }),
          },
          city: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_city') }),
          },
          district: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_district') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_district'), value: 200 }),
            fullsize: this.$t('validation.fullsize', { field: this.$t('updateInfomation.lb_district') }),
          },
          address: {
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_detail_address') }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_detail_address'), value: 400 }),
            fullsize: this.$t('validation.fullsize', { field: this.$t('updateInfomation.lb_detail_address') }),
          },
          building_room: {
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_building_room'), value: 400 }),
            fullsize: this.$t('validation.fullsize', { field: this.$t('updateInfomation.lb_building_room') }),
          },
          birthday: {
            afterDate: this.$t('validation.after', { field: this.$t('updateInfomation.lb_birthday'), attribute: '1920-01-01' }),
            required: this.$t('validation.required', { field: this.$t('updateInfomation.lb_birthday') }),
            dateFormat: this.$t('validation.date_format', { field: this.$t('updateInfomation.lb_birthday'), attribute: 'YYYY-MM-DD' }),
            beforeDate: this.$t('validation.before', {
              field: this.$t('updateInfomation.lb_birthday'),
              attribute: new Date().toISOString().replace(/T[0-9a-zA-Z:.]+/g, '')
            })
          },
          year: {
            numeric: this.$t('validation.numeric', { field: this.$t('updateInfomation.lb_year') }),
            length: this.$t('validation.length', { field: this.$t('updateInfomation.lb_year'), number: 4 })
          },
          month: {
            numeric: this.$t('validation.numeric', { field: this.$t('updateInfomation.lb_month') }),
            lengthBetween: this.$t('validation.length', { field: this.$t('updateInfomation.lb_month'), number: 2 })
          },
          day: {
            numeric: this.$t('validation.numeric', { field: this.$t('updateInfomation.lb_day') }),
            lengthBetween: this.$t('validation.length', { field: this.$t('updateInfomation.lb_day'), number: 2 })
          },
          password: {
            min: this.$t('validation.min', { field: this.$t('updateInfomation.lb_password'), value: 8 }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_password'), value: 16 }),
            confirmed: this.$t('validation.confirmed', { field: this.$t('updateInfomation.lb_password') }),
            passwordRegex: this.$t('validation.passwordRegex', { field: this.$t('updateInfomation.lb_password') }),
          },
          confirmed_password: {
            min: this.$t('validation.min', { field: this.$t('updateInfomation.lb_confirm_password'), value: 8 }),
            max: this.$t('validation.max', { field: this.$t('updateInfomation.lb_confirm_password'), value: 16 }),
          },
          member_code: {
            textNumberHaftSize: this.$t('validation.textNumberHaftSize', { field: this.$t('updateInfomation.lb_member_code') }),
            existsMemberCode: this.$t('message.msg065_not_exists_member_code'),
            existsOrder: this.$t('message.msg035_order_exists.line_1') + '<br />' + this.$t('message.msg035_order_exists.line_2')
          }
        }
      }

      this.$validator.localize('ja', dict);
    },

    /**
     * Funtion show popup confirm stop change infomation
     *
     * @return {void}
     */
    showPopUp: function () {
      $('#confirm-stop-update-infomation').modal('show');
    },

    /**
     * Function redirect to top my page
     *
     * @return {void}
     */
    goToMyPage: function () {
      this.$store.dispatch('member/unsetMember');

      let path = this.$router.resolve({
        name: this.routerToMyPage,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    }
  }
}
