/**
 * File CompleteUpdateBusiness.js
 * Hanlde init page complate update member infomation
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-22
 */

import { mapState } from 'vuex';
import constant from '@/constant';
import ClientInfo from "@/components/UI/ClientInfo";

export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('updateInfomation.lb_title_complete_update')
    }
  },
  data:() => ({
    model: {
      inquiryNm: '',
      inquiryTelNo: '',
      inquiryUrl: '',
      inquiryNote: ''
    },
    pathToHomePage: '',
  }),
  beforeRouteEnter (to, from, next) {
    // Check is valid action go to from page 440 to page 450 then setting localstorage
    if (from.name != constant.router.CONFIRM_UPDATE_INFOMATION) {
      localStorage.setItem('not_valid_step', true);
    }

    next();
  },
  created() {
    // Check is valid action go to from page 440 to page 450
    if (!!localStorage.getItem('not_valid_step')) {
      localStorage.removeItem('not_valid_step');

      let path = this.$router.resolve({
        name: constant.router.UPDATE_INFOMATION,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);

      return;
    }

    this.model.inquiryNm = this.client.inquiry_nm;
    this.model.inquiryTelNo = this.client.inquiry_tel_no;
    this.model.inquiryUrl = this.client.inquiry_url;
    this.model.inquiryNote = this.client.inquiry_notes;
  },
  computed: {
    ...mapState({
      client: state => state.client.clientInfo
    })
  },
  methods: {
    /**
     * Function redirec to my page
     *
     * @returns {void}
     */
    goToMyPage: function () {
      let path = this.$router.resolve({
        name: constant.router.MY_PAGE,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    }
  },
  components: {
    ClientInfo
  }
}
