/**
 * File ForfotPasswordBusiness.js
 * Check email and phone to send URL setting password to user
 *
 * @author Rikkei.DucVN
 * @date 2018-10-02
 */

import constant from '@/constant';
import { post } from '@/plugins/api';

export default {
  name: 'ForgotPassword',
  layout: 'default',
  middleware: 'redirect_if_authenticated',
  head() {
    return {
      title: this.$t('login.lb_forgot_password_head')
    }
  },
  data() {
    return {
      error: false,
      click_login: false,
      email: '',
      phone: '',
      client_id: '',
    }
  },
  created() {
    this.renderMsgErr();
  },
  methods: {
    /**
     * Function go to back page
     *
     * @returns {void}
     */
    back() {
      this.$router.go(-1);
    },

    /**
     * Function submit form to send data to API forgot password
     *
     * @returns {void}
     */
    onSubmit() {
      this.$validator.validateAll().then((result) => {
        if (result) {
          this.$nuxt.$loading.start();
          this.error = false;
          this.click_login = true;

          // Post data to API by Axios
          return post(constant.api.FORGOT_PASSWORD_API, {
            email: this.email,
            phone: this.phone,
            client_id: this.$route.params.client_id,
          }).then(result => {
            this.$nuxt.$loading.finish();
            this.click_login = false;
            if (result.data.data.result||result.status) {
              this.$router.push({name: constant.router.COMPLETE_SEND_EMAIL_PASS});
            }
          }).catch(e => {
            this.$nuxt.$loading.finish();
            this.click_login = false;
            this.error = true;
          });
        }
      }).catch(() => {
        return false;
      });

    },

    /**
     * Function overider message validator
     *
     * @returns {void}
     */
    renderMsgErr: function() {
      const dict = {
        custom: {
          email: {
            required: this.$t('validation.required', { field: this.$t('login.lb_login_email') }),
            mail: this.$t('validation.email', { field: this.$t('login.lb_login_email') }),
          },
          phone: {
            required: this.$t('validation.required', { field: this.$t('login.lb_home_phone') + this.$t('login.lb_phone') }),
            regex: this.$t('validation.regex', { field: this.$t('login.lb_home_phone') + this.$t('login.lb_phone') }),
            min: this.$t('validation.min', { field: this.$t('login.lb_home_phone'), value: 10 }),
            max: this.$t('validation.max', { field: this.$t('login.lb_home_phone'), value: 20 })
          }
        }
      }
      this.$validator.localize('ja', dict);
    },
    validate() {
      this.error = false;
      this.$validator.validateAll().catch(() => {
        return false;
      });
    }
  }
}
