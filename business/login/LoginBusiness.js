/**
 * File Loginbusiness.js
 * Define request and response in api login
 *
 * @author Rikkei.DucVN
 * @date 2018-10-02
 */

import { mapActions, mapState } from 'vuex';
import constant from '@/constant';
import { get } from '@/plugins/api';

export default {
  name: 'Login',
  layout: 'default',
  middleware: 'redirect_if_authenticated',
  head() {
    return {
      title: this.$t('login.lb_login_head')
    }
  },
  data() {
    return {
      error: false,
      click_login: false,

      // Variable to replace button "List Perform" with button "Back"
      selectChair: this.$store.state.auth.redirectURL?true:false,
      mail: '',
      password: '',
      client_id: '',
      // Variable of login admin mode
      web_permission_kb: this.$store.state.auth.admin_flag
    }
  },
  created() {
    this.renderMsgErr();
  },
  computed: {
    ...mapState({
      auth: state => state.auth.user
    })
  },
  methods: {
    ...mapActions('auth', [
      'login',
      'logout'
    ]),

    /**
     * Function go to Back page
     */
    back() {
      this.$router.go(-1);
    },

    /**
     * Function go to Lits Perform  for login
     */
    list() {
      this.$router.push({name: constant.router.LISTPERFORM});
    },

    /**
     * Function submit form for login
     *
     * @returns {void}
     */
    onSubmit() {
      this.$validator.validateAll().then((valid) => {
        if (valid) {
          this.$nuxt.$loading.start()

          // Get user input
          let user = {
            mail: this.mail,
            password: this.password,
            client_id: this.$route.params.client_id,
            web_permission_kb: this.web_permission_kb
          };
          this.error = false;
          this.clickLogin = true;

          // Set the target url when we Login successful
          let url = constant.router.BASE_URL_NAME;

          // Check stay in screen SELECT_TICKET we must change target URL
          if (this.$store.state.auth.redirectURL == constant.router.SELECT_TICKET_NAME
            || this.$store.state.auth.redirectURL == constant.router.SELECT_SEAT_NAME) {
            url = this.$store.state.auth.redirectURL;
          }

          // Remove all infomation
          this.$store.dispatch('member/unsetMember');
          this.$store.dispatch('register/removeModel');
          this.$store.dispatch('register/updateStepOne', false);
          this.$store.dispatch('register/updateStepTwo', false);
          localStorage.removeItem('is_checked_both');

          // Call function login to Login
          this.login(user).then((res) => {
            this.clickLogin = false;

            // Redirect error page when Black_cd =1
            if (this.$store.state.auth.redirect_URL_BLACKCD == constant.router.ERROR) {
              url = constant.router.ERROR_BLACK_CD;

              this.$router.push({name: url, params: { id: this.$store.state.auth.id }, query: this.$store.state.auth.query });
              this.$nuxt.$loading.finish();

              return false;
            }

            return true;
          }).then(status => {
            if (status) {
              this.getCartWhenLogin();
            }
          }).then(status => {
            // Redirect to the target URL
            this.$router.push({name: url, params: { id: this.$store.state.auth.id }, query: this.$store.state.auth.query });
            this.$nuxt.$loading.finish();

            return;
          }).catch(err => {
            this.$nuxt.$loading.finish();

            if (!err.response.data.data.data.type) {
              this.$store.dispatch('auth/setError', [
                this.$t('message.msg090_lock_account.line_1'),
                this.$t('message.msg090_lock_account.line_2')
              ]);

              let path = this.$router.resolve({
                name: constant.router.ERROR_NAME,
                params: { client_id: this.$route.params.client_id }
              });

              this.$router.push(path.href);

              return;
            }

            this.error = true;
            this.clickLogin = false;
          });
        }
      }).catch(() => {
        return false;
      });
    },

    /**
     * Function check exsits cart and get cart after login
     *
     * @returns {boolean}
     */
    getCartWhenLogin: function () {
      const IS_EXISTS_CART = 0;

      return get(constant.api.BOOKING_GET_CART,{
        client_id: this.$route.params.client_id,
        member_id: this.auth.member_id
      }).then(response => {
        let data = response.data.data;

        if (data.check_flg == IS_EXISTS_CART) {
          this.$store.dispatch('booking/setCart', data.cart_id);

          return get(constant.api.CART_DETAIL, {
            client_id: this.$route.params.client_id,
            cart_id: data.cart_id
          }).then(response => {
            let cartDetail = response.data.data;
            this.$store.dispatch('booking/setCartDetail', cartDetail);

            return true;
          });
        }

        return true;
      }).catch(err => {
        this.$store.dispatch('auth/logout', {
          client_id: this.$route.params.client_id,
          member_id: this.$store.state.auth.user.member_id
        });

        // cart dont't exit move to error page
        this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);

        let path = this.$router.resolve({
          name: constant.router.ERROR_NAME,
          params: { client_id: this.$route.params.client_id, }
        });

        this.$router.push(path.href);
      });
    },

    /**
     * Function overider message validator
     *
     * @returns {void}
     */
    renderMsgErr: function() {
      const dict = {
        custom: {
          loginMail: {
            required: this.$t('validation.required', { field: this.$t('login.lb_ID') })
          },
          loginPwd: {
            required: this.$t('validation.required', { field: this.$t('login.lb_login_password') }),
            passwordRegex: this.$t('validation.passwordRegex', { field: this.$t('login.lb_login_password') }),
          }
        }
      }
      this.$validator.localize('ja', dict);
    },

    /**
     * Function check validate
     *
     * @return {void}
     */
    validate() {
      this.error = false;
      this.$validator.validateAll().catch(() => {
        return false;
      });
    }
  }
}
