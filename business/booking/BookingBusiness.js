/**
 * File BookingBusiness.js
 * Hanlde init page booking
 *
 * @author Rikkei.ThienNB
 * @date 2018-11-01
 */

import Warning from "@/components/Booking/Warning"
import TicketInfo from "@/components/Booking/TicketInfo"
import TicketContent from "@/components/Booking/TicketContent"
import TicketContentSeat from "@/components/Booking/TicketContentSeat"
import TicketBookingSeat from "@/components/Booking/TicketBookingSeat"
import TicketSummary from "@/components/Booking/TicketSummary"
import LoginModal from "@/components/Navigation/TheLoginModal"
import WarningModal from "@/components/Navigation/TheWarningModal"
import {mapState, mapGetters} from 'vuex';
import constant from '@/constant';
import Config from "@/constant/config"
import {get, post} from '@/plugins/api';

export default {
  middleware: 'guest',
  head() {
    return {
      title: this.$t('booking.header'),
    }
  },
  components: {
    Warning,
    LoginModal,
    WarningModal,
    TicketInfo,
    TicketContent,
    TicketContentSeat,
    TicketBookingSeat,
    TicketSummary
  },
  computed: {
    ...mapGetters({
      myTickets: 'booking/myTickets',
      isLogin: 'auth/isLogin',
      memberId: 'auth/getMemberId',
      memberKbNo: 'auth/getMemberKbNo',
      memberTypeNo: 'auth/getMemberTypeNo',
      cartId: 'booking/cartId',
      checkMemberValid: 'auth/checkMemberValid',
      cartDetail: 'booking/cartDetail',
      adminTime: 'auth/getAdminTime',
      unit: 'auth/getUnit'
    }),
    haveMemberDiscount() {
      let result = false;
      $.each(this.dataSeatType.seats, function (seatName, seatInfo) {
        if (seatInfo.member_discount_flg == 1) {
          result = true;
          return true;
        }
      });

      return result;
    },
    sumNumberTicket() {
      let numTicket = 0;
      let route = this.$route;
      this.myTickets.forEach(function (el) {
        if (el.client_id == route.params.client_id
          && el.show_group_id == route.query.show_group_id
          && el.show_no == route.query.show_no
          && el.ticket_price > 0) {
          numTicket += el.number_ticket * 1;
        }
      });
      return numTicket;
    },
    myTicketsInShow() {
      let seats = [];
      let route = this.$route;
      this.myTickets.forEach(function (el) {
        if (el.client_id == route.params.client_id
          && el.show_group_id == route.query.show_group_id
          && el.show_no == route.query.show_no
          && el.ticket_price > 0) {
          seats.push(el);
        }
      });
      return seats;
    },
    myTicketsInShowBefore() {

      //function for load ticket in cart to my ticket
      let seats = [];
      let route = this.$route;
      this.cartDetail.forEach( el => {

        if (el.client_id == route.params.client_id
          && el.show_group_id == route.query.show_group_id
          && el.show_no == route.query.show_no
          && el.sales_no == route.query.sales_no
          && el.standard_ticket_price > 0) {
          let ticketInCart = {
            client_id: el.client_id,
            number_specified_flg: this.setNumberSpecified(el.seat_type_kb,el.internet_seat_kb),
            number_ticket: parseInt(el.ticket_count),
            sales_kb: el.sales_kb,
            sales_no: el.sales_no,
            seat_nm: el.seat_nm,
            seat_no: parseInt(el.seat_no),
            seat_type_kb: el.seat_type_kb,
            seat_type_nm: el.seat_type_nm,
            seat_type_no: parseInt(el.seat_type_no),
            show_group_id: el.show_group_id,
            show_no: el.show_no,
            ticket_price: parseInt(el.standard_ticket_price),
            ticket_type_nm: el.ticket_type_nm,
            ticket_type_no: parseInt(el.ticket_type_no),
          }
          seats.push(ticketInCart);
        }
      });
      if (seats.length > 0) {
        let deleteTicketData = {
          show_group_id: this.$route.query.show_group_id,
          show_no: this.$route.query.show_no,
          client_id: this.$route.params.client_id,
          sales_no: this.$route.query.sales_no,
          seat_type_kb: Config.SEAT_DESIGNATED
        };

        this.$store.dispatch("booking/clearBooking");
        seats.forEach(itemSeat => {

          if (itemSeat.number_specified_flg == 0) {
            this.addNewSeat(1, itemSeat);
          }
          if (itemSeat.number_specified_flg == 1) {

            if (itemSeat.number_ticket > 0) {
              this.$store.dispatch("booking/addTicket", itemSeat);
            }
          }
        })
      }

      return this.myTicketsInShow;
    }

  },

  data() {
    return {
      dataTicket: {},
      dataSeatType: {},
      dataSeatMap: [],
      message: '',
      validate: true,
      processing: false
    }
  },
  mounted: function () {
    //hiddenfield for save ticket in cart
    $('#booking-before').val(JSON.stringify(this.myTicketsInShowBefore))
  },
  created() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();

    });
    this.initPage();

    this.checkSalesTerm();

  },
  methods: {
    /**
     * Function init page get ticket info
     *
     * @returns {Array}
     */
    initPage: function () {
      get(constant.api.BOOKING_INFO, {
        client_id: this.$route.params.client_id,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        admin_time: this.adminTime
      })
        .then(result => {
          this.dataTicket = result.data.data;
          //finish loading
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);
          this.$store.dispatch('auth/setError', [
            this.$t('message.msg085_exception.line_1'),
            this.$t('message.msg085_exception.line_2'),
          ]);
          let path = this.$router.resolve({
            name: constant.router.ERROR_NAME,
            params: {client_id: this.$route.params.client_id}
          });
          this.$router.push(path.href);
        });

      get(constant.api.TICKET_INFO, {
        client_id: this.$route.params.client_id,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        sales_no: this.$route.query.sales_no,
        cart_id : this.cartId
      })
        .then(result => {
          this.dataSeatType = result.data.data;
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);
          this.$store.dispatch('auth/setError', [
            this.$t('message.msg085_exception.line_1'),
            this.$t('message.msg085_exception.line_2'),
          ]);
          let path = this.$router.resolve({
            name: constant.router.ERROR_NAME,
            params: {client_id: this.$route.params.client_id}
          });
          this.$router.push(path.href);
        });
    },
    checkSalesTerm: function () {
      get(constant.api.BOOKING_SALES_TERM, {
        client_id: this.$route.params.client_id,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        sales_no: this.$route.query.sales_no
      })
        .then(result => {
          let sales_flg = result.data.data.sales_flg;
          if (sales_flg == 0) {
            this.$store.dispatch('auth/setError', [this.$t('message.msg004_error_sales_term')]);
            this.$router.push({name: constant.router.ERROR_NAME});
          }

        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);
          this.$store.dispatch('auth/setError', [
            this.$t('message.msg085_exception.line_1'),
            this.$t('message.msg085_exception.line_2'),
          ]);
          let path = this.$router.resolve({
            name: constant.router.ERROR_NAME,
            params: {client_id: this.$route.params.client_id}
          });
          this.$router.push(path.href);
        });

    },
    checkLimitTicket() {
      let valid = true;
      if (this.sumNumberTicket > 0) {
        return get(constant.api.BOOKING_CHECK_LIMIT, {
          client_id: this.$route.params.client_id,
          show_group_id: this.$route.query.show_group_id,
          show_no: this.$route.query.show_no,
          sales_no: this.$route.query.sales_no,
          seat_select_count: this.sumNumberTicket,
          member_id: this.memberId,
        })
          .then(result => {
            let data = result.data.data;

            if (data.day_entry_limit_count_ng_flg == 1) {
              this.message = data.day_entry_limit_count_ng_flg_msg;
              this.validate = false;
              valid = false;
              $('#theWarningModal').modal('show');
            } else if (data.once_purchase_limit_count_ng_flg == 1) {
              this.message = data.once_purchase_limit_count_ng_flg_msg;
              this.validate = false;
              valid = false;
              $('#theWarningModal').modal('show');
            } else if (data.purchase_limit_count_ng_flg == 1) {
              this.message = data.purchase_limit_count_ng_flg_msg;
              this.validate = false;
              valid = false;
              $('#theWarningModal').modal('show');
            } else if (data.first_limit_count_ng_flg == 1) {
              this.message = data.first_limit_count_ng_flg_msg;
              this.validate = false;
              valid = false;
              $('#theWarningModal').modal('show');
            }

            return valid;
          })
          .catch(err => {
            // Will be redirect to page error 570 later
            console.log(err);
          });
      } else {

        return false;
      }


    },
    setNumberSpecified: function(seatTypeKb,internetSeatKb){
      let numberSpecFlg = 0;
      if(seatTypeKb == 2){
        numberSpecFlg = 1;
      }
      if(seatTypeKb ==1 && (internetSeatKb  == 2 || internetSeatKb  == 3 )){
        numberSpecFlg = 1;
      }

      return numberSpecFlg;
    },
    addNewSeat: function (numTicket, ticket) {
      let ticketInfo = {
        ticket_type_no: ticket.ticket_type_no,
        ticket_type_nm: ticket.ticket_type_nm,

        seat_no: ticket.seat_no,
        seat_nm: ticket.seat_nm,

        seat_type_no: ticket.seat_type_no,
        seat_type_nm: ticket.seat_type_nm,
        ticket_price: ticket.ticket_price,
        number_ticket: numTicket,
        seat_type_kb: ticket.seat_type_kb,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        client_id: this.$route.params.client_id,
        sales_no: this.$route.query.sales_no,
        sales_kb: ticket.sales_kb,
        number_specified_flg: 0
      }

      this.$store.dispatch("booking/addTicket", ticket);
    },
    getCartId() {
      return get(constant.api.BOOKING_GET_CART, {
        client_id: this.$route.params.client_id,
        member_id: this.memberId,
        create_flg: true
      })
        .then(result => {
          let cart_id = result.data.data.cart_id;
          let check_flg = result.data.data.check_flg;
          this.$store.dispatch('booking/setCart', cart_id)

          return check_flg;
        })
    },

    checkSeatBooking() {
      let valid = true;
      let seats = [];
      let route = this.$route;
      this.myTickets.forEach(function (el) {
        if (el.client_id == route.params.client_id
          && el.show_group_id == route.query.show_group_id
          && el.show_no == route.query.show_no
          && el.ticket_price > 0) {
          seats.push(el);
        }
      });

      return post(constant.api.BOOKING_CHECK_SEAT, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        sales_no: this.$route.query.sales_no,
        seats: seats,
        member_kb_no: this.memberKbNo,
        membertype_no: this.memberTypeNo,
        member_id: this.memberId,
        unit: this.unit
      })
        .then(result => {
          let data = result.data.data;

          if (data.result_code == 1) {
            valid = true;
          } else if (data.result_code == 3) {

            this.message = this.$t('message.msg044_save_designate_seat_fail.line_1') + '\n' + this.$t('message.msg044_save_designate_seat_fail.line_2');
            this.validate = false;
            valid = false;
            $('#theWarningModal').modal('show');
          } else if (data.result_code == 8) {
            //TODO : unselect seat
            this.message = this.$t('message.msg043_save_free_seat_fail');
            this.validate = false;
            valid = false;
            $('#theWarningModal').modal('show');
          } else if (data.result_code == 9) {
            valid = false;
            this.$store.dispatch('auth/setError', ['']);
            this.$router.push({name: constant.router.ERROR_NAME});
          }

          return valid;
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);
        });

    },
    updateSeatInCart() {
      let valid = true;
      let route = this.$route;
      let bookingBefore = JSON.parse($('#booking-before').val());
      let bookingAfter = this.myTicketsInShow;
      let cartDetail = this.cartDetail;
      bookingBefore.forEach(function (seat) {
        cartDetail.forEach(function (cart) {
          if (cart.seat_type_no == seat.seat_type_no
            && cart.ticket_type_no == seat.ticket_type_no
            && cart.client_id == route.params.client_id
            && cart.show_group_id == route.query.show_group_id
            && cart.show_no == route.query.show_no
            && cart.sales_no == route.query.sales_no
          ) {
            seat.cart_seq = cart.cart_seq;
            seat.seat_no = cart.seat_no;
            seat.sales_kb = cart.sales_kb;
          }
        })
      });

      let uniqueResultDelete = bookingBefore.filter(function (obj) {
        return !bookingAfter.some(function (obj2) {
          if (obj.number_specified_flg == 0) {
            return obj.seat_no == obj2.seat_no;
          }
          if (obj.number_specified_flg == 1) {
            return obj.ticket_type_no == obj2.ticket_type_no
              && obj.seat_type_no == obj2.seat_type_no
              && obj.number_ticket == obj2.number_ticket;
          }
        });
      });
      let uniqueResultAdd = bookingAfter.filter(function (obj) {
        return !bookingBefore.some(function (obj2) {
          if (obj.number_specified_flg == 0) {
            return obj.seat_no == obj2.seat_no;
          }
          if (obj.number_specified_flg == 1) {
            return obj.ticket_type_no == obj2.ticket_type_no
              && obj.seat_type_no == obj2.seat_type_no
              && obj.number_ticket == obj2.number_ticket;
          }
        });
      });

      return post(constant.api.BOOKING_UPDATE_SEAT, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId,
        show_group_id: this.$route.query.show_group_id,
        show_no: this.$route.query.show_no,
        sales_no: this.$route.query.sales_no,
        seats_delete: uniqueResultDelete,
        seats_new: uniqueResultAdd,
        member_kb_no: this.memberKbNo,
        membertype_no: this.memberTypeNo,
        member_id: this.memberId,
        unit: this.unit
      })
        .then(result => {
          return true;
        })
        .catch(err => {
          return false;
        })

    },

    onNextBtn: async function () {
      // check if processing to prevent duplication
      if (this.processing == true) {
        return;
      }
      this.processing = true;

      let checkLimit = await this.checkLimitTicket();

      if (checkLimit == true) {

        //Get new cartId if dont have
        let getCartFlg = await this.getCartId();
        if (getCartFlg == 1) {
          // Cart not exit create new one
          //check and booking seat to databse
          let checkSeat = await this.checkSeatBooking();
          if (checkSeat == true) {
            // click button next after real time
            this.$emit('appSync');

            let path = this.$router.resolve({
              name: constant.router.CART,
              params: {client_id: this.$route.params.client_id}
            });

            this.$router.push(path.href);
            this.processing = false;
          }

        } else {
          let updateCart = await this.updateSeatInCart();
          if (updateCart) {
            // click button next after real time
            this.$emit('appSync');

            let path = this.$router.resolve({
              name: constant.router.CART,
              params: {client_id: this.$route.params.client_id}
            });

            this.$router.push(path.href);
            this.processing = false;
          }
        }

      }
      this.processing = false;
      this.$emit('nextBtnClick', true);
    },
    onBackBtn: function () {
      var url ={};
      if (this.dataTicket.show_group_disp_kb == 1) {
        url = {
          name: constant.router.LISTPERFORM,
          params: {client_id: this.$route.params.client_id}
        }
      } else {
        url = {
          name: constant.router.SHOW_SCHEDULE_LIST,
          params: {client_id: this.$route.params.client_id, show_group_id: this.$route.query.show_group_id}
        }
      }
      // add param flg if search
      if(this.$route.query.searching){
        url.query ={'searching':true} ;
      }
      let path =  this.$router.resolve(url);
      this.$router.push(path.href);
      this.$emit('backBtnClick', true);
    },
    async validated(status) {
      alert(status);
    }
  }

};
