/**
 * File ErrorCommonBusiness.js
 * Show error
 *
 * @author Rikkei.DucVN
 * @date 2018-10-12
 */

import constant from '@/constant';
import { post } from '@/plugins/api';
import ClientInfo from '@/components/UI/ClientInfo';

export default {
  name: 'ErrorCommonBusiness',
  layout: 'default',
  head() {
    return {
      title: this.$t('common.lb_title_error')
    }
  },
  data() {
    return {
      messenge_error: '',
      clientId: '',
      model: {
        inquiry_nm: '',
        inquiry_tel_no: '',
        inquiry_url: '',
        inquiry_notes: ''
      }
    }
  },
  methods: {

    /**
     * Function go to Lits Perform
     *
     * @returns {void}
     */
    list() {
      this.$router.push({name: constant.router.LISTPERFORM});
    },

     /**
     * Function to load error messenger
     *
     * @returns {void}
     */
    loadError() {
      this.messenge_error = this.$store.state.auth.error
    },

    /**
     * Function load info client to display
     *
     * @returns {void}
     */
    onLoad() {
      // Post data to API by Axios
      return post(constant.api.GET_CLIENT_INFO, {
        client_id: this.$route.params.client_id,
      }).then(result => {
        this.model.inquiry_nm = result.data.data.inquiry_nm;
        this.model.inquiry_url = result.data.data.inquiry_url;
        this.model.inquiry_notes = result.data.data.inquiry_notes;
        this.model.inquiry_tel_no = result.data.data.inquiry_tel_no;
      });
    }
  },
  beforeMount(){
    this.onLoad();
    this.loadError();
  },
  components: {
    ClientInfo
  }
}
