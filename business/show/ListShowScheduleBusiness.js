/**
 * File ListShowScheduleBusiness.js
 * Handle list show schedule business
 *
 * @author Rikkei.DungLV
 * @date 2018-10-30
 */

import ItemShowSchedule from '@/components/Show/ItemShowSchedule';
import ItemShowScheduleMain from '@/components/Show/ItemShowScheduleMain';
import {post, get} from '@/plugins/api';
import _api from '@/constant/api';
import Config from '@/constant/config';
import Router from '@/constant/router';
import {mapState, mapGetters} from 'vuex';

export default {
  name: 'Schedule',
  middleware: 'guest',
  components: {
    ItemShowSchedule,
    ItemShowScheduleMain
  },
  head() {
    return {
      title: this.$t('show.title_schedule')
    }
  },
  data() {
    return {
      constant: {
        EXPIRED_SHOW: 1,
        EXPIRED_HIDE: 0,
        SEAT_SELECTION: 1,
        BUTTON_SELECT_SHOW: 1,
        BUTTON_SELECT_HIDE: 0,
        WITH_SELECT_SEAT: Config.LAYOUT_WITH_SEAT,
        NO_SELECT_SEAT: Config.LAYOUT_NO_SEAT
      },
      schedule: null,
      loading: true,
      seeExpired: false,
      page: {
        offset: 0,
        limit: Config.RECORD_PER_PAGE
      },
      // List object show_group_id, sales_no, cart_id, to check method payment with show have show_group_id=1
      showCart: {
        checkMethodList: [],
        cartId: null
      }
    }
  },
  created() {
    this.$nextTick(() => {
      this.getListShowSchedule();
    })
  },
  computed: {
    ...mapGetters({
      searchInfo: 'show/getSearchInfo',
    }),
    ...mapState({
      // Detail cart in state
      cartData: state => state.booking.cartDetail || null,
      admin_time: state => state.auth.admin_time,
      cartId: state => state.booking.cartId || null
    }),

    /**
     * Get current client id
     * @return {*}
     */
    clientId() {
      return this.$route.params.client_id;
    },

    /**
     * Get show group id
     * @return {string} Show group id
     */
    showGroupId: {
      get: function () {
        return this.$route.params.show_group_id;
      },
      set: function (newValue) {
        return newValue;
      }
    },

    /**
     * Get constant router
     * @return {object}
     */
    rsRouter() {
      return Router;
    },

    /**
     * Check user searching on show list page
     * @return {*}
     */
    searching(){
      return this.$route.query.searching ? true : null;
    },

    /**
     * Get list id show in cart from state
     * @return {Array} List id of show_group and sales in cart (show_group_{show_group_id}, sales_{sales_no})
     */
    showsInCart() {
      let tmpCart = [], cart = this.cartData;
      this.showCart.cartId = this.cartId || null;
      if (cart && cart.length > 0) {
        // Setting current cart id of user
        for (let i = 0; i < cart.length; i++) {
          tmpCart.push('show_group_'+cart[i].show_group_id+'_sales_'+cart[i].sales_no+'_show_' + cart[i].show_no);
          // Push list cart to check method
          this.showCart.checkMethodList.push({
            show_group_id: cart[i].show_group_id,
            sales_no: cart[i].sales_no
          });
        }
        return tmpCart;
      }
      return null;
    }
  },
  methods: {
    /**
     * Get list show schedule
     * @return {Promise}
     */
    getListShowSchedule() {
      this.$nuxt.$loading.start()
      post(_api.SHOW_LIST_SCHEDULE, {
        client_id: this.clientId,
        show_group_id: this.showGroupId,
        see_expired: this.seeExpired,
        offset: this.page.offset,
        limit: this.page.limit
      })
        .then(res => {
          if (res.data.data && res.data.data.record_num > 0) {
            this.schedule = res.data.data
          }
          this.loading = false
          this.$nuxt.$loading.finish()
        })
        .catch(err => {
          this.loading = false
          this.$nuxt.$loading.finish()
        })
    },

    /**
     * Add param search if user click search on header and redirect this page
     * @return {*}
     */
    redirectToHome() {
      // When user click button search, then next page.
      // After redirect home page, get list show back from search
      if (this.searching) {
        this.$store.dispatch('show/changeStatusSearch', true);
        return this.$router.push({name: Router.LISTPERFORM, params: {client_id: this.clientId}, query: this.searchInfo});
      }
      return this.$router.push({name: Router.LISTPERFORM, params: {client_id: this.clientId}});
    },

    /**
     * Get time admin when user login admin screen
     * @return {*}
     */
    getAdminTime() {
      if (this.admin_time) {
        let adDate = this.admin_time.date ? this.admin_time.date : '';
        let adHour = this.admin_time.hour ? this.admin_time.hour : '';
        let adMinute = this.admin_time.minute ? this.admin_time.minute : '';
        let adDateTime = adDate + ' ' + adHour + ':' + adMinute;
        let fmReg = new RegExp(/^(([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})\s([0-9]{1,2})\:([0-9]{1,2}))$/);
        if (fmReg.test(adDateTime)) {
          return adDateTime;
        }
      }
      return null;
    },

    /**
     * User click on button to cart on modal check method
     * @return {*}
     */
    redirectToCart(){
      $('#modalAlertShow').modal('hide');
      return this.$router.push({name: Router.CART, params: {client_id: this.$route.params.client_id}});
    }
  }
}
