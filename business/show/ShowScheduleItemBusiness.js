/**
 * File ShowScheduleBusiness.js
 * Handle business in show schedule item of show
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

import AApi from '@/constant/api';
import {get, post} from '@/plugins/api';

export default {
  name: "ItemShowSchedule",
  props: [
    'clientId',
    'showGroupId',
    'schedule',
    'rsRouter',
    'constant',
    'searching',
    'showsInCart',
    'seeExpired',
    'showCart'
  ],
  methods: {
    /**
     * Check exists show in cart and show icon
     *
     * @param show
     * @return {Boolean}
     */
    checkShowInCart(show) {
      // showsInCart contain list current show exists in cart
      if (show && this.showsInCart && this.showsInCart.length > 0) {
        return this.showsInCart.indexOf('show_group_' + show.show_group_id + '_sales_' + show.sales_no + '_show_' + show.show_no) >= 0
          ? true
          : false;
      }
      return false;
    },

    /**
     * Check valid method payment of cart
     * @param {object} groupShowCart
     */
    checkValidMethodOfCart(cartId, groupShowCart) {
      return new Promise((resolve, reject) => {
        post(AApi.SHOW_CHECK_VALID_METHOD_PAYMENT,
          {
            client_id: this.$route.params.client_id,
            cart_id: cartId,
            list_show_group: groupShowCart
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      })
    },

    /**
     * Handle check payment method and redirect of button select of show when click
     * Check show was selected and all show in current cart
     *
     * @param {Object} obj <Params oof url when redirect>
     * @param {Object} show <Show object>
     * @return {*}
     */
    clickButtonSelect(obj, show) {
      let url = this.renderLink(obj);
      // Check if exists cart, check method of current show in cart and show was selected
      if (!this.checkExistsCart()) {
        // Else next redirect to seat page
        return this.$router.push(url);
      }
      // @NOTE: Can't push normal object to list with element is Observe object, so need to convert list normal object
      let groupCart = JSON.parse(JSON.stringify(this.showCart.checkMethodList));
      // Start loading progress on top
      this.$nuxt.$loading.start();
      // If show have already in cart, then not push this again
      // Check this show already exists in cart, if not then push this to list show
      // Then, send this list show to API check method exists
      let existsShow = groupCart.filter(cart => {
        return (cart.show_group_id == show.show_group_id && cart.sales_no == show.sales_no);
      });
      if (!existsShow || existsShow && existsShow.length == 0) {
        // Get cart id and info of this show to check
        groupCart.push({show_group_id: show.show_group_id, sales_no: show.sales_no});
      }

      // Handle check exists and valid payment method
      return this.checkValidMethodOfCart(this.showCart.cartId, groupCart)
        .then(res => {
          this.$nuxt.$loading.finish();
          // If response return is exists invalid method, show modal info error
          if (res && res.data.data && res.data.data.exists_invalid_method) {
            $('#modalAlertShow').modal('show');
            return false;
          }
          // Else next redirect to seat page
          return this.$router.push(url);
        })
        .catch(err => {
          this.$nuxt.$loading.finish();
        });
    },

    /**
     * Render link base param object
     *
     * @param obj
     * @return {string}
     */
    renderLink(obj) {
      let url = this.$router.resolve(obj);
      if (url) {
        return url.href;
      }

      return '/'
    },

    /**
     * Check exists cart after user login
     * @return {boolean}
     */
    checkExistsCart() {
      if (this.showCart && this.showCart.cartId) {
        return true;
      }
      return false;
    }
  }
}
