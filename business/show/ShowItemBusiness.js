/**
 * File ShowItemBusiness.js
 * Handle business in item show
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

import Config from '@/constant/config';
import RRoute from '@/constant/router';
import AApi from '@/constant/api';
import {get, post} from '@/plugins/api';

export default {
  name: "ItemShow",
  props: ['show', 'constant', 'searching', 'showsInCart', 'showCart'],
  data() {
    return {
      rRoute: null
    }
  },
  computed: {
    clientId() {
      return this.$route.params.client_id
    }
  },
  created() {
    this.rRoute = RRoute;
  },
  methods: {
    /**
     * Get url from image
     * @return {String}
     */
    imageMainUrl: function (client_id, show_group_id) {
      return process.env.baseS3Url
        + Config.PATH_IMG_SHOW_MAIN.replace(':client_id', client_id).replace(':show_group_id', show_group_id);
    },

    /**
     * Remove image if image error
     * @return {Boolean}
     */
    loadImage(el) {
      $(this.$el).find('.rs-image').removeClass('no-image').addClass('no-image');
      $(this.$el).find('.img-load').remove();
      return false;
    },

    /**
     * Handle image with width and height auto
     * @param el
     */
    imageRetrieve(el) {
      this.$rsAutoScale(el);
    },

    /**
     * Check exists show in cart and show icon
     *
     * @param show
     * @return {Boolean}
     */
    checkShowInCart(show) {
      if (show && this.showsInCart && this.showsInCart.length > 0) {
        if (show.show_group_disp_kb == 1) {
          return this.showsInCart.indexOf('show_group_' + show.show_group_id + '_sales_' + show.sales_no + '_show_' + show.show_no) >= 0 ? true : false;
        } else {
          return this.showsInCart.indexOf('show_group_' + show.show_group_id) >= 0 ? true : false;
        }
      }
      return false;
    },

    /**
     * Check valid method payment of cart
     *
     * @param {string} cartId
     * @param {object} groupShowCart
     * @return {*}
     */
    checkValidMethodOfCart(cartId, groupShowCart) {
      return new Promise((resolve, reject) => {
        post(AApi.SHOW_CHECK_VALID_METHOD_PAYMENT,
          {
            client_id: this.$route.params.client_id,
            cart_id: cartId,
            list_show_group: groupShowCart
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      })
    },

    /**
     * Handle check payment method and redirect of button select of show when click
     * Check show was selected and all show in current cart
     *
     * @param {Object} obj <Params oof url when redirect>
     * @param {Object} show <Show object>
     * @return {*}
     */
    clickButtonSelect(obj, show) {
      let url = this.renderLink(obj);
      let groupCart = JSON.parse(JSON.stringify(this.showCart.checkMethodList));
      if (!this.checkExistsCart()) {
        // Else next redirect to seat page
        return this.$router.push(url);
      }
      // When show with show_group_disp_kb = 1 and have exists in cart, check exists payment method
      if (show && show.show_group_disp_kb == 1) {
        this.$nuxt.$loading.start();
        let existsShow = groupCart.filter(cart => {
          return (cart.show_group_id == show.show_group_id && cart.sales_no == show.sales_no);
        });

        // Check this show already exists in cart, if not then push this to list show
        // Then, send this list show to API check method exists
        if (!existsShow || existsShow && existsShow.length == 0) {
          // Get cart id and info of this show to check
          groupCart.push({show_group_id: show.show_group_id, sales_no: show.sales_no});
        }

        // Handle check exists and valid payment method
        return this.checkValidMethodOfCart(this.showCart.cartId, groupCart)
          .then(res => {
            this.$nuxt.$loading.finish();
            // If response return is exists invalid method, show modal info error
            if (res && res.data.data && res.data.data.exists_invalid_method) {
              $('#modalAlertShow').modal('show');
              return false;
            }
            // Else next redirect to seat page
            return this.$router.push(url);
          })
          .catch(err => {
            this.$nuxt.$loading.finish();
          });
      }

      // Else next redirect to seat page
      return this.$router.push(url);
    },

    /**
     * Convert param to string url
     *
     * @param obj
     * @return {string}
     */
    renderLink(obj) {
      let url = this.$router.resolve(obj);
      if (url) {
        return url.href;
      }
      return '/';
    },

    /**
     * Check exists cart
     * @return {boolean}
     */
    checkExistsCart() {
      if (this.showCart && this.showCart.cartId) {
        return true;
      }
      return false;
    }
  }
}
