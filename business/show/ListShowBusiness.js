/**
 * File ListShowBusiness.js
 * Handle list show business
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

import ItemShow from "@/components/Show/ItemShow";
import NotifyList from "@/components/Show/NotifyList";
import {mapState, mapGetters, mapActions} from 'vuex';
import {post, get} from "@/plugins/api";
import _api from "@/constant/api";
import _router from "@/constant/router";
import Config from "@/constant/config";

export default {
  name: "Index",
  middleware: 'guest',
  components: {
    ItemShow,
    NotifyList
  },
  head() {
    return {
      title: this.$t('show.title')
    }
  },
  data() {
    return {
      constant: {
        WITH_SELECT_SEAT: Config.LAYOUT_WITH_SEAT,
        NO_SELECT_SEAT: Config.LAYOUT_NO_SEAT,
        BUTTON_SELECT_SHOW: 1,
        BUTTON_SELECT_HIDE: 0
      },
      notifyList: [],
      notify_page: {
        currentPage: 1,
        totalItems: 1,
        itemsPerPage: Config.RECORD_PER_PAGE
      },
      // Paginate show
      showPage: {
        page: 1,
        bottom: false,
        limit: Config.RECORD_PER_PAGE
      },
      // List object show_group_id, sales_no, cart_id, to check method payment with show have show_group_id=1
      showCart: {
        checkMethodList: [],
        cartId: null
      }
    }
  },
  created() {
    this.initPrepareData();
  },
  computed: {
    ...mapState({
      show: state => state.show,
      // Detail cart in state
      cartData: state => state.booking.cartDetail || null,
      cartId: state => state.booking.cartId || null,
      admin_time: state => state.auth.admin_time
    }),
    ...mapGetters({
      totalRecord: 'show/getTotalRecordShow'
    }),

    /**
     * Show list items
     * @return {default.computed.shows|(function())|Array}
     */
    shows() {
      return this.show.shows
    },

    /**
     * Get status current of page is searching or don't searching
     * @return {boolean}
     */
    searching() {
      return this.show.searching
    },

    /**
     * Check user refresh of page
     * @return {boolean}
     */
    isRefresh() {
      return this.show.refresh
    },

    /**
     * Check user click button search on header or not
     * @return {boolean}
     */
    isClickBtnSearch() {
      return this.show.isClickBtnSearch;
    },

    /**
     * Get list id show in cart from state
     * @return {Array} List id of show_group and sales in cart (show_group_{show_group_id}, sales_{sales_no})
     */
    showsInCart() {
      let tmpCart = [], cart = this.cartData;
      this.showCart.cartId = this.cartId || null;
      if (cart && cart.length > 0) {
        // Setting current cart id of user
        for (var i = 0; i < cart.length; i++) {
          if (cart[i].show_group_disp_kb && cart[i].show_group_disp_kb == 1) {
            tmpCart.push('show_group_' + cart[i].show_group_id + '_sales_' + cart[i].sales_no + '_show_' + cart[i].show_no);
          } else {
            tmpCart.push('show_group_' + cart[i].show_group_id);
          }
          // Push list cart to check method
          this.showCart.checkMethodList.push({
            show_group_id: cart[i].show_group_id,
            sales_no: cart[i].sales_no
          });
        }
        return tmpCart;
      }
      return null;
    }
  },
  mounted: async function () {
    // Add event scroll window
    window.addEventListener('scroll', () => {
      this.showPage.bottom = this.bottomVisible();
    });
    // nextTick to call back after DOM already
    await this.$nextTick(() => {
      // If not search, select show from database
      this.getNotifies(1);
      this.addShow();
    });
  },
  methods: {
    ...mapActions('show', ['searchShow', 'listShow']),

    /**
     * Check bottom visible scroll loading
     * @return {*}
     */
    bottomVisible() {
      const scrollY = window.pageYOffset;
      const visible = document.documentElement.clientHeight;
      const showItemEl = $('.show-list .show-item');
      if (showItemEl.length > 0) {
        const lastShowItem = document.querySelector('.show-list .show-item:last-child');

        const posScroll = lastShowItem.offsetTop + lastShowItem.clientHeight;
        const bottomOfPage = visible + scrollY >= posScroll;

        // Check fixed footer scroll when loading
        if (showItemEl.length >= Config.MAX_RECORD_FIXED_FOOTER) {
          const posElementFixed = $('.show-list .show-item:nth-child(' + (Config.MAX_RECORD_FIXED_FOOTER - 1) + ')');
          if (posElementFixed) {
            let ps = $(".show-list .show-item:nth-child(" + (Config.MAX_RECORD_FIXED_FOOTER - 1) + ")")[0].offsetTop
              + $(".show-list .show-item:nth-child(" + (Config.MAX_RECORD_FIXED_FOOTER - 1) + ")")[0].clientHeight;
            // When load max item show >= 15 item, then fixed footer
            if ($('.show-list .show-item').length >= Config.MAX_RECORD_FIXED_FOOTER
              && (ps <= visible + scrollY)) {
              $('#mainFooter').addClass('rs-fixed');
            } else {
              $('#mainFooter').removeClass('rs-fixed');
            }
          }
        }

        // Check scroll to load more show
        return scrollY && (bottomOfPage || posScroll < visible);
      }
      return false;
    },

    /**
     * Add show when scroll
     * @return {mapActions}
     */
    async addShow() {
      let data = this.setData();
      this.$nuxt.$loading.start();
      await this.$store.dispatch('show/listShow', data)
        .then(res => {
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          this.$nuxt.$loading.finish();
        })
    },

    /**
     * Config data to search show
     * @return {object}
     */
    setData() {
      return {
        client_id: this.$route.params.client_id || null,
        key_search: this.$route.query.key_search || null,
        genre_no: this.$route.query.genre_no || null,
        from_show_date: this.$route.query.from_show_date || null,
        to_show_date: this.$route.query.to_show_date || null,
        from_sales_date: this.$route.query.from_sales_date || null,
        to_sales_date: this.$route.query.to_sales_date || null,
        showPage: this.showPage || null,
        admin_time: this.getAdminTime()
      }
    },

    /**
     * Prepare data to init page
     * @return void
     */
    initPrepareData() {
      this.showPage.page = 1;
      this.$store.dispatch('show/updatePage', this.showPage.page)
    },

    /**
     * Get list notify
     * @return {Promise}
     */
    getNotifies(pageNum) {
      get(_api.NOTIFY_LIST, {
        client_id: this.$route.params.client_id,
        admin_time: this.getAdminTime(),
        page: pageNum
      }).then(res => {
        const notifiesArray = [];
        for (var notifyItem in res.data.data.notify_list) {
          notifiesArray.push(res.data.data.notify_list[notifyItem]);
        }
        this.notifyList = notifiesArray;
        this.notify_page.totalItems = Number(res.data.data.record_num);
      }).catch(err => {
        throw new Error(err);
      })
    },

    /**
     * Check event when click page
     * @param pageNum
     * @return {*}
     */
    pageChanged(pageNum) {
      // event change page , get posts in page
      this.notify_page.currentPage = pageNum;
      this.getNotifies(pageNum);
    },

    /**
     * Get time admin when user login admin screen
     * @return {*}
     */
    getAdminTime() {
      if (this.admin_time) {
        let adDate = this.admin_time.date ? this.admin_time.date : '';
        let adHour = this.admin_time.hour ? this.admin_time.hour : '';
        let adMinute = this.admin_time.minute ? this.admin_time.minute : '';
        let adDateTime = adDate + ' ' + adHour + ':' + adMinute;
        let fmReg = new RegExp(/^(([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})\s([0-9]{1,2})\:([0-9]{1,2}))$/);
        if (fmReg.test(adDateTime)) {
          return adDateTime
        }
      }
      return null;
    },

    /**
     * Redirect to page cart
     */
    redirectToCart() {
      $('#modalAlertShow').modal('hide');
      return this.$router.push({name: _router.CART, params: {client_id: this.$route.params.client_id}});
    }
  },
  watch: {
    /**
     * Watch user scroll to bottom
     * @param bottom
     * @return {void|*}
     */
    'showPage.bottom'(bottom) {
      if (bottom) {
        this.showPage.page++;
        this.$store.dispatch('show/updatePage', this.showPage.page);
        this.addShow();
      }
    },

    /**
     * Watch user reload page
     * @param refresh
     * @return {void}
     */
    isRefresh(refresh) {
      if (refresh) {
        // When  user refresh page, init get data of show
        this.initPrepareData();
        this.addShow();
      }
    },

    /**
     * Watch user click button search
     * @param isClick
     */
    isClickBtnSearch(isClick) {
      if (isClick) {
        this.initPrepareData();
        this.addShow();
        this.$store.dispatch('show/updateClickBtnSearch', false);
      }
    }
  }
}
