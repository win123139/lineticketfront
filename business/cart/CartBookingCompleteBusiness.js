/**
 * CartBookingCompleteBusiness.js
 * Handle complete booking
 *
 * @author Rikkei.DungLV
 * @date 2018-10-22
 */
import _api from '@/constant/api';
import _router from '@/constant/router';
import {get, post} from "@/plugins/api"
import ClientInfo from "@/components/UI/ClientInfo"
import Breadcrumb from '@/components/Cart/Breadcrumb'
import {mapState, mapGetters} from 'vuex'

export default {
  name: "index",
  middleware: 'authenticated',
  components: {
    ClientInfo,
    Breadcrumb
  },
  head() {
    return {
      title: this.$t('booking.title_booking_complete')
    }
  },

  data() {
    return {
      step: 4,
      clientInfo: null
    }
  },

  computed: {
    ...mapState({
      reserveNo: state => state.booking.reserveNo || null
    }),
    ...mapGetters({
      clientInfoStore: 'client/getClient'
    })
  },

  mounted() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
      // If info of client is empty, call API and get from database
      if (!this.clientInfoStore) {
        this.setClientInfo()
          .then(res => {
            this.clientInfo = res;
            this.$nuxt.$loading.finish();
          })
          .catch(err => {
            this.$nuxt.$loading.finish();
            throw new Error(err);
          });
        // If exists data of client, get from cache
      } else {
        this.clientInfo = this.clientInfoStore;
        this.$nuxt.$loading.finish();
      }
    });
  },

  methods: {
    setClientInfo() {
      return new Promise((resolve, reject) => {
        post(_api.GET_CLIENT_INFO, {
          client_id: this.$route.params.client_id
        })
          .then(res => {
            resolve(res.data.data);
          })
          .catch(err => {
            reject(err);
          })
      });
    },

    /**
     * Append booking code to text extract content
     *
     * @param content
     * @param bookingCode
     * @return {void | string | *}
     */
    appendBookingCode(content, bookingCode) {
      return content.replace('[予約番号]', bookingCode);
    },

    /**
     * Redirect to home page
     * @return {*}
     */
    goToHome() {
      return this.$router.push({name: _router.LISTPERFORM, params: {'client_id': this.clientInfo.client_id}})
    }
  }
}
