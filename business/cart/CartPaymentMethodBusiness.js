/**
 * File CartPaymentMethodBusiness.js
 * Handle business in pyament method of cart
 *
 * @author Rikkei.DungLV
 * @date 2018-11-15
 */

import Breadcrumb from '@/components/Cart/Breadcrumb';
import PaymentMethodDelivery from '@/components/Cart/PaymentMethodDelivery';
import api from '@/constant/api';
import router from '@/constant/router';
import configDefine from '@/constant/config';
import {get, post} from '@/plugins/api';
import {mapState} from 'vuex';

export default {
  name: "index",
  head() {
    return {
      title: this.$t('booking.title_booking_method_payment')
    }
  },
  components: {
    Breadcrumb,
    PaymentMethodDelivery
  },
  middleware: 'authenticated',
  data() {
    return {
      errorMethod: null,
      config: {
        METHOD_DELIVERY_CODE: configDefine.PAYMENT_METHOD_DELIVERY_CODE,
        PAYMENT_CREDIT_CARD_CODE: configDefine.PAYMENT_METHOD_CREDIT_CARD_CODE,
        STOP_FLAG_TRUE: 1,
        STOP_FLAG_FALSE: 0,
        LENGTH_POST_CODE_1: 3,
        LENGTH_POST_CODE_2: 4,
        NAME_TYPE_METHOD_PAYMENT: 'payment',
        NAME_TYPE_METHOD_RECEIVE: 'receive'
      },
      feeMsgPayment: '',
      prefectureList: null,
      payment: {
        paymentList: null,
        paymentMethod: null,
        paymentMethodNm: null,
        receiveMethod: null,
        receiveMethodNm: null
      },
      delivery: {
        fullName: null,
        furigana: null,
        noPhone: null,
        postCode1: null,
        postCode2: null,
        postCode: null,
        prefecture: null,
        prefectureCode: null,
        province: null,
        otherAddress: null,
        noRoomBuild: null
      },
      cartId: null
    }
  },
  computed: {
    ...mapState({
      cartData: state => state.booking.cartDetail || null,
      adminTime: state => state.auth.admin_time || null,
      user: state => state.auth.user
    })
  },
  async created() {
    // Check exists or empty cart
    //1.カート情報取得(SQL036)を行い、カートが存在しない場合は、エラー画面(570)に遷移し、メッセージ（msg005)を表示する
    if (!this.checkExistsCart()) {
      this.redirectToError([
        this.$t('message.msg005_cart_not_exit')
      ]);
    }

    // Check valid cart to sync from session and database
    //2.セッションのカート情報と、DBのカート情報(SQL036)の整合性をチェックし、エラーの場合はメッセージ（msg048）を表示
    this.cartId = this.cartData[0].cart_id || null;
    let isValidCart = this.checkValidCart(this.cartId);

    // When not exist cart id and not valid cart, redirect to error page 570
    if (!this.cartId || !isValidCart) {
      this.redirectToError([
        this.$t('message.msg048_cart_has_changed')
      ]);
    }
  },

  mounted() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
      // 3.支払受取方法情報(SQL045)・予約・先行手数料の取得(SQL097)を取得を行い、支払受取方法がない場合はエラー画面（570）に遷移し、メッセージ（msg062）を表示す
      // Get list payment method base SQL045
      this.getPaymentMethodList(this.cartId)
        .then(res => {
          // Check exists payment method, if not found any payment method, redirect to error page and show msg062
          if (!res || !res.data.data || res.data.data.length == 0) {
            this.redirectToError([
              this.$t('message.msg062_invalid_method_payment')
            ]);
          }
          this.payment.paymentList = res.data.data || null;
          // 2. 予約・先行手数料の取得(SQL097)/Get fee message common show top page base on SQL097
          return this.getFeeMsgCommon(this.cartId);
        })
        .then(res => {
          this.feeMsgPayment = res.data.data[0] ? res.data.data[0].fee_msg_common : '';
          // 3. SQL001で「都道府県」一覧を取得
          return this.getPrefectureList();
        })
        .then(res => {
          this.prefectureList = res.data.data && res.data.data.length > 0 ? res.data.data : [];
          // When exists previous value of name prefecture, auto get code of prefecture
          if (this.delivery.prefecture) {
            this.delivery.prefectureCode = this.getCodePrefectureBaseName(this.delivery.prefecture);
          }
          // Reset method payment previous
          this.resetPaymentInfo();
          // Set info user login when after get data
          if (this.payment.receiveMethod
            && this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod) == this.config.METHOD_DELIVERY_CODE) {
            this.initFormDelivery(this.user);
          }
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          this.$nuxt.$loading.finish();
          throw new Error(err);
        });
    });
  },

  methods: {
    /**
     * Check cart exists and valid cart
     * @return {boolean}
     */
    checkExistsCart() {
      if (!this.cartData || this.cartData.length == 0) {
        return false;
      }
      return true;
    },

    /**
     * Check valid and sync cart from session and database
     * @param cartId
     * @return {Promise<void>}
     */
    checkValidCart(cartId) {
      return new Promise((resolve, reject) => {
        if (cartId && cartId != ' ') {
          get(api.CART_DETAIL, {
            client_id: this.$route.params.client_id,
            cart_id: cartId
          })
            .then(res => {
              let cartList = res.data.data;
              if (!cartList || cartList.length == 0) {
                resolve(false);
              }
              // Compare card in session and from db
              if (this.equalCartList(this.getIdAndSeqCart(cartList), this.getIdAndSeqCart(this.cartData))) {
                resolve(true);
              }

              resolve(false);
            })
            .catch(err => {
              throw new Error(err);
            })
        }
      })
    },

    /**
     * Check valid method code when user selected
     * @param {string} type <'payment': check valid payment method, 'receive': Check valid receive method>
     * @param {string} code
     * @return {boolean}
     */
    isValidMethodSelected(type, code) {
      if (type && code && this.payment.paymentList && this.payment.paymentList.length > 0) {
        if (type === 'payment') {
          let methods = this.payment.paymentList.find(function (el) {
            return el.code_no_006 == code;
          });
          if (methods) {
            return true;
          }
        }

        if (type === 'receive') {
          for (var i = 0; i < this.payment.paymentList.length; i++) {
            if (this.payment.paymentList[i].receive_list) {
              let recei = this.payment.paymentList[i].receive_list.find(el2 => {
                return el2.code_no_007 == code;
              });

              if (recei) {
                return true;
                break;
              }
            }
          }
        }
        return false;
      }
    },

    /**
     * Get lost Id and seq cart of cart
     * @param {array} cartList
     * @return {array}
     */
    getIdAndSeqCart(cartList) {
      return cartList.map((el) => {
        return {
          cart_id: el.cart_id.trim(),
          cart_seq: el.cart_seq.replace(/^\s+|\s+$/gm, '')
        }
      })
    },

    /**
     * Check mapping array cart from database and in session
     * @return {boolean}
     */
    equalCartList(array1, array2) {
      let isEqual = true, originArray = array2;
      if (array1 && array2 && array1.length == array2.length) {
        for (var i = 0; i < array1.length; i++) {
          let existItem = array2.find(el => {
            return el.cart_id == array1[i].cart_id && el.cart_seq == array1[i].cart_seq;
          });

          if (existItem) {
            originArray = originArray.filter(item => {
              return item.cart_id != existItem.cart_id && item.cart_seq != existItem.cart_seq;
            });
          } else {
            isEqual = false;
          }
        }

        if (originArray && originArray.length > 0) {
          isEqual = false;
        }
      } else {
        isEqual = false;
      }
      return isEqual;
    },

    /**
     * Get list payment method base on cart id
     *
     * @param {string} cartId
     * @return {Promise}
     */
    getPaymentMethodList(cartId) {
      return new Promise((resolve, reject) => {
        get(api.PAYMENT_GET_METHOD_LIST,
          {
            client_id: this.$route.params.client_id,
            cart_id: cartId,
            admin_time: this.getAdminTime()/*'2018/01/01 00:00'*/
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    /**
     * Get fee message common
     * @param cartId
     */
    getFeeMsgCommon(cartId) {
      return new Promise((resolve, reject) => {
        get(api.PAYMENT_GET_FEE_MESSAGE_COMMON,
          {
            client_id: this.$route.params.client_id,
            cart_id: cartId
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          })
      });
    },

    /**
     * 2.1.SQL001で「都道府県」一覧を取得/
     * Get prefecture list base on SQL001
     * @return {Promise}
     */
    getPrefectureList() {
      return new Promise((resolve, reject) => {
        get(api.GET_PREFECTURE_LIST)
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    /**
     * Get time admin when user login admin screen
     * @return {*}
     */
    getAdminTime() {
      if (this.adminTime) {
        let adDate = this.adminTime.date ? this.adminTime.date : '';
        let adHour = this.adminTime.hour ? this.adminTime.hour : '';
        let adMinute = this.adminTime.minute ? this.adminTime.minute : '';
        let adDateTime = adDate + ' ' + adHour + ':' + adMinute;
        let fmReg = new RegExp(/^(([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})\s([0-9]{1,2})\:([0-9]{1,2}))$/);
        if (fmReg.test(adDateTime)) {
          return adDateTime;
        }
      }
      return null;
    },

    /**
     * Init data for form with info of current user login
     * @param {object} dataUser <Object info of current user login>
     * @return {*}
     */
    initFormDelivery(dataUser) {
      if (dataUser) {
        this.delivery.fullName = dataUser.member_nm || null;
        this.delivery.furigana = dataUser.member_kn || null;
        this.delivery.postCode1 = dataUser.post_no ? dataUser.post_no.substr(0, this.config.LENGTH_POST_CODE_1) : null;
        this.delivery.postCode2 = dataUser.post_no ? dataUser.post_no.substr(this.config.LENGTH_POST_CODE_1) : null;
        this.delivery.postCode = dataUser.post_no || null;
        this.delivery.prefecture = dataUser.prefecture || (this.prefectureList && this.prefectureList.length > 0
          ? this.prefectureList[0].code_nm : null) || null;
        this.delivery.province = dataUser.municipality || null;
        this.delivery.noPhone = dataUser.tel_no && dataUser.mobile_no ? dataUser.tel_no :
          (dataUser.tel_no && !dataUser.mobile_no ? dataUser.tel_no :
            (!dataUser.tel_no && dataUser.mobile_no ? dataUser.mobile_no : null));
        this.delivery.otherAddress = dataUser.address1 || null;
        this.delivery.noRoomBuild = dataUser.address2 || null;
      }
    },

    /**
     * Add payment info and commit data to state
     * @return {*}
     */
    async setPaymentInfo() {
      // Check have yet pass validate?
      let allowNext = await new Promise((resolve, reject) => {
        let isAllow = true;
        this.$children.forEach(vm => {
          let isPassValidateVm = vm.$validator.validate().then(result => {
            if (!result) {
              return false;
            } else {
              return true;
            }
          });
          isAllow = isAllow && isPassValidateVm;
        });
        resolve(isAllow);
      });
      // If don't pass validate, stop this page, else redirect to next page
      if (!allowNext) {
        return false;
      }

      // Check exist payment and valid method in list payment method
      if (!this.payment.paymentMethod || !this.payment.receiveMethod
        || !this.isValidMethodSelected(this.config.NAME_TYPE_METHOD_PAYMENT, this.payment.paymentMethod)
        || !this.isValidMethodSelected(this.config.NAME_TYPE_METHOD_RECEIVE, this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod))) {
        this.redirectToError([
          this.$t('message.msg062_invalid_method_payment')
        ]);
        return false;
      }

      //1.1.受取方法が配送(SQL045.code_no_007='202')の場合は、配送先情報の入力禁止文字のチェックを行う※禁止文字は別紙「共通事項」のNO.18を参照
      // Convert character in black table character
      if (this.payment.receiveMethod && this.payment.receiveMethod == configDefine.PAYMENT_METHOD_DELIVERY_CODE) {
        if (this.delivery) {
          this.delivery.fullName = this.delivery.fullName ? this.$shiftJis(this.delivery.fullName) : this.delivery.fullName;
          this.delivery.furigana = this.delivery.furigana ? this.$shiftJis(this.delivery.furigana) : this.delivery.furigana;
          this.delivery.otherAddress = this.delivery.otherAddress ? this.$shiftJis(this.delivery.otherAddress) : this.delivery.otherAddress;
          this.delivery.province = this.delivery.province ? this.$shiftJis(this.delivery.province) : this.delivery.province;
          this.delivery.noRoomBuild = this.delivery.noRoomBuild ? this.$shiftJis(this.delivery.noRoomBuild) : this.delivery.noRoomBuild;
        }
      }
      // Set data to state and keep in cart
      let data = {
        payment: {
          code: this.payment.paymentMethod || null,
          name: this.payment.paymentMethod ? this.getNmPaymentOrReceiveMethod(this.config.NAME_TYPE_METHOD_PAYMENT, this.payment.paymentMethod) : null,
          settle_info: this.payment.paymentMethod ? this.getSettleOrDepartInfoMethod(this.config.NAME_TYPE_METHOD_PAYMENT, this.payment.paymentMethod) : null
        },
        receive: {
          code: this.payment.receiveMethod ? this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod) : null,
          name: this.payment.receiveMethod
            ? this.getNmPaymentOrReceiveMethod(this.config.NAME_TYPE_METHOD_RECEIVE, this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod))
            : null,
          depart_info: this.payment.receiveMethod
            ? this.getSettleOrDepartInfoMethod(this.config.NAME_TYPE_METHOD_RECEIVE, this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod))
            : null
        },
        delivery: this.payment.receiveMethod
        && this.getCodeReceiptMethodBaseInput(this.payment.receiveMethod) == this.config.METHOD_DELIVERY_CODE
          ? this.delivery : null
      }
      this.$store.dispatch('payment/setPaymentInfo', data);
      this.redirectToCartConfirm();
    },

    /**
     * Reset info of payment
     */
    resetPaymentInfo() {
      this.$store.dispatch('payment/resetPaymentInfo');
    },

    /**
     * Get code receive method
     * @param {string} receive
     * @return {string}
     */
    getCodeReceiptMethodBaseInput(receive) {
      let reg = new RegExp('^[0-9]+\_');
      return receive.replace(reg, '');
    },

    /**
     * Get code of prefecture base on name of prefecture
     * @param {string} prefectureNm <Name of prefecture>
     * @return {string|null} Code of prefecture
     */
    getCodePrefectureBaseName(prefectureNm) {
      if (this.getPrefectureList && this.prefectureList.length > 0) {
        let prefecture = this.prefectureList.find(function (el) {
          return el.code_nm = prefectureNm;
        });
        if (prefecture) {
          return prefecture.code_no || '';
        }

        return null;
      }
    },

    /**
     * Redirect to error page 570 with message error
     * @return {VueRouter.push}
     */
    redirectToError(messages) {
      this.$store.dispatch('auth/setError', messages);
      return this.$router.push({name: router.ERROR_NAME});
    },

    /**
     * Redirect to cart confirm
     * @TODO replace name of router with constant
     */
    redirectToCartConfirm() {
      return this.$router.push({name: 'client_id-cart-confirm', params: {client_id: this.$route.params.client_id}});
    },

    /**
     * Redirect to cart
     */
    redirectToCart() {
      this.resetPaymentInfo();
      return this.$router.push({name: router.CART, params: {client_id: this.$route.params.client_id}});
    },

    /**
     * Get name of payment method or name of receive method
     * @param {string} type <payment: get name of payment method|receive: get name of receive method>
     * @param {string} code <Code of method payment of receive>
     * @return {string|null} Name of method
     */
    getNmPaymentOrReceiveMethod(type, code) {
      if (type && code && this.payment.paymentList && this.payment.paymentList.length > 0) {
        if (type === this.config.NAME_TYPE_METHOD_PAYMENT) {
          let methods = this.payment.paymentList.find(function (el) {
            return el.code_no_006 == code;
          });
          if (methods) {
            return methods.code_nm_006 || null;
          }
        }

        if (type === this.config.NAME_TYPE_METHOD_RECEIVE) {
          let nmRecei = null;
          for (var i = 0; i < this.payment.paymentList.length; i++) {
            if (this.payment.paymentList[i].receive_list) {
              let recei = this.payment.paymentList[i].receive_list.find(el2 => {
                return el2.code_no_007 == code;
              });

              if (recei) {
                nmRecei = recei.code_nm_007;
                break;
              }
            }
          }
          return nmRecei || null;
        }
        return null;
      }
    },

    /**
     * Get Settle info of payment method and depart info of receive method
     * @param {string} type <payment: get name of payment method|receive: get name of receive method>
     * @param {string} code <Code of method payment of receive>
     * @return {string|null} Name of method
     */
    getSettleOrDepartInfoMethod(type, code) {
      if (type && code && this.payment.paymentList && this.payment.paymentList.length > 0) {
        if (type === this.config.NAME_TYPE_METHOD_PAYMENT) {
          let methods = this.payment.paymentList.find(function (el) {
            return el.code_no_006 == code;
          });
          if (methods) {
            return methods.settle_info || null;
          }
        }

        if (type === this.config.NAME_TYPE_METHOD_RECEIVE) {
          let nmRecei = null;
          for (var i = 0; i < this.payment.paymentList.length; i++) {
            if (this.payment.paymentList[i].receive_list) {
              let recei = this.payment.paymentList[i].receive_list.find(el2 => {
                return el2.code_no_007 == code;
              });

              if (recei) {
                nmRecei = recei.depart_info;
                break;
              }
            }
          }
          return nmRecei || null;
        }
        return null;
      }
    }
  },
  watch: {
    /**
     * When change payment method, reset receive method and remove form input delivery
     * @param value <Code of payment method>
     */
    'payment.paymentMethod'(value) {
      this.payment.receiveMethod = null;
    },

    /**
     * When change receive method, fill data to form info delivery
     * @param value <Va>
     */
    'payment.receiveMethod'(value) {
      let methodReceive = value ? this.getCodeReceiptMethodBaseInput(value) : null;
      if (methodReceive && methodReceive == this.config.METHOD_DELIVERY_CODE) {
        this.initFormDelivery(this.user);
      }
    },

    /**
     * When prefecture name was selected, auto setting code prefecture responsibility
     * @param {string} value <Name of prefecture>
     */
    'delivery.prefecture'(value) {
      this.delivery.prefectureCode = value ? this.getCodePrefectureBaseName(value) : null;
    }
  }
}
