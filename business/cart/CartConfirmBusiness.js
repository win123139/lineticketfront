import {mapState, mapGetters} from 'vuex';
import constant from '@/constant';
import {get, post} from '@/plugins/api';
import Breadcrumb from '@/components/Cart/Breadcrumb';
import ShowDetailConfirm from '@/components/Cart/ShowDetailConfirm';
import TotalPayment from '@/components/Cart/TotalPayment';
import ListFee from '@/components/Cart/ListFee';
import TotalPaymentAmount from '@/components/Cart/TotalPaymentAmount';
import InfoReceive from '@/components/Cart/InfoReceive';
import PaymentType from '@/components/Cart/PaymentType';
import QueueModal from '@/components/Cart/QueueModal';
import WarningModal from '@/components/Navigation/TheWarningModal';
import Converter from '@/static/js/convertToArr';

import ListTransacitons from '@/grapsql/queries/ListTransacitons';
import NewTransacitonSub from '@/grapsql/subscriptions/NewTransacitonsSubscription';
import DeletedTransacitonSub from '@/grapsql/subscriptions/DeletedTransactionSub';
import {appsyncProvider ,appName} from '@/plugins/realtime';

const MAX_PAYMENT_AMOUNT = 300001;
const MAX_LIMIT_TRANSACTIONS = 3;
const moment = require('moment');

export default {
  components: {
    Breadcrumb,
    ShowDetailConfirm,
    TotalPayment,
    ListFee,
    TotalPaymentAmount,
    PaymentType,
    InfoReceive,
    WarningModal,
    QueueModal
  },
  middleware: 'authenticated',
  head() {
    return {
      title: this.$t('cart.lb_cart_confirm')
    }
  },
  apolloProvider: appsyncProvider,
  data() {
    return {
      totalBeforeInQueue: 0,
      step: 3,
      type: 'confirm',
      fees: [],
      cartDetail: [],
      message: '',
      code: '',
      //data sample SQL045
      paymentMethod: {
        code_no_006_nm: '',
        code_no_007_nm: '',
        disp_limit: ''
      },
      delivery: {
        noPhone: '',
        fullName: '',
        furigana: '',
        postCode: '',
        prefecture: '',
        province: '',
        otherAddress: '',
        noRoomBuild: ''
      },
      transactionAppSync: []
    }
  },
  computed: {
    ...mapGetters({
      cartId: 'booking/cartId',
      cartDetailSession: 'booking/cartDetail',
      getMemberInfo: 'auth/getMemberInfo',
      getAdminTime: 'auth/getAdminTime',
      getPaymentMethodState: 'payment/getPaymentMethod',
      getReceiveMethodState: 'payment/getReceiveMethod',
      getDeliveryState: 'payment/getDelivery',
      getAdmin: 'auth/getAdmin'
    }),

    /**
     * Check if has fees
     * @return {boolean}
     */
    hasFees() {
      return this.fees.length > 0;
    },

    /**
     * Format fee to display
     * @returns {Array}
     */
    displayFees() {
      let dispFees = [];
      let tmpFees = this.fees.slice();
      tmpFees.map(function(fee) {
        let tmpFee = Object.assign({}, fee);
        if (tmpFee.type == 2) {
          let tmpIndex = -1;
          if (dispFees.some(function(tmp, index) {
            if(tmp.fee_no == tmpFee.fee_no){
              tmpIndex = index;
              return true;
            }
            return false;
          })) {
            dispFees[tmpIndex].fee_price = parseInt(dispFees[tmpIndex].fee_price) + parseInt(tmpFee.fee_price);
          }
          if (tmpIndex == -1) {
            dispFees.push(tmpFee);
          }
        } else if (fee.type == 1) {
          dispFees.push(tmpFee);
        }
      });
      return dispFees;
    },

    /**
     * Convert cart detail to show
     * @returns {{}}
     */
    cartDetailGroup() {
      let result = {};
      if (typeof this.cartDetail != 'undefined' && this.cartDetail.length > 0) {
        this.cartDetail.forEach(function (item) {
          if (!result.hasOwnProperty(item.show_nm)) {
            result[item.show_nm] = [];
          }
          result[item.show_nm].push(item);
        });
      }
      return result;
    },

    /**
     * Get all show group id
     * @returns {Array}
     */
    cartShowGroup() {
      let result = [];
      if (this.cartDetail.length > 0) {
        this.cartDetail.forEach(function (item) {
          if (result.indexOf(item.show_group_id) < 0) {
            result.push(item.show_group_id);
          }
        })
      }
      return result;
    },

    /**
     * Calculate total price of tickets
     * @returns {number}
     */
    calTotalPrice() {
      let price = 0;
      this.cartDetail.forEach(function (item) {
        price += Number(item.price) * item.ticket_count;
      });
      return price;
    },

    /**
     * Concat payment method
     * @returns {string}
     */
    concatPaymentMethod() {
      let join = '';
      if (this.paymentMethod.code_no_006_nm != '' && this.paymentMethod.code_no_007_nm != ''){
        join = '－';
      }
      return this.paymentMethod.code_no_006_nm + join + this.paymentMethod.code_no_007_nm;
    },

    /**
     * Calculate total payment = total price tickets + total price fees
     * @returns {number}
     */
    calTotalPayment() {
      let payment = Number(this.calTotalPrice);
      this.fees.forEach(function (fee) {
        payment += Number(fee.fee_price);
      });
      return payment;
    },

    /**
     * Get payment method code
     * @returns {*}
     */
    getPaymentMethod() {
      return this.paymentMethod.code_no_006;
    },

    /**
     * Get receive method
     * @returns {number}
     */
    getReceiveMethod() {
      return this.paymentMethod.code_no_007;
    }
  },

  /**
   * On created event
   */
  created() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
    });
    this.initPage();
  },

  methods: {
    /**
     * Handle init page
     */
    initPage: async function () {

      //clear all information payment in state
      this.$store.dispatch('booking/resetInfoPayment');

      //get cart detail from state
      this.cartDetail = this.cartDetailSession.map(function (item, index) {
        if (item.standard_ticket_price - item.sales_price > 0) {
          item.price = item.sales_price;
        } else {
          item.price = item.standard_ticket_price;
        }
        return item;
      });

      const _this = this;
      //get payment method list
      await get(constant.api.PAYMENT_GET_METHOD_LIST, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId,
        admin_time: this.getAdminTime
      })
        .then(result => {
          if(result.data.data != null) {
            this.paymentMethod = {
              code_no_006: this.getPaymentMethodState.code,
              code_no_007: this.getReceiveMethodState.code,
              startdtime: this.getReceiveMethodState.start_dtime,
              enddtime: this.getReceiveMethodState.end_dtime
            };
            if (this.getDeliveryState != null) {
              this.delivery = {
                noPhone: this.getDeliveryState.noPhone,
                fullName: this.getDeliveryState.fullName,
                furigana: this.getDeliveryState.furigana,
                postCode: this.getDeliveryState.postCode,
                prefecture: this.getDeliveryState.prefecture,
                province: this.getDeliveryState.province,
                otherAddress: this.getDeliveryState.otherAddress,
                noRoomBuild: this.getDeliveryState.noRoomBuild
              }
            }

            result.data.data.forEach(function(method) {
              if(method.code_no_006 == _this.paymentMethod.code_no_006) {
                method.receive_list.forEach(function(receive) {
                  if (receive.code_no_007 == _this.paymentMethod.code_no_007) {
                    _this.paymentMethod.code_no_006_nm = method.code_nm_006;
                    _this.paymentMethod.code_no_007_nm = receive.code_nm_007;
                    _this.paymentMethod.disp_limit = method.disp_limit;
                    _this.paymentMethod.stop_flg = method.stop_flg;
                  }
                });
              }
            })
          }
        }).catch(err => {
          throw new Error(err);
        });

      //get all fees of cart
      await get(constant.api.CART_CONFIRM_FEE, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId,
        settle_cd: this.paymentMethod.code_no_006,
        depart_cd: this.paymentMethod.code_no_007
      })
          .then(result => {
            if (result.data.data != null){
              this.fees = result.data.data;
            } else {
              this.fees = [];
            }
          })
          .catch(err => {
            console.log(err);
            throw new Error(err);
          });

      this.checkMethodWithTotalPay();
      this.$nuxt.$loading.finish();
    },

    /**
     * Check payment method and total payment
     * If has errors redirect to cart payment method
     */
    checkMethodWithTotalPay: function () {
      if(this.getPaymentMethod == '203' && this.calTotalPayment >= MAX_PAYMENT_AMOUNT) {
        //go to cart payment method with message 066
        alert(this.$t('message.msg066_error_max_payment'));
        let path = this.$router.resolve({
          name: constant.router.CART_PAYMENT_METHOD,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
      }
    },

    /**
     * Get cart from api
     * @returns {*}
     */
    getCartDetailServer: async function () {
      let check = await get(constant.api.CART_DETAIL, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId
      });
      return check.data.data;
    },

    /**
     * Check if cart is existed
     * @param cart
     * @returns {boolean}
     */
    checkExistCart: function (cart) {
      return typeof cart != 'undefined' && cart.length > 0;
    },

    /**
     * Check consistency between cart in state and on server
     * @param cartState
     * @param cartServer
     * @returns {*|boolean}
     */
    checkConsistencyCart: function (cartState, cartServer) {
      let cartSeqState = Converter(cartState, 'cart_seq');
      let cartSeqServer = Converter(cartServer, 'cart_seq');

      let check = cartSeqServer.every(function (item) {
        let index = cartSeqState.indexOf(item);
        if (index < 0) {
          return false;
        }
        cartSeqState.splice(index, 1);
        return true;
      });

      if (check) {
        return true;
      }

      this.message = this.$t('message.msg048_cart_has_changed');
      this.code = 'CART';
      $('#theWarningModal').modal('show');
      return false;
    },

    /**
     * Check if ticket is out if reserve time
     * @returns {boolean}
     */
    checkSeatOutOfReserveTime: async function () {
      let check = await post(constant.api.CART_CHECK_OUT_OF_RESERVE_TIME, {
        client_id: this.$route.params.client_id,
        cart_id: this.cartId,
        admin_time: this.getAdminTime
      });
      if (check.data.status === 'success') {
        return true;
      }

      this.message = this.$t('message.msg004_error_sales_term');
      this.code = 'CART';
      $('#theWarningModal').modal('show');
      return false;
    },

    /**
     * Check if out of limit payment day
     * @returns {boolean}
     */
    checkLimitDayPayment: function () {
      let date = this.paymentMethod.disp_limit;
      //convert datetime to yyyy-mm-dd
      if (date.length == 8){
        date = date.substr(0,4) + '-' + date.substr(4,2) + '-' + date.substr(6);
      } else {
        let tmpDate = date.substr(0,4) + '-' + date.substr(4,2) + '-' + date.substr(6,2);
        let tmpTime =  date.substr(8,2) + ':' + date.substr(10,2) + ':' + date.substr(12);
        date = tmpDate + ' ' + tmpTime;
      }
      let check = new Date(date) > new Date();
      if(check){
        return true;
      }

      this.message = this.$t('message.msg069_error_limit_payment_date');
      this.code = 'CART_PAYMENT_METHOD';
      $('#theWarningModal').modal('show');
      return false;
    },

    /**
     * Check if payment method is in maintain mode
     * @returns {boolean}
     */
    checkMaintainMode: function () {
      let check = this.paymentMethod.stop_flg == 1;
      if(!check) {
        return false;
      }

      this.message = this.$t('message.msg067_link_maintance');
      this.message = this.message.replace('{startdtime', this.paymentMethod.startdtime);
      this.message = this.message.replace('{enddtime', this.paymentMethod.enddtime);
      this.code = 'CART_PAYMENT_METHOD';
      $('#theWarningModal').modal('show');

      return true;
    },

    checkAdmin: function() {
      let admin = this.getAdmin;
      if (!admin.admin  && !admin.admin_flag) {
        return false;
      }
      this.message = this.$t('message.msg074_confirm_reserve_admin');
      $('#theWarningModal').modal('show');
      return true;
    },

    checkLimitTransaction: function() {
      console.log(this.transactionAppSync)
      let dataInProgress = this.transactionAppSync.filter(function(item) {
        return item.status;
      });
      let totalInProgress = dataInProgress.length;
      if (totalInProgress < MAX_LIMIT_TRANSACTIONS) {
        return {
          canGo: true
        }
      } else {
        let dataInQueue = this.transactionAppSync.filter(function(item) {
          return !item.status && item.created_at < '2545045823162' ;
        });
        return {
          canGo: false,
          count: totalInProgress + dataInQueue.length
        };
      }
    },

    /**
     * Filter fees by type
     * @param fees
     * @param type
     * @param ticket
     * @returns {Array}
     */
    filterFees: function(fees, type, ticket = null) {
      let tmp = [];
      if (type == 2 && ticket != null) {
        tmp = fees.filter(function(fee) {
          if (
            fee.type == type
            && fee.show_group_id == ticket.show_group_id
            && fee.show_no == ticket.show_no
            && fee.seat_no == ticket.seat_no
          ) {
            return true;
          }
          return false;
        });
      } else {
        tmp = fees.filter(function(fee) {
          if (fee.type == type) {
            return true;
          }
          return false;
        });
      }

      return tmp.map(function(item) {
        return {
          fee_no: item.fee_no,
          fee_unit: item.fee_unit,
          fee_type: item.fee_type,
          fee_price: item.fee_price,
          fee_nm: item.fee_nm,
          settledepart_cd: item.settledepart_cd
        };
      });
    },

    /**
     * Redirect if has error
     * @param code
     */
    redirectPage: function(code) {
      let path = this.$router.resolve({
        name: constant.router[code],
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    },
    

    /**
     * Event onclick back button
     */
    onClickBackButton: async function () {
      //store cart get from server
      let cartServer = await this.getCartDetailServer();

      if (!this.checkExistCart(cartServer)) {
        //go to error screen with message 005
        this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);
        this.$router.push({name: constant.router.ERROR_NAME});

      } else if (this.checkConsistencyCart(this.cartDetail, cartServer)) {
        //go to choose payment screen
        let path = this.$router.resolve({
          name: constant.router.CART_PAYMENT_METHOD,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
      }
    },

    /**
     * Event onclick confirm button
     */
    onClickConfirmButton: async function () {
      this.$nuxt.$loading.start();
      //store cart get from server
      let cartServer = await this.getCartDetailServer();

      if (!this.checkExistCart(cartServer)) {
        //go to error screen with message 005
        this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);
        this.$router.push({name: constant.router.ERROR_NAME});
      }

      //check condition confirm button
      let checkNotError = !this.checkAdmin()
          && this.checkConsistencyCart(this.cartDetailSession, cartServer)
          && await this.checkSeatOutOfReserveTime()
          && this.checkLimitDayPayment()
          && !this.checkMaintainMode();

      if (checkNotError) {
        //register cart
        let member = this.getMemberInfo;

        let show_group_common = this.cartShowGroup.length > 1 ? '' : this.cartShowGroup[0];

        let _this = this;

        //format parameter for api registerInfo
        let param = {
          client_id: this.$route.params.client_id,
          cart_id: this.cartId,
          member_id: member.member_id,
          ins_client_id: this.$route.params.client_id,
          show_group_id: show_group_common,
          urikake_gaku: this.calTotalPayment,
          settle_cd: this.paymentMethod.code_no_006,
          depart_cd: this.paymentMethod.code_no_007,
          member_kb_no: member.member_kb_no,
          member_nm: member.member_nm,
          member_kn: member.member_kn,
          post_no: member.post_no,
          prefecture: member.prefecture,
          municipality: member.municipality,
          address1: member.address1,
          address2: member.address2,
          contact_tel_no1: member.tel_no,
          contact_tel_no2: member.mobile_no,
          delivery_tel_no1: this.delivery.noPhone,
          delivery_nm: this.delivery.fullName,
          delivery_kn: this.delivery.furigana,
          delivery_post_no: this.delivery.postCode,
          delivery_prefecture: this.delivery.prefecture,
          delivery_municipality: this.delivery.province,
          delivery_address1: this.delivery.otherAddress,
          delivery_address2: this.delivery.noRoomBuild,
          mail_address: member.mail_address,
          _data: [],
          _fees: []
        };

        //filter data tickets and attach fees
        this.cartDetail.forEach(function (item) {
          let seats = Converter(item, 'seat_no');
          seats.forEach(function (seat) {
            let ticket = {
              show_group_id: item.show_group_id,
              show_no: item.show_no,
              sales_no: item.sales_no,
              seat_no: seat,
              seat_type_no: item.seat_type_no,
              ticket_type_no: item.ticket_type_no,
              sales_price: item.sales_price,
              fees: []
            };
            //attach fee to ticket
            ticket.fees = _this.filterFees(_this.fees, 2, ticket);
            param._data.push(ticket);
          });
        });

        param._fees = this.filterFees(this.fees, 1);
        // post api
        try {
          let check = await post(constant.api.CART_REGISTER_INFO, param);

          let data = {
            urikakeNo: check.data.data.urikake_no,
            reserveNo: check.data.data.reserve_no,
            urikakeType: check.data.data.urikake_type,
            departEndDtime: check.data.data.depart_end_dtime,
            limitPaymentDays: check.data.data.limit_payment_days
          };
          //store all information for payment
          this.$store.dispatch('booking/setInfoPayment', data);

          if (check.data.data.data == 'credit') {
            //TODO: go to credit card screen
            let checkLimitTransaction = this.checkLimitTransaction();
            console.log(this.checkLimitTransaction());
            if(checkLimitTransaction.canGo) {
              alert('go credit');
              return;
            } else {
              this.totalBeforeInQueue = checkLimitTransaction.count;
              $('#QueueModal').modal('show');
              return;
            }
          } else {
            this.$nuxt.$loading.finish();
            //go to cart complete screen 350
            this.$store.dispatch('booking/clearBookingAll');
            let path = this.$router.resolve({
              name: constant.router.BOOKING_COMPLETE,
              params: {client_id: this.$route.params.client_id}
            });

            this.$router.push(path.href);
          }
        } catch (err) {
          let error = err;
          if (err.response != null) {
            error = err.response.data.data;
          }
          this.$nuxt.$loading.finish();
          //check error message
          let errMsg = {};
          let message = [];
          if (error.sendgrid || error.fami_pass) {
            //go to error screen with message 005
            errMsg = this.$t('message.msg003_exception');
            message = Object.keys(errMsg).map(function(item) {
              return errMsg[item];
            })
          } else if (error.transaction) {
            errMsg = this.$t('message.msg092_error_system');
            message.push(errMsg);
          } else {
            errMsg = this.$t('message.msg003_exception');
            message = Object.keys(errMsg).map(function(item) {
              return errMsg[item];
            })
          }
          this.$store.dispatch('booking/clearBookingAll');
          //go to error screen with message
          this.$store.dispatch('auth/setError', message);
          this.$router.push({name: constant.router.ERROR_NAME});
        }
      }
      this.$nuxt.$loading.finish();
    }
  },
  apollo: {
    transactionAppSync: {
      query: () => ListTransacitons,
      update: data => data.listTransacitons.items,
      subscribeToMore: [
        {
          document: DeletedTransacitonSub,
          updateQuery: function(previousResult, { subscriptionData: { data: { onDeleteTransacitons }} }) {
            console.log('here', this);
            this.transactionAppSync = this.transactionAppSync.filter(function(item) {
              return item.client_id == onDeleteTransacitons.client_id && item.transaction_id != onDeleteTransacitons.transaction_id;
            });
            this.totalBeforeInQueue = this.checkLimitTransaction().count;
            console.log(this.transactionAppSync)
          },
          onError: err => {
            console.log(err);
            // Timeout real time page will be reload
            // window.location.reload();
          }
        },
        {
          document: NewTransacitonSub,
          updateQuery: function(previousResult, { subscriptionData: { data: { onCreateTransacitons }} }) {
            console.log('here', this);
            this.transactionAppSync = this.transactionAppSync.concat(onCreateTransacitons);
            this.totalBeforeInQueue = this.checkLimitTransaction().count;
            console.log(this.transactionAppSync)
          },
          onError: err => {
            console.log(err);
            // Timeout real time page will be reload
            // window.location.reload();
          }
        }
      ],
      // Additional options here
      fetchPolicy: 'cache-and-network'
    }
  }
}