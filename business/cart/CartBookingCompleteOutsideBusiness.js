/**
 * CartBookingCompleteBusiness.js
 * Handle complete booking
 *
 * @author Rikkei.DungLV
 * @date 2018-10-22
 */
import _api from '@/constant/api';
import _router from '@/constant/router';
import {get, post} from "@/plugins/api"
import ClientInfo from "@/components/UI/ClientInfo"
import configCommon from "@/constant/config"
import {mapState, mapGetters} from 'vuex'

export default {
  name: "index",
  middleware: 'authenticated',
  components: {
    ClientInfo
  },
  head() {
    return {
      title: this.$t('booking.title_booking_complete')
    }
  },

  data() {
    return {
      clientInfo: null,
      config: {
        RECEIVE_FAMI_PASS_METHOD: configCommon.PAYMENT_METHOD_FAMILYMART_CODE,
        PAYMENT_METHOD_CREDIT_CARD: configCommon.PAYMENT_CREDIT_CARD_CODE,
        PAYMENT_METHOD_COMBINI: configCommon.PAYMENT_METHOD_COMBINI
      }
    }
  },

  computed: {
    ...mapState({
      payment: state => state.payment,
      reserveNo: state => state.booking.reserveNo || null
    }),
    ...mapGetters({
      clientInfoStore: 'client/getClient'
    })
  },

  mounted() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
      // If info of client is empty, call API and get from database
      if (!this.clientInfoStore) {
        this.setClientInfo()
          .then(res => {
            this.clientInfo = res;
          })
          .catch(err => {
            throw new Error(err);
          });
        // If exists data of client, get from cache
      } else {
        this.clientInfo = this.clientInfoStore;
      }
      // Handle fami pass and send mail
      if (this.payment && this.payment.paymentMethod && this.payment.paymentMethod.code && this.payment.receiveMethod
        && this.payment.receiveMethod.code) {
        if ((this.payment.paymentMethod.code == this.config.PAYMENT_METHOD_CREDIT_CARD
          || this.payment.paymentMethod.code == this.config.PAYMENT_METHOD_COMBINI)
          && this.payment.receiveMethod.code == this.config.RECEIVE_FAMI_PASS_METHOD) {
          this.updateFamilyMartBooking()
            .then(res => {
              return this.sendMailBookingComplete();
            })
            .then(res => {
              if (res) {
                return this.changeSettlement();
              }
            })
            .then(res => {
              this.$nuxt.$loading.finish();
            })
            .catch(err => {
              this.$nuxt.$loading.finish();
              throw new Error(err);
            });
        } else {
          this.sendMailBookingComplete()
            .then(res => {
              if (res) {
                return this.changeSettlement();
              }
            })
            .then(res => {
              this.$nuxt.$loading.finish();
            })
            .catch(err => {
              this.$nuxt.$loading.finish();
              throw new Error(err);
            });
        }
      } else {
        this.$nuxt.$loading.finish();
        throw new Error(err);
      }
    });
  },

  methods: {
    setClientInfo() {
      this.$nuxt.$loading.start();
      post(_api.GET_CLIENT_INFO, {
        client_id: this.$route.params.client_id
      })
        .then(res => {
          this.clientInfo = res.data.data;
        })
        .catch(err => {
          error({statusCode: 500, error: err});
        });
    },

    /**
     * Update info payment
     *
     */
    changeSettlement() {
      return new Promise((resolve, reject) => {
        post(_api.HANDLE_CHANGE_SETTLEMENT, {
          client_id: this.$route.params.client_id,
          booking_code: this.bookingCodeStore
        })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err)
          })
      });
    },

    /**
     * Append booking code to text extract content
     *
     * @param content
     * @param bookingCode
     * @return {void | string | *}
     */
    appendBookingCode(content, bookingCode) {
      return content.replace('[予約番号]', bookingCode);
    },

    /**
     * Handle send mail to member info booking ticket
     * @return {boolean}
     */
    sendMailBookingComplete() {
      return new Promise((resolve, reject) => {
        post(_api.OUT_SEND_MAIL_COMPLETE_BOOKING, {
          client_id: this.$route.params.client_id,
          reserve_no: this.reserveNo || null,
          receive_method: this.payment.receiveMethod.code || null,
          payment_method: this.payment.paymentMethod.code || null
        })
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
            this.$nuxt.$loading.finish();
          });
      });
    },

    /**
     *
     * @return {Promise<any>}
     */
    updateFamilyMartBooking(){
      return new Promise((resolve, reject) => {
        post(_api.OUT_HANDLE_FAMI_PASS_TICKET,
          {
            client_id: this.$route.params.client_id,
            reserve_no: this.reserveNo || null,
            receive_method: this.payment.receiveMethod.code || null,
            payment_method: this.payment.paymentMethod.code || null
          })
          .then(res => {
            console.log(res);
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    goToHome() {
      return this.$router.push({name: _router.LISTPERFORM, params: {'client_id': this.clientInfo.client_id}})
    }
  }
}
