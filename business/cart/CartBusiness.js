/**
 * File CartBusiness.js
 * Hanlde init page cart
 *
 * @author Rikkei.ThienNB
 * @date 2018-11-01
 */
import {mapState, mapGetters} from 'vuex';
import constant from '@/constant';
import {get, post} from '@/plugins/api';
import Breadcrumb from '@/components/Cart/Breadcrumb';
import ShowDetail from '@/components/Cart/ShowDetail';
import CartWarning from '@/components/Cart/CartWarning';
import TotalPayment from '@/components/Cart/TotalPayment';
import WarningModal from "@/components/Navigation/TheWarningModal";
import DeleteSeatModal from "@/components/Cart/TheDeleteSeatModal";
import ResetCartModal from "@/components/Cart/TheResetCartModal";

export default {
  components: {
    Breadcrumb,
    ShowDetail,
    CartWarning,
    TotalPayment,
    WarningModal,
    ResetCartModal,
    DeleteSeatModal
  },
  middleware: 'guest',
  head() {
    return {
      title: this.$t('cart.lb_cart'),
    }
  },
  data() {
    return {
      step: 1,
      type: 'cart',
      message:'',
      seat: '',
      showPage:false,
      cartDetail:[]
    }
  },
  computed: {
    ...mapGetters({
      cartId: 'booking/cartId',
      memberId: 'auth/getMemberId',
      memberKbNo: 'auth/getMemberKbNo',
      memberTypeNo: 'auth/getMemberTypeNo',
      unit: 'auth/getUnit'
    }),
    cartDetailGroup(){
      let result = {} ;
      if(this.cartDetail.length >0) {

        this.cartDetail.forEach(function (el) {

          if (typeof(result[el.show_nm]) === 'undefined') {
            result[el.show_nm] = [];
          }

          result[el.show_nm].push(el);

        });
      }

      return result;
    },
    numberTicket(){
      let result = 0 ;
      if(this.cartDetail.length >0) {
        result = this.cartDetail.length;
      }

      return result;
    }
  },
  created() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();

    });
    this.initPage();
  },
  methods: {
    /**
     * Function init page get ticket info
     *
     * @returns {Array}
     */
    initPage: function () {
        get(constant.api.CART_DETAIL, {
          client_id: this.$route.params.client_id,
          cart_id: this.cartId
        })
        .then(result => {
          this.cartDetail = result.data.data;
          this.$store.dispatch('booking/setCartDetail', this.cartDetail);
          //finish loading
          this.showPage = true;
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);

          // cart dont't exit move to error page
          this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);
          this.$router.push({name: constant.router.ERROR_NAME});

        });
    },
    onWarningModal(){
      this.message = this.$t("message.msg059_cancel_pair_ticket");
      $('#theWarningModal').modal('show');
    },
    onDeleteModal(seat){
      this.message = this.$t("message.msg057_cancel_seat");
      this.seat= seat;
      $('#TheDeleteSeatModal').modal('show');
    },
    onResetCartModal(){
      this.message = this.$t("message.msg058_reset_cart");
      $('#TheResetCartModal').modal('show');
    },
    onConfirmDelete(seat) {
      post(constant.api.CART_DELETE, {
        client_id : this.$route.params.client_id,
        cart_id : this.cartId,
        show_group_id : seat.show_group_id,
        show_no :  seat.show_no,
        seat_type_no :seat.seat_type_no,
        number_cart_item : this.numberTicket,
        cart_seq : seat.cart_seq,
        seat_no  : seat.seat_no,
        member_kb_no  : this.memberKbNo,
        member_type_no : this.memberTypeNo,
        member_id  : this.memberId,
        unit  : this.unit,
        internet_seat_kb : seat.internet_seat_kb,
        ticket_type_no : seat.ticket_type_no,
        sales_no  : seat.sales_no,
        sales_kb  : seat.sales_kb,
        standard_ticket_price  : seat.standard_ticket_price
      })
        .then(result => {
          this.$router.go();
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);

          // cart dont't exit move to error page
          this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);
          this.$router.push({name: constant.router.ERROR_NAME});

        });

    },
    onContinueBooking(){
      let path = this.$router.resolve({
        name: constant.router.LISTPERFORM,
        params: {client_id: this.$route.params.client_id}
      });

      this.$router.push(path.href);

    },
    onComfirmResetCart(){
      post(constant.api.CART_RESET, {
        client_id : this.$route.params.client_id,
        cart_id : this.cartId

      })
        .then(result => {
          //clear booking choose
          this.$store.dispatch('booking/clearBooking');
          this.$router.go();
        })
        .catch(err => {
          // Will be redirect to page error 570 later
          console.log(err);
          // cart dont't exit move to error page
          this.$store.dispatch('auth/setError', [this.$t('message.msg005_cart_not_exit')]);
          this.$router.push({name: constant.router.ERROR_NAME});
        });
    },

    onGoPayment(){
      let path = this.$router.resolve({
        name: constant.router.CART_PAYMENT_METHOD,
        params: {client_id: this.$route.params.client_id}
      });

      this.$router.push(path.href);
    }


  }
}