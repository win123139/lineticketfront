/**
 * File CartPaymentMethodDeliveryBusiness.js
 * Handle business in payment method delivery of cart
 *
 * @author Rikkei.DungLV
 * @date 2018-11-15
 */

import _api from '@/constant/api';
import {get, post} from '@/plugins/api';
import {mapState} from 'vuex';

const LENGTH_POST_CODE_1 = 3;
const LENGTH_POST_CODE_2 = 4;
const ENTER_SPACE = 1;
const REMOVE_SPACE = 2;
const ENTER_DASH = 1;
const REMOVE_DASH = 2;

export default {
  name: 'PaymentMethodDelivery',
  props: ['delivery', 'prefectureList', 'config'],
  data() {
    return {
      allowSearchAddress: false,
      errorMessagePostCode: null
    }
  },
  computed: {
    ...mapState({
      client: state => state.client.clientInfo
    })
  },
  created() {
    this.customMessageValidate();
  },
  mounted() {
    // Check enable button search address
    if (this.delivery.postCode1 && this.delivery.postCode1.length == this.config.LENGTH_POST_CODE_1 && !isNaN(this.delivery.postCode1)
      &&
      this.delivery.postCode2 && this.delivery.postCode2.length == this.config.LENGTH_POST_CODE_2 && !isNaN(this.delivery.postCode2)) {
      this.allowSearchAddress = true;
    } else {
      this.allowSearchAddress = false;
    }
  },
  methods: {
    /**
     * Handle search post code when user click button search post code
     * @return {void}
     **/
    searchPostCode() {
      // When button disable, don't allow search
      if (!this.allowSearchAddress) return false;
      // Else, search code with value and setting data to form
      this.$nuxt.$loading.start();
      get(_api.SEARCH_POST_CODE,
        {
          client_id: this.$route.params.client_id,
          post_code_1: this.delivery.postCode1,
          post_code_2: this.delivery.postCode2
        })
        .then(res => {
          // When exist address from post code input, auto set value to input address
          if (res && res.data.data.list_address.length > 0) {
            let postCode = res.data.data.list_address[0];
            this.delivery.prefecture = postCode.todofuken_nm;
            this.delivery.province = postCode.shikuchoson_nm;
            this.delivery.otherAddress = postCode.choiki_nm;
            this.errorMessagePostCode =null;
          } else {
            // Else, not exists data, setting null both input address
            this.errorMessagePostCode = this.$t('message.msg068_lb_post_code_not_exists')
            this.delivery.prefecture = null;
            this.delivery.province = null;
            this.delivery.otherAddress = null;
          }
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          this.errorMessagePostCode =null;
          this.$nuxt.$loading.finish();
        });
    },

    /**
     * Custom show message validate on screen
     * @return {*}
     **/
    customMessageValidate() {
      const dict = {
        custom: {
          fullname: {
            required: this.$t('validation.required', {field: this.$t('register.lb_full_name')}),
            max: this.$t('validation.max', {field: this.$t('register.lb_full_name'), value: 200}),
            spaceFullSize: this.getKbOfField('fullname') == ENTER_SPACE
              ? this.$t('message.msg010_enter_space_full_size', { field: this.$t('register.lb_full_name') })
              : this.$t('message.msg011_remove_space_full_size', { field: this.$t('register.lb_full_name') }),
            fullsize: this.$t('message.msg012_input_full_size', { field: this.$t('register.lb_full_name') }),
          },
          furigana: {
            required: this.$t('validation.required', {field: this.$t('register.lb_furigana')}),
            max: this.$t('validation.max', {field: this.$t('register.lb_furigana'), value: 200}),
            kanaFullSize: this.getKbOfField('furigana') == ENTER_SPACE
              ? this.$t('message.msg010_enter_space_full_size', { field: this.$t('register.lb_furigana') })
              : (this.getKbOfField('furigana') == REMOVE_SPACE
                  ? this.$t('message.msg011_remove_space_full_size', { field: this.$t('register.lb_furigana') })
                  : this.$t('message.msg012_input_full_size', { field: this.$t('register.lb_furigana') })
              ),
          },
          phone: {
            phoneNumber: this.getKbOfField('phone') == ENTER_DASH
              ? this.$tc('message.msg013_enter_dash', false, { field: this.$t('register.lb_phone_number'), digit: 11 })
              : (this.getKbOfField('phone') == REMOVE_DASH
                  ? this.$tc('message.msg014_remote_dash', false, { field: this.$t('register.lb_phone_number'), digit: 11 })
                  : this.$t('validation.format', {field: this.$t('register.lb_phone_number')})
              )
          },
          postcode: {
            numeric: this.$t('validation.numeric', {field: this.$t('register.lb_zipcode')}),
            required: this.$t('validation.required', {field: this.$t('register.lb_zipcode')}),
            length: this.$t('validation.length', {field: this.$t('register.lb_zipcode'), number: 7}),
          },
          prefecture: {
            required: this.$t('validation.required', {field: this.$t('register.lb_city')}),
          },
          province: {
            required: this.$t('validation.required', {field: this.$t('register.lb_district')}),
            max: this.$t('validation.max', {field: this.$t('register.lb_district'), value: 200}),
            fullsize: this.$t('validation.fullsize', {field: this.$t('register.lb_district')}),
          },
          otherAddress: {
            required: this.$t('validation.required', {field: this.$t('register.lb_detail_address')}),
            max: this.$t('validation.max', {field: this.$t('register.lb_detail_address'), value: 400}),
            fullsize: this.$t('validation.fullsize', {field: this.$t('register.lb_detail_address')}),
          },
          noRoomBuild: {
            max: this.$t('validation.max', {field: this.$t('register.lb_building_room'), value: 400}),
            fullsize: this.$t('validation.fullsize', {field: this.$t('register.lb_building_room')}),
          }
        }
      }

      this.$validator.localize('ja', dict);
    },

    /**
     * Get kb of field form
     * ['fullname', 'furigana', 'phone', 'postcode', 'other_address', 'prefecture', 'province']
     * @return {number}
     */
    getKbOfField(fieldName) {
      if (this.client) {
        switch (fieldName) {
          case 'fullname':
          case 'furigana':
            return this.client.member_nm_kb || 3;
            break;

          case 'phone':
          case 'mobile_phone':
            return this.client.tel_no_kb || 3;
            break;

          default: 3
        }
      }
      return 3;
    }
  },
  watch: {
    /**
     * Catch event when user input value of post code and enable button search with below condition:
     * length of post code 1 is 3, format number
     * length of post code 2 is 4, format number
     *
     * @param value
     * @return {void}
     */
    'delivery.postCode1'(value) {
      this.delivery.postCode = value + '' + this.delivery.postCode2;
      if (value.length == LENGTH_POST_CODE_1 && !isNaN(value) && this.delivery.postCode2
        && this.delivery.postCode2.length == this.config.LENGTH_POST_CODE_2 && !isNaN(this.delivery.postCode2)) {
        this.allowSearchAddress = true;
      } else {
        this.allowSearchAddress = false;
      }
    },
    'delivery.postCode2'(value) {
      this.delivery.postCode = this.delivery.postCode1 + '' + value;
      if (value.length == LENGTH_POST_CODE_2 && !isNaN(value) && this.delivery.postCode1
        && this.delivery.postCode1.length == this.config.LENGTH_POST_CODE_1 && !isNaN(this.delivery.postCode1)) {
        this.allowSearchAddress = true;
      } else {
        this.allowSearchAddress = false;
      }
    },
  }
}
