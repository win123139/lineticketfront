import {get, post} from "@/plugins/api"
import pathApi from "@/constant/api"
import {mapState, mapGetters} from 'vuex'
import router from '@/constant/router'
import configDefine from '@/constant/config'

export default {
  name: "payment-card",
  middleware: 'authenticated',
  head() {
    return {
      title: 'Payment card',
      script: [
        {src: 'https://pt01.mul-pay.jp/ext/js/token.js'}
      ]
    }
  },
  data() {
    return {
      config: {
        RES_TOKEN_SUCCESS: "000",
        RES_TOKEN_ERROR_CARD_REQUIRED: 100,
        RES_TOKEN_ERROR_CARD_FORMAT: 101,
        RES_TOKEN_ERROR_CARD_LENGTH: 102,
        RES_TOKEN_ERROR_EXPIRED_REQUIRED: 110,
        RES_TOKEN_ERROR_EXPIRED_NUMBER: 111,
        RES_TOKEN_ERROR_EXPIRED_LENGTH: 112,
        RES_TOKEN_ERROR_EXPIRED_INVALID: 113,
        RES_TOKEN_ERROR_SECURE_CODE_FORMAT: 121,
        RES_TOKEN_ERROR_SECURE_CODE_LENGTH: 122,
        RES_TOKEN_ERROR_NAME_FORMAT: 131,
        RES_TOKEN_ERROR_NAME_LENGTH: 132,
        RES_TOKEN_ERROR_OTHER_GT: 141,
        RES_TOKEN_ERROR_OTHER_LT: 902
      },
      card: {
        //@TODO: replace this
        cardId:'4111111111111111', //null,
        expired: '1901',//null,
        expiredMonth: null,
        expiredYear: null,
        ownerName: null,
        secureCode: null
      },
      payment: {
        token: null,
        settleInfo: null
      },
      cart: {
        reserveNo: null
      }
    }
  },
  computed: {
    ...mapState({
      cartData: state => state.booking.cartDetail || null,
      adminTime: state => state.auth.admin_time || null,
      user: state => state.auth.user,
      paymentStore: state => state.payment || null,
      reserveNo: state => state.booking.reserveNo || null,
      urikakeNo: state => state.booking.urikakeNo || null,
      urikakeType: state => state.booking.urikakeType || null,
      departEndTime: state => state.booking.departEndDtime || null,
      limitPaymentDays: state => state.booking.limitPaymentDays || null
    }),

    yearList() {
      let yearList = ['-'];
      for (let i = (new Date()).getFullYear(); i <= (new Date()).getFullYear() + 10; i++) {
        yearList.push(i.toString().substring(2));
      }
      return yearList;
    },

    monthList() {
      let monthList = ['-'];
      for (let i = 1; i <= 12; i++) {
        monthList.push(i);
      }
      this.card.expiredMonth = '';
      this.card.expiredYear = '';
      this.card.expired = '';
      return monthList;
    }
  },
  created() {

    //1.カート情報取得(SQL036)を行い、カートが存在しない場合は、エラー画面(570)に遷移し、メッセージ（msg005)を表示する
    // if (!this.checkExistsCart()) {
    //   this.redirectToError([
    //     this.$t('message.msg005_cart_not_exit')
    //   ]);
    // }
    // this.cartId = this.cartData[0].cart_id || null;
  },
  mounted() {
    this.$nextTick(() => {
      this.$nuxt.$loading.start();
      this.customMessageValidateCard();
      // Init value of settle info
      if (this.paymentStore && this.paymentStore.paymentMethod && this.paymentStore.paymentMethod.settle_info) {
        this.payment.settleInfo = this.paymentStore.paymentMethod.settle_info;
      }
      this.$nuxt.$loading.finish();
    });
  },
  methods: {
    /**
     * Customize message validate form card
     * @return {Promise<void>}
     */
    customMessageValidateCard() {
      const dict = {
        custom: {
          cardId: {
            required: this.$t('message.msg094_card_id_required'),
            numeric: this.$t('message.msg095_card_id_format'),
            min: this.$t('message.msg096_card_id_length'),
            max: this.$t('message.msg096_card_id_length')
          },
          expired: {
            required: this.$t('message.msg097_expired_required'),
            cardExpired: this.$t('message.msg098_expired_minmax')
          },
          ownerName: {
            required: this.$t('message.msg101_name_required'),
            max: this.$t('message.msg100_name_length'),
            regex: this.$t('message.msg099_name_format')
          },
          secureCode: {
            required: this.$t('message.msg104_secure_required'),
            max: this.$t('message.msg103_secure_length'),
            min: this.$t('message.msg103_secure_length'),
            numeric: this.$t('message.msg102_secure_format')
          }
        }
      }
      this.$validator.localize('ja', dict);
    },

    /**
     * Process payment card
     * @return {*}
     */
    async processPaymentCard() {
      // Validate form credit card
      let validate = await this.validateFormCard();
      if (!validate) {
        return false;
      }
      this.$nuxt.$loading.start();
      // st01: Get token of card if valid
      await this.getToken()
        .then(res => {
          // Handle and show error from api get token for form card
          if (res && res.resultCode.toString() != this.config.RES_TOKEN_SUCCESS) {
            // When result code >= 141, handle cancel reserve and redirec to error page 570
            // Else show error on screen credit card
            if (res.resultCode >= 141) {
              // st01.1-1 cancel order card
              this.cancelOrderCart(this.$route.params.client_id, this.reserveNo, this.urikakeNo)
                .then(res => {
                  //@Todo: Replace Message error
                  // st01-2 redirect to error page
                  this.redirectToError(['Error get token user']);
                });
            } else {
              // st01.2-1 Show error on screen payment card
              this.handleErrorGetToken(res);
            }
          } else {
            // Prepare token to server
            this.payment.token = res && res.tokenObject && res.tokenObject.token ? res.tokenObject.token : null;
            // st02: Handle payment card on API
            return this.handlePaymentCardApi(this.$route.params.client_id);
          }
        })
        .then(res => {
          console.log(res);
          if (res && res.data.data) {
            //When handle success, redirect to third-party site to authorization password
            //st02.1-1 Redirect to authorization third-party
            if (res.data.data.status == 200) {
              this.$router.push(res.data.data.redirect);
              //st02.2-1 Redirect to error page
            } else if(res.data.data.status == 599) {
              if (res.data.data.type && res.data.data.type == 1) {
                this.redirectToError([res.data.data.message]);
              }
            } else {
              //st02.3-1 Show error
              throw new Error(res.data.data.message);
            }
          }
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          // this.cancelOrderCart(this.$route.params.client_id, this.reserveNo, this.urikakeNo);
          this.$nuxt.$loading.finish();
          throw new Error(err);
        });
      console.log(this.payment);
      console.log(validate);
      console.log(123);
    },

    /**
     * Process payment card after user authorization from third-party site
     * @return {Promise<boolean>}
     */
    async processPaymentCardAfterAuthorization() {
      this.$nuxt.$loading.start();
      await this.handlePaymentCardAfterAuthorizationApi()
        .then(res => {
          console.log(res);
          if (res && res.data.data) {
            // When handle success, redirect to booking complete outside 360
            // Else handle cancel card and redirect to error page
            // ELse throw new error
            if (res.data.data.status == 200) {
              this.$router.push({name: router.BOOKING_COMPLETE_OUTSIDE, params: {client_id: this.$route.params.client_id}});
            } else if(res.data.data.status == 599) {
              if (res.data.data.type && res.data.data.type == 1) {
                this.redirectToError([res.data.data.message]);
              }
            } else {
              throw new Error(res.data.data.message);
            }
          }
          this.$nuxt.$loading.finish();
        })
        .catch(err => {
          // this.cancelOrderCart(this.$route.params.client_id, this.reserveNo, this.urikakeNo);
          this.$nuxt.$loading.finish();
          throw new Error(err);
        });
    },
    /**
     * Validate form card
     * @return {boolean}
     */
    validateFormCard() {
      return new Promise((resolve, reject) => {
        this.$validator.validate().then(result => {
          if (result) {
            resolve(true);
            return true;
          } else {
            resolve(false);
            return false;
          }
        })
          .catch(err => {
            reject(err);
          });
      });
    },

    /**
     * Get token of card
     * @return {Promise<any>}
     */
    getToken() {
      return new Promise((resolve, reject) => {
        let cardId = this.card.cardId || null;
        let expired = this.card.expired || null;
        $(function () {
          Multipayment.init('tshop00035916');
          Multipayment.getToken({
            cardno: cardId,
            expire: expired,
            // holdername: 'LUONG VIET DUNG'
          }, function (res) {
            resolve(res);
          });
        });
      })
    },


    /**
     * Show validate message from server
     * @param res
     * @return {Promise<void>}
     */
    async handleErrorGetToken(res) {
      if (res && res.resultCode != this.config.RES_TOKEN_SUCCESS) {
        switch (res.resultCode) {
          case this.config.RES_TOKEN_ERROR_CARD_REQUIRED:
            this.card.cardId = null;
            break;
          case this.config.RES_TOKEN_ERROR_CARD_FORMAT:
            this.card.cardId = "abc";
            break;
          case this.config.RES_TOKEN_ERROR_CARD_LENGTH:
            this.card.cardId = "123";
            break;
          case this.config.RES_TOKEN_ERROR_EXPIRED_REQUIRED:
            this.card.expired = null;
            break;
          case this.config.RES_TOKEN_ERROR_EXPIRED_NUMBER:
            this.card.expired = "abc";
            break;
          case this.config.RES_TOKEN_ERROR_EXPIRED_LENGTH:
            this.card.expired = "123";
            break;
          case this.config.RES_TOKEN_ERROR_EXPIRED_INVALID:
            this.card.expired = "1010";
            break;
          case this.config.RES_TOKEN_ERROR_SECURE_CODE_FORMAT:
            this.card.secureCode = "abc";
            break;
          case this.config.RES_TOKEN_ERROR_SECURE_CODE_LENGTH:
            this.card.secureCode = "12343435366566665";
            break;
          case this.config.RES_TOKEN_ERROR_NAME_FORMAT:
            this.card.ownerName = "&&????";
            break;
          case this.config.RES_TOKEN_ERROR_NAME_LENGTH:
            this.card.ownerName = "abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabca";
            break;
          default:
            this.redirectToError([
              this.$t('message.msg092_error_system')
            ])
            break;
        }
        this.$validator.validate();
      }
    },

    /**
     * Handle card on API
     * @return {VeeValidate<validate>}
     */
    handlePaymentCardApi(clientId) {
      return new Promise((resolve, reject) => {
        let dt = this.$encodeBase64Str(JSON.stringify({
          token_payment: this.payment.token,
          reserve_no: this.reserveNo,
          urikake_no: this.urikakeNo,
          urikake_type: this.urikakeType,
          depart_end_time: this.departEndTime,
          limit_payment_days: this.limitPaymentDays
        }));
        post(pathApi.HANDLE_PAYMENT_CARD,
          {
            client_id: clientId,
            data: dt.str,
            id: dt.key
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    /**
     * Handle card on API
     * @return {VeeValidate<validate>}
     */
    handlePaymentCardAfterAuthorizationApi(clientId) {
      return new Promise((resolve, reject) => {
        let dt = this.$encodeBase64Str(JSON.stringify({
          token_payment: this.payment.token,
          reserve_no: this.reserveNo,
          urikake_no: this.urikakeNo,
          urikake_type: this.urikakeType,
          depart_end_time: this.departEndTime,
          limit_payment_days: this.limitPaymentDays
        }));
        post(pathApi.HANDLE_PAYMENT_CARD_AFTER_AUTHORIZATION,
          {
            client_id: clientId,
            data: dt.str,
            id: dt.key
          })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    /**
     * Handle cancel reserve cart order
     * @return {Promise<void>}
     */
    cancelOrderCart(clientId, reserveNo, urikakeNo) {
      return new Promise((resolve, reject) => {
        post('cancel-all-reserve', {
          clientId: clientId || null,
          reserveNo: reserveNo || null,
          urikakeNo: urikakeNo || null
        })
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      })
    },

    /**
     * Redirect to error page 570 with message error
     * @return {VueRouter.push}
     */
    redirectToError(messages) {
      this.$store.dispatch('auth/setError', messages);
      return this.$router.push({name: router.ERROR_NAME});
    },

  },
  watch: {
    'card.expiredMonth'(value) {
      this.card.expired = this.card.expiredYear + '' + value
    },

    'card.expiredYear'(value) {
      this.card.expired = value + '' + this.card.expiredMonth;
    }
  }
}
