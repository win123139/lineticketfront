/**
 * File ConfirmMembershipBusiness.js
 * Handler business in page enter custom infomation
 *
 * @author Rikkei.DatDM
 * @date 2018-11-12
 */

import constant from '@/constant';
import { post, get } from '@/plugins/api';
import { mapState } from 'vuex';

// Constant pay creditcard
const CREDITCARD_SETTLE_CD = '102';
const NYUKIN_FLG_TOTAL_ZERO = 1;
const NYUKIN_FLG_TOTAL_NON_ZERO = 0;
const LIMIT_END_DATE = '99991231';


export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('member.title_confirm_membership')
    }
  },
  data: () => ({
    flg: {
      date_flg: true,
      member_enter_flg: true,
      membership_flg: true,
      unit_flg: true,
      membership_total_flg: true,
      convini_flg: true,
      pay_char: true
    },
    val: {
      date_value: '',
      member_enter_value: '',
      membership_value: '',
      unit: '',
      membership_total: '',
      convini_fee: '',
      pay_char: '',
      membership_total_pay: '',
      member_kb_type_nm: '',
      nyukin_flg: '',
      entry_cd: ''
    },
    msgError: '',
  }),
  created() {
    // Check register membership
    if (this.checkRegisterMembership()) {
      return;
    }
    this.initPage();
  },
  computed: {
    ...mapState({
      auth: state => state.auth,
      membership: state => state.member.member_ship,
      validStepConfirm: state => state.member.validStepConfirm,
      validStepComplete: state => state.member.validStepComplete
    })
  },
  methods: {
    /**
     * Init page
     * @return {void}
     */
    initPage: function(){
      this.flg.date_flg = (this.membership.limit_kb == 1) ? false : true;
      this.flg.member_enter_flg = (this.membership.member_enter_flg == 0 || this.membership.member_enter_fee == 0)
        ? false : true;
      this.flg.membership_flg = (this.membership.membership_flg == 0 || this.membership.membership_fee == 0)
        ? false : true;
      this.flg.unit_flg = (this.membership.unit == 1) ? false : true;
      this.flg.membership_total_flg = (this.membership.membership_total == 0) ? false : true;
      this.flg.convini_flg = (this.membership.membership_convini) ? true : false;
      this.flg.pay_char = (this.membership.membership_total == 0) ? false : true;


      this.val.member_kb_type_nm = this.membership.member_kb_type_nm;
      this.val.date_value = (this.membership.limit_end_date == '')
        ? this.membership.limit_end_date : this.formatDate(this.membership.limit_end_date);
      this.val.member_enter_value = this.membership.member_enter_fee;
      this.val.membership_value = this.membership.membership_fee;
      this.val.unit = this.membership.unit;
      this.val.membership_total = this.membership.membership_total;
      this.val.convini_fee = this.membership.convini_fee;
      this.val.pay_char = this.membership.pay_char;
      this.val.membership_total_pay = this.membership.membership_total_pay;
      this.val.nyukin_flg = this.membership.membership_total == 0 ? NYUKIN_FLG_TOTAL_ZERO : NYUKIN_FLG_TOTAL_NON_ZERO;
    },
    /**
     * Format display date
     * @param {String} date
     * @returns {String}
     */
    formatDate: function (date) {
      let date_start = new Date();
      let dd_start = date_start.getDate();
      let mm_start = date_start.getMonth() + 1;
      let yy_start = date_start.getFullYear();

      let date_end = new Date(date.substring(0, 4) + '/' + date.substring(4, 6) + '/' + date.substring(6, 8));
      let dd_end = date_end.getDate();
      let mm_end = date_end.getMonth() + 1;
      let yy_end = date_end.getFullYear();

      return yy_start + '/' + mm_start + '/' + dd_start + '～' + yy_end + '/' + mm_end + '/' + dd_end;
    },
    /**
     * Button back membership register
     * @return {void}
     */
    goToBack: function () {
      let path = this.$router.resolve({
        name: constant.router.MEMBERSHIP_REGISTER,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    },
    /**
     * Button next page membership completed
     * @return {void}
     */
    goToNextPage: function () {
      // Check condition choose register membership
      if (this.checkRegisterMembership()) {
        return;
      }
      this.checkStatusMember();
    },
    /**
     * Function redirect to page error
     * @returns {void}
     */
    redirectToError: function() {
      let path = this.$router.resolve({
        name: constant.router.ERROR_NAME,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    },
    /**
     * Check confirm register membership
     * @returns {boolean}
     */
    checkRegisterMembership: function () {
      // Check condition have been confirm go to screen complete
      if (this.validStepComplete) {
        let path = this.$router.resolve({
          name: constant.router.MY_PAGE,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);

        return true;
      }

      // Check condition no choose membership go to screen complete
      if (Object.keys(this.membership).length == 0 || !this.validStepConfirm) {
        let path = this.$router.resolve({
          name: constant.router.MEMBERSHIP_REGISTER,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);

        return true;
      }

      return false;
    },

    /**
     *
     */
    onlyAcceptCondition: function () {
      let status = true;

      if (!this.membership.member_list_information) return true;

      this.membership.member_list_information.forEach(el => {
        if (el.condition_kb != '0') status = false;
      });

      return status;
    },

    /**
     * Check status update member
     *
     * @return {void}
     */
    checkStatusMember: function () {
      this.$nuxt.$loading.start();

      // Call api check status update member
      post(constant.api.CHECK_STATUS_MEMBER, {
        client_id: this.$route.params.client_id,
        member_id: this.auth.user.member_id
      }).then(res => {
        // Get check_flg of sql065
        let check = res.data.data.check_update_membership[0];
        this.membership.member_list_information = res.data.data.membership_info;

        if (this.onlyAcceptCondition() && check.new_flg == '0') {
          this.msgError = this.$t('message.msg082_can_not_join_group', { group: this.membership.member_kb_type_nm });
        } else if (!this.onlyAcceptCondition() && check.continue_flg == '0') {
          this.msgError = this.$t('message.msg083_can_not_update', { group: this.membership.member_kb_type_nm });
        } else {
          // Else confirm data into database
          this.confirmMembership();
          return;
        }

        // Compare check_flg = 0 go to my page
        let path = this.$router.resolve({
          name: constant.router.MY_PAGE,
          params: {client_id: this.$route.params.client_id}
        });

        this.$store.dispatch('auth/setError', [
          this.msgError
        ]);

        this.$nuxt.$loading.finish();
        this.$store.dispatch('member/updateStepAccess', false);

        return path;
      }).then(path => {
        if (!path) return;

        this.$router.push(path.href);
      }).catch(err => {
        this.$nuxt.$loading.finish();

        let messages = [
          this.$t('message.msg003_exception.line_1'),
          this.$t('message.msg003_exception.line_2'),
          this.$t('message.msg003_exception.line_3')
        ];

        this.$store.dispatch('auth/setError', messages);

        this.redirectToError();
      })
    },
    /**
     * Call api confirm register membership
     * @return {void}
     */
    confirmMembership: function () {
      post(constant.api.POST_CONFIRM_MEMBERSHIP, {
        client_id: this.$route.params.client_id,
        member_id: this.auth.user.member_id,
        member_kb_no: this.membership.member_kb_no,
        member_type_no: this.membership.member_type_no,
        member_end_date: this.membership.limit_kb != 1 ? this.membership.limit_end_date : LIMIT_END_DATE,
        entry_cd: this.membership.entry_cd,
        unit: this.membership.unit,
        nyukin_flg: this.val.nyukin_flg,
        total_fee: this.membership.membership_total,
        settle_cd: this.membership.settle_cd,
        limit_payment_days: this.membership.limit_payment_days,
        urikake_gaku: this.membership.membership_total_pay,
        membership_fee: this.membership.membership_fee,
        member_enter_fee: this.membership.member_enter_fee,
        convini_fee: this.membership.convini_fee,
        flg_convini: this.membership.flg_convini,
        pay_char: this.membership.pay_char
      }).then(res => {
        // If pay is credit card go to page credit card pay
        if (this.membership.settle_cd == CREDITCARD_SETTLE_CD) {
          // TODO pass params to screen credit card
          // Go to page credit card pay
          alert('Go to page credit card pay');
          return;
        }

        // Go to next page complete register membership
        let path = this.$router.resolve({
          name: constant.router.MEMBERSHIP_COMPLETE,
          params: {client_id: this.$route.params.client_id}
        });

        return path.href;
      }).then(path => {
        // Set step confirm
        this.$store.dispatch('member/updateStepComplete', true);
        this.$store.dispatch('member/updateStepConfirm', false);

        if (typeof path === 'string' || path instanceof String) {
          this.$router.push(path);
        }
      }).then(res => {
        this.$nuxt.$loading.finish();
      }).catch(err => {
        // Finish progress bar
        this.$nuxt.$loading.finish();

        let res = err.response.data.data;
        let messages = [];

        if (res.status) {
          messages = [
            this.$t('message.msg093_cancel_membership'),
          ];
        } else {
          messages = [
            this.$t('message.msg003_exception.line_1'),
            this.$t('message.msg003_exception.line_2'),
            this.$t('message.msg003_exception.line_3')
          ];
        }

        this.$store.dispatch('auth/setError', messages);

        this.redirectToError();
        return;
      });
    }
  }
}
