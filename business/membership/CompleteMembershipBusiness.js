/**
 * File CompleteMembershipBusiness.js
 * Handler business in page enter custom infomation
 *
 * @author Rikkei.DatDM
 * @date 2018-11-27
 */

import constant from '@/constant';
import { post, get } from '@/plugins/api';
import { mapState } from 'vuex';
import ClientInfo from "@/components/UI/ClientInfo";

export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.$t('member.title_complete_membership')
    }
  },
  components: {
    ClientInfo
  },
  data: () => ({
    model: {
      inquiryNm: '',
      inquiryTelNo: '',
      inquiryUrl: '',
      inquiryNote: ''
    },
    pathToMyPage: ''
  }),
  created() {
    this.initPage();

    // Path to my page
    let pathToHome = this.$router.resolve({
      name: constant.router.MY_PAGE,
      params: { client_id: this.$route.params.client_id }
    });

    this.pathToMyPage = pathToHome.href;
  },
  computed: {
    ...mapState({
      auth: state => state.auth,
      client: state => state.client,
      member: state => state.member
    })
  },
  methods: {
    /**
     * Init Page
     * @return {void}
     */
    initPage: function () {
      // Check condition go to screen confirm
      if (this.member.validStepConfirm) {
        let path = this.$router.resolve({
          name: constant.router.MEMBERSHIP_CONFIRM,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
        return;
      }

      // Check condition go to my page
      if (!this.member.validStepComplete) {
        let path = this.$router.resolve({
          name: constant.router.MY_PAGE,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
        return;
      }

      this.model.inquiryNm = this.client.clientInfo.inquiry_nm;
      this.model.inquiryTelNo = this.client.clientInfo.inquiry_tel_no;
      this.model.inquiryUrl = this.client.clientInfo.inquiry_url;
      this.model.inquiryNote = this.client.clientInfo.inquiry_notes;

      // Send mail
      if (Object.keys(this.member.member_ship).length != 0) {
        this.sendMailAfterComplete();
      }

      // Set step
      this.$store.dispatch('member/unsetMemberShip');
      this.$store.dispatch('member/updateStepAccess', false);
    },
    /**
     * Send mail after complete membership
     * @returns {*}
     */
    sendMailAfterComplete: function () {
      return post(constant.api.SEND_MAIL_MEMBERSHIP,{
        client_id: this.$route.params.client_id,
        member_id: this.auth.user.member_id
      }).then(result => {
        // Reset infomation member
        localStorage.setItem("token", result.data.data.token);

        // Reset state after login
        this.$store.dispatch('auth/setUser', result.data.data.userInf);
        this.$store.dispatch('member/unsetMember');
        this.$store.dispatch('member/unsetError');
        this.$nuxt.$loading.finish();
      }).catch(err => {
        let messages = [
          this.$t('message.msg003_exception.line_1'),
          this.$t('message.msg003_exception.line_2'),
          this.$t('message.msg003_exception.line_3')
        ];

        this.$store.dispatch('auth/setError', messages);

        this.redirectToError();
      });
    },
    /**
     * Function redirect to page error
     * @returns {void}
     */
    redirectToError: function() {
      let path = this.$router.resolve({
        name: constant.router.ERROR_NAME,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    }
  }
}
