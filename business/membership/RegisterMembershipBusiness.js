/**
 * File RegisterMembershipBusiness.js
 * Handler business in page enter custom infomation
 *
 * @author Rikkei.DatDM
 * @date 2018-11-12
 */

import constant from '@/constant';
import { post } from '@/plugins/api';
import { mapState } from 'vuex';

const NO_PAY = 0;
const CASH = 1;
const CREDIT = 2;
const CONVINI = 3;
const BANK = 4;
const CONVINI_FEE = 300000;
const LIMIT_KB_4 = 4;
const ENTRY_CD_INSERT = 1;
const ENTRY_CD_UPDATE = 2;

export default {
  middleware: ['authenticated', 'redirect_if_is_admin'],
  head() {
    return {
      title: this.membership.action == constant.config.ACTION_NEW_JOIN
        ? this.$t('member.title_register_membership')
        : this.$t('member.title_update_membership')
    }
  },
  /**
   * Loading route before DOM
   *
   * @param {*} to
   * @param {*} from
   * @param {*} next
   */
  beforeRouteEnter (to, from, next) {
    // Check if access from other page except 480 and current page
    if (from.name != constant.router.MEMBERSHIP_CONFIRM && from.name != null) {
      localStorage.removeItem('member_ship');
    } else {
      localStorage.setItem('member_ship', true);
    }

    next();
  },
  data: () => ({
    isNewRegister: constant.config.ACTION_NEW_JOIN,
    membership_group: [],
    member_list_information: [],
    membership_terms_url: '',
    model: {
      unit: '',
      trans_kb: '',
      member_kb_type_nm: '',
      member_kb_no: '',
      member_type_no: '',
      limit_end_date: '',
      limit_kb: '',
      member_enter_flg: '',
      member_enter_fee: '',
      membership_flg: '',
      membership_fee: '',
      convini_fee: '',
      entry_cd: '',
      settle_cd: '',
      flg_convini: '',
      pay_char: '',
      isCheck: false,
      index: null
    },
    isCheck: false,
    provision_check: false,
    validation: false,
    radio_button: true,
    define: {
      cash: CASH,
      credit: CREDIT,
      convini: CONVINI,
      bank: BANK
    },
    member_kb_type_nm: '',
    flg: {
      cash: false,
      credit: false,
      convini: false,
      bank: false,
      membership_type: false,
      unit: false,
      trans_kb: false,
      checkbox: false,
      max_value: false,
      min_value: false,
      update: false,
      condition_kb: false,
      pay_one: false
    },
    unit: '',
    data_check: ''
  }),
  created() {
    // Check valid access page then redirect to my page
    if (!this.membership.validAccessPage) {
      // Redirect to page register into membership
      let path = this.$router.resolve({
        name: constant.router.MY_PAGE,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);

      return;
    }

    this.validation = false;
    // unset state membership
    this.$store.dispatch('member/updateStepConfirm', false);
    this.$store.dispatch('member/updateStepComplete', false);

    this.initPage();

    this.checkIsBackFromConfirm();
  },
  mounted() {
    this.$nextTick(() => {
      // Go to top
      window.scrollTo(0, 0);
      // Loading start
      this.$nuxt.$loading.start();
    })
  },
  watch: {
    // Click radio member_kb_type_nm
    member_kb_type_nm: function () {
      this.model.index = this.member_kb_type_nm;

      // Check is reset trans_kb and unit when have localStorage member_ship
      if (!localStorage.getItem('member_ship')) {
        this.model.trans_kb = (this.flg.cash || this.flg.credit || this.flg.convini || this.flg.bank) ? this.model.trans_kb : '';
        this.model.unit = '';
      } else {
        // Remove localstorage member_ship
        localStorage.removeItem('member_ship');
      }
    },
    // Click checkbox provision to display button
    provision_check: function () {
      if (!this.provision_check) {
        this.isCheck = false;

      } else {
        this.isCheck = true;
      }
    }
  },
  computed: {
    ...mapState({
      auth: state => state.auth,
      membership: state => state.member
    })
  },
  methods: {
    /**
     * Init page
     *
     * @return {void}
     */
    initPage: function () {
      // Call api get information membership
      post(constant.api.INFO_MEMBER_GROUP, {
        client_id : this.$route.params.client_id,
        member_id: this.auth.user.member_id
      }).then(result => {
        // Get data from api
        this.membership_group = result.data.data.membership_group;
        this.membership_terms_url = result.data.data.membership_terms_url;
        this.member_list_information = result.data.data.member_list_information;

        // Format list information membership
        this.formatListMember();

        // Check list membership group is not exists
        if (this.membership_group.length < 1) {
          this.isCheck = false;
        }

        // Check list membership group only 1 record
        if (this.membership_group.length == 1) {
          this.member_kb_type_nm = 0;
        }

        // Check case update membership
        this.checkUpdate();

        this.$nuxt.$loading.finish();
      }).catch(err => {
        this.$store.dispatch('auth/setError', [
          this.$t('message.msg003_exception.line_1'),
          this.$t('message.msg003_exception.line_2'),
          this.$t('message.msg003_exception.line_3')
        ]);

        this.$nuxt.$loading.finish();

        // Redirect to page error 570 later
        let path = this.$router.resolve({
          name: constant.router.ERROR_NAME,
          params: { client_id: this.$route.params.client_id }
        });

        this.$router.push(path.href);
      });
    },

    /**
     * Click button next page
     *
     * @return {void}
     */
    goToNextPage: function () {
      if (this.validationButton()) {
        window.scrollTo(0, 0);
        return;
      }

      // Set model save state
      this.setModel();

      this.$store.dispatch('member/updateStepConfirm', true);
      this.$store.dispatch('member/setMemberShip', this.model);

      let path = this.$router.resolve({
        name: constant.router.MEMBERSHIP_CONFIRM,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    },

    /**
     * Validation button next
     *
     * @returns {boolean}
     */
    validationButton: function () {
      this.validation = false;
      this.flg.membership_type = false;
      this.flg.unit = false;
      this.flg.unit_format = false;
      this.flg.max_value = false;
      this.flg.min_value = false;
      this.flg.trans_kb = false;
      this.flg.checkbox = false;
      this.flg.convini = false;
      this.flg.condition_kb = false;

      // Check checkbox is selected
      if (!this.provision_check) {
        this.validation = true;
        this.flg.checkbox = true;

        return this.validation;
      }
      // Check choose radio membership group
      if (this.member_kb_type_nm === '') {
        this.validation = true;
        this.flg.membership_type = true;

      } else {

        this.data_check = this.membership_group[this.member_kb_type_nm];
        this.unit = this.data_check.unit;

        // Check poried update
        if (this.data_check.condition_kb && !this.validation) {
          this.validation = true;
          this.flg.condition_kb = true;

          return this.validation;
        }

        // Get pay
        if (this.data_check.pay_one != NO_PAY) {
          this.model.trans_kb = this.data_check.pay_one;
        }

        // Get field unit = 1
        if (this.data_check.unit == 1) {
          this.model.unit = this.data_check.unit;
        }

        // Get field trans_kb = 0
        if (this.data_check.flg_no_pay) {
          this.model.trans_kb = NO_PAY;
        }

        // Check field unit required
        if (this.model.unit === '' && this.data_check.unit > 1 ) {
          this.validation = true;
          this.flg.unit = true;
        }

        // Check pay required
        let check_trans_kb = !this.data_check.flg_cash
          && !this.data_check.flg_credit
          && !this.data_check.flg_convini
          && !this.data_check.flg_bank;

        if (this.model.trans_kb === '' && !check_trans_kb) {
          this.validation = true;
          this.flg.trans_kb = true;
        }

        // Check field unit is integer
        if (this.data_check.unit > 1 && !$.isNumeric(this.model.unit) && this.model.unit !== '') {
          this.validation = true;
          this.flg.unit_format = true;
        }

        // Check field unit is negative number
        if (this.data_check.unit > 1 && this.model.unit < 1 && this.model.unit !== '') {
          this.validation = true;
          this.flg.min_value = true;
        }

        // Check field unit is more than value default
        if (this.data_check.unit > 1 && this.model.unit > this.unit && this.model.unit !== '') {
          this.validation = true;
          this.flg.max_value = true;
        }

        // Check convini_fee more then 30万
        if (this.data_check.convini_fee != 0 && this.data_check.convini_fee > CONVINI_FEE && this.model.trans_kb == CONVINI) {
          this.validation = true;
          this.flg.convini = true;
        }
      }

      return this.validation;
    },

    /**
     * Check case update membership
     *
     * @return {void}
     */
    checkUpdate: function () {
      let dateCurrent = new Date();

      for (var ele of this.membership_group) {

        for (var ele2 of this.member_list_information) {
          let checkDate = this.formatDate(ele2.member_start_date) <= dateCurrent && dateCurrent <= this.formatDate(ele2.member_end_date);
          let checkCondition = ele2.condition_kb == 2;
          let checkNo = ele2.member_type_no == ele.member_type_no && ele2.member_kb_no == ele.member_kb_no;
          if (checkDate && checkCondition && checkNo) {
            this.flg.update = true;
            this.membership_group = [];
            this.membership_group.push(ele);
            this.member_kb_type_nm = 0;
            break;
          }

          if (this.flg.update) {
            break;
          }
        }
      }
    },

    /**
     * Format date
     *
     * @param date
     * @returns {Date}
     */
    formatDate: function (date) {
      if (date == '') return false;
      return new Date(date.substring(0, 4) + '-' + date.substring(4, 6) + '-' + date.substring(6, 8));
    },

    /**
     * Format list information membership
     *
     * @return {void}
     */
    formatListMember: function () {
      let dateCurrent = new Date();
      this.membership_group.forEach((ele, ind) => {
        // No pay
        this.membership_group[ind].pay_one = NO_PAY;
        // Membership fee and Member enter fee
        let fee = (ele.membership_fee != 0 || ele.member_enter_fee != 0) && (ele.membership_flg != 0 || ele.member_enter_flg != 0);
        // Condition field cash display
        let flg_cash = ele.cash_trans_kb == 1 && fee;
        // Condition field creditcard display
        let flg_credit = ele.creditcard_trans_kb == 1 && fee;
        // Condition field convini display
        let flg_convini = ele.convini_trans_kb == 1 && fee;
        // Condition field bank display
        let flg_bank = ele.bank_trans_kb == 1 && fee;
        // Condition pay is display
        let flg_no_pay = (ele.member_enter_flg == 0 && ele.membership_flg == 0) || (ele.member_enter_fee + ele.membership_fee == 0);

        // Set flag
        this.membership_group[ind].flg_cash = flg_cash;
        this.membership_group[ind].flg_credit = flg_credit;
        this.membership_group[ind].flg_convini = flg_convini;
        this.membership_group[ind].flg_bank = flg_bank;
        this.membership_group[ind].flg_no_pay = flg_no_pay;

        // Check display 1 pay cash
        if (flg_cash && !flg_credit && !flg_convini && !flg_bank) {
          this.flg.cash = true;
          this.model.trans_kb = CASH;
          this.membership_group[ind].flg_cash_radio = false;
          this.membership_group[ind].pay_one = CASH;
        } else {
          this.membership_group[ind].flg_cash_radio = true;
        }

        // Check display 1 pay credit
        if (!flg_cash && flg_credit && !flg_convini && !flg_bank) {
          this.flg.credit = true;
          this.model.trans_kb = CREDIT;
          this.membership_group[ind].flg_credit_radio = false;
          this.membership_group[ind].pay_one = CREDIT;
        } else {
          this.membership_group[ind].flg_credit_radio = true;
        }

        // Check display 1 pay convini
        if (!flg_cash && !flg_credit && flg_convini && !flg_bank) {
          this.flg.convini = true;
          this.model.trans_kb = CONVINI;
          this.membership_group[ind].flg_convini_radio = false;
          this.membership_group[ind].pay_one = CONVINI;
        } else {
          this.membership_group[ind].flg_convini_radio = true;
        }

        // Check display 1 pay bank
        if (!flg_cash && !flg_credit && !flg_convini && flg_bank) {
          this.flg.bank = true;
          this.model.trans_kb = BANK;
          this.membership_group[ind].flg_bank_radio = false;
          this.membership_group[ind].pay_one = BANK;
        } else {
          this.membership_group[ind].flg_bank_radio = true;
        }

        // Check case not execute membership
        let checkMembership = ele.limit_kb == LIMIT_KB_4 && this.checkCondtionKb()
          && (ele.not_enter_limit_date && dateCurrent >= this.formatDate(ele.not_enter_limit_date));
        if (checkMembership) {
          this.membership_group[ind].condition_kb = true;
        } else {
          this.membership_group[ind].condition_kb = false;
        }
      });
    },

    /**
     * Check case dont take part in membership in value of list condtion_kb = 0
     *
     * @returns {Boolean}
     */
    checkCondtionKb: function () {
      if (this.member_list_information.length) {
        let condition = true;

        this.member_list_information.forEach(el => {
          if (el.condition_kb != '0') condition = false;
        });

        return condition;
      }

      return false;
    },

    /**
     * Go to my page
     *
     * @return {void}
     */
    goToMyPage: function () {
      this.$store.dispatch('member/unsetMemberShip');

      let path = this.$router.resolve({
        name: constant.router.MY_PAGE,
        params: { client_id: this.$route.params.client_id }
      });

      this.$router.push(path.href);
    },

    /**
     * Set model save state
     *
     * @return {void}
     */
    setModel: function () {
      this.model.member_kb_type_nm = this.data_check.member_kb_type_nm;
      this.model.member_kb_no = this.data_check.member_kb_no;
      this.model.member_type_no = this.data_check.member_type_no;
      this.model.limit_end_date = this.data_check.limit_end_date;
      this.model.limit_kb = this.data_check.limit_kb;
      this.model.entry_cd = this.flg.update ? ENTRY_CD_UPDATE : ENTRY_CD_INSERT;
      this.model.member_enter_flg = this.data_check.member_enter_flg;
      this.model.member_enter_fee = this.data_check.member_enter_fee;
      this.model.membership_flg = this.data_check.membership_flg;
      this.model.membership_fee = this.data_check.membership_fee;
      this.model.convini_fee = this.data_check.convini_fee;
      this.model.membership_total = parseInt(this.data_check.member_enter_fee) + parseInt(this.data_check.membership_fee) * parseInt(this.model.unit);
      this.model.limit_payment_days = this.data_check.limit_payment_days;
      this.model.isCheck = true;
      this.model.member_list_information = this.member_list_information;

      // Save pay
      switch (this.model.trans_kb) {
        case CASH:
          this.model.membership_convini = false;
          this.model.pay_char = this.data_check.cash_settle_nm;
          this.model.membership_total_pay = this.model.membership_total;
          this.model.settle_cd = this.data_check.cash_settle_cd;
          this.model.flg_convini = false;
          break;
        case CREDIT:
          this.model.membership_convini = false;
          this.model.pay_char = this.data_check.creditcard_settle_nm;
          this.model.membership_total_pay = this.model.membership_total;
          this.model.settle_cd = this.data_check.creditcard_settle_cd;
          this.model.flg_convini = false;
          break;
        case CONVINI:
          this.model.membership_convini = true;
          this.model.pay_char = this.data_check.convini_settle_nm;
          this.model.membership_total_pay = this.model.membership_total + parseInt(this.data_check.convini_fee);
          this.model.settle_cd = this.data_check.convini_settle_cd;
          this.model.flg_convini = true;
          break;
        case BANK:
          this.model.membership_convini = false;
          this.model.pay_char = this.data_check.bank_settle_nm;
          this.model.membership_total_pay = this.model.membership_total;
          this.model.settle_cd = this.data_check.bank_settle_cd;
          this.model.flg_convini = false;
          break;
        default:
          this.model.membership_convini = false;
          this.model.pay_char = '';
          this.model.membership_total_pay = this.model.membership_total;
          this.model.flg_convini = false;
      }
    },

    /**
     * Function check is back from confirm page 480
     *
     * @returns {void}
     */
    checkIsBackFromConfirm: function () {
      // When backing from other page not 480, or when F5 page without confirming action then remove state
      if (!localStorage.getItem('member_ship') || !Object.keys(this.membership.member_ship).length) {
        this.$store.dispatch('member/unsetMemberShip');
      } else {
        // Keep state when backing from the page 480 or reloading page after backing from the page 480
        this.isCheck = true;
        this.provision_check = true;
        this.model.trans_kb = this.membership.member_ship.trans_kb;
        this.model.unit = this.membership.member_ship.unit;

        // When setting member_kb_type_nm then model member_kb_type_nm is change and watcher member_kb_type_nm is runing
        this.member_kb_type_nm = this.membership.member_ship.index;
      }
    }
  }
}
