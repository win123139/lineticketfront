/**
 * Real time booking seat use vue-apollo
 */
import Vue from 'vue';
import AWSAppSyncClient, {  createAppSyncLink } from "aws-appsync";
import VueApollo from 'vue-apollo';
import fet from 'unfetch';
import appsync_local from '@/constant/appsync_local';
import { createHttpLink, HttpLink } from 'apollo-link-http';
import { InMemoryCache } from "apollo-cache-inmemory";
import 'isomorphic-fetch';
import 'core-js';
import { onError } from 'apollo-link-error';

const config = {
    disableOffline: true
};

const resetToken = onError(({ response, networkError }) => {
    if (networkError && networkError.statusCode === 403) {
        // remove cached token on 401 from the server
        window.location.reload();
    }
});

let appsync = {};
let appName = '';
if(process.env.environment == 'develop') {
  appsync = appsync_local.develop;
  appName = appsync_local.develop.appName;
}
if(process.env.environment == 'product') {
  appsync = appsync_local.product;
  appName = appsync_local.product.appName;
}
const uri = appsync.graphqlendpoint;
const  httpLink = createHttpLink({
    // You should use an absolute URL here
    uri,
    fet,
    cache: new InMemoryCache(),
});

const link = createAppSyncLink({
    url: appsync.graphqlendpoint,
    region: appsync.region,
    auth: {
        type: appsync.authenticationType,
        apiKey: appsync.apiKey,
    },
    httpLink,
    resetToken
});

const options = {
    link,
    defaultOptions: {
        watchQuery: {
            fetchPolicy: 'cache-and-network',
        }
    }
};

const client = new AWSAppSyncClient(config, options);

const appsyncProvider = new VueApollo({
    defaultClient: client
});

Vue.config.productionTip = false;
Vue.use(VueApollo);

export  {appsyncProvider,appName}