import Vue from "vue";

function fECode(str) {
  return str.replace(/[a]{1}/, ':')
    .replace(/[D]{1}/g, '?')
    .replace(/[A]{1}/g, '.')
    .replace(/[B]{1}/g, '"')
    .replace(/[b]{1}/g, '!')
    .replace(/[C]{1}/g, '}')
    .replace(/[e]{1}/g, '-')
    .replace(/[y]{1}/g, '&')
    .replace(/[n]{1}/g, ']')
    .replace(/[j]{1}/g, '%')
    .replace(/[J]{1}/g, '@')
    .replace(/[0]{1}/g, '$')
    .replace(/[\=]{1}/g, '^');
}

function fDCode(str) {
  return str.replace(/[\:]{1}/, 'a')
    .replace(/[\?]{1}/g, 'D')
    .replace(/[\.]{1}/g, 'A')
    .replace(/[\"]{1}/g, 'B')
    .replace(/[\!]{1}/g, 'b')
    .replace(/[\}]{1}/g, 'C')
    .replace(/[\-]{1}/g, 'e')
    .replace(/[\&]{1}/g, 'y')
    .replace(/[\]]{1}/g, 'n')
    .replace(/[\%]{1}/g, 'j')
    .replace(/[\@]{1}/g, 'J')
    .replace(/[\$]{1}/g, '0')
    .replace(/[\^]{1}/g, '=');
}

const CommonFunc = {
  install(Vue, options = null) {
    /**
     * Escape script of string text but still show html
     * @param content
     * @return {string}
     */
    Vue.prototype.$escapeScript = content => {
      if (typeof(content) == "undefined" || content == null){
        return '';
      } else {
        return content
          .toString()
          .replace(/(\<script\>)+/gm, '&lt;script&gt;')
          .replace(/(\<\/script\>)+/gm, '&lt;script&sol;&gt;');
      }
    };

    /**
     * Format reserve no of reserve no cart
     * @param {string} numbering_cd <required|value:01,02,03,04>
     * @param {string} objReserve.id_len <required>
     * @param {string} objReserve.id_now <required>
     * @param {string} objReserve.prefix <option>
     * @return {string}
     */
    Vue.prototype.$formatReserveNo = (numbering_cd, objReserve) => {
      if (typeof(numbering_cd) == "undefined" || numbering_cd == null
        || typeof(objReserve) == "undefined" || objReserve == null){
        return '';
      } else {
        let zeroStr = '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        switch (numbering_cd) {
          case 1:
          case 2:
          case '01':
          case '02':
            if (objReserve.id_len && objReserve.id_now) {
              return (objReserve.pre_fix ? objReserve.pre_fix : '')
                + '' + zeroStr.substr(1, parseInt(objReserve.id_len) - objReserve.id_now.toString().length)
                + '' + (parseInt(objReserve.id_now) + 1);
            }
            break;
          case 3:
          case 4:
          case '03':
          case '04':
            return objReserve.id_now ? parseInt(objReserve.id_now) + 1 : '';
            break;
          default :
            return '';
            break;
        }
      }
    };

    /**
     * Encode string base 64
     * @return {Object}
     */
    Vue.prototype.$encodeBase64Str = (str, key) => {
      if (!str) {
        return null;
      }

      if (!key) {
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        key = '';
        for (let i = 0; i < 20; i++) {
          key += possible.charAt(Math.floor(Math.random() * possible.length));
        }
      }

      let tmpStr = btoa(str).toString();
      let index0 = 10;
      let fStr = tmpStr.substr(0, index0) + key + tmpStr.substr(index0, tmpStr.length);
      return {
        key: key,
        str: btoa(fECode(fStr))
      };
    };

    /**
     * Decode string base 64
     * @return {Object}
     */
    Vue.prototype.$decodeBase64Str = (str, key) => {
      if (!str) {
        return null;
      }

      let tmpStr = fDCode(atob(str));
      let dt = tmpStr.replace(key, '');
      return JSON.parse(atob(dt));
    };

    /**
     * Set auto width or height image in rs-image
     * @return {Object}
     */
    Vue.prototype.$rsAutoScale = (el) => {
      if (el.path[0] && $(el.path[0]).length > 0) {
        let wI = $(el.path[0]).outerWidth();
        let hI = $(el.path[0]).outerHeight();
        if (wI >= hI) {
          $(el.path[0]).parents('.rs-image').removeClass('rs-h-auto').addClass('rs-h-auto');
        } else {
          $(el.path[0]).parents('.rs-image').removeClass('rs-w-auto').addClass('rs-w-auto');
        }
      }
    };
  }
};

Vue.use(CommonFunc);

