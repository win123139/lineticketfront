/**
 * File mail.js
 * Check format mail with 200 character
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-15
 */

export default {
  getMessage(field, args) {
    return '';
  },
  validate(value, args) {
    let regex = /^(?=.{1,200}$)(([^`!#$%^&&*<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"']+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return  value.search(regex) != -1;
  }
}
