/**
 * File length_between.js
 * Check string length between the digits
 *
 * @author Rikkei.TriHNM
 * @date 2018-12-07
 */

export default {
  validate(value, args) {
    let [min, max] = args;

    return value.toString().length >= min && value.toString().length <= max;
  }
}
