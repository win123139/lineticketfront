/**
 * File exists_member_code.js
 * Check membercode is exists in database
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-05
 */

import { get } from '../../plugins/api';
import constant from '../../constant';

const IS_ADMIN_MODE = 1;

export default {
  getMessage(field, args) {
    return '';
  },
  validate(value, args) {
    let [clientId, memberNm, memberKn, mobileNo, telNo] = args;
    let params = {
      client_id: clientId,
      code: value,
      member_nm: memberNm,
      member_kn: memberKn,
      mobile_no: mobileNo,
      tel_no: telNo
    }

    // Check is admin mode
    if ($nuxt.$store.state.auth.admin_flag == IS_ADMIN_MODE) {
      params.web_permission_kb = $nuxt.$store.state.auth.admin_flag;
    }

    // Call api check exists member code
    return get(constant.api.CHECK_EXISTS_MEMBER_CODE, params).then(res => {
      let data = res.data.data.exists_code;
      let isValid = Object.keys(data).length !== 0;

      if (isValid) {
        localStorage.setItem('member_inf', JSON.stringify(data));
      }

      return isValid;
    }).catch(err => {
      return false;
    });
  }
}
