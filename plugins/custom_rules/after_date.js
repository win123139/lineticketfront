/**
 * File after_date.js
 * Check after date with format YYYY-MM-DD
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-15
 */

export default {
  getMessage(field, args) {
    return '';
  },
  validate(value, args) {
    let [date] = args;

    date = date.replace(/[-]/g, '');
    value = value.replace(/[-]/g, '');

    return (parseInt(date) < parseInt(value));
  }
}
