/**
 * File before_date.js
 * Check before date with format YYYY-MM-DD
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-30
 */

export default {
  validate(value, args) {
    let [date] = args;

    date = date.replace(/[-]/g, '');
    value = value.replace(/[-]/g, '');

    return (parseInt(date) > parseInt(value));
  }
}
