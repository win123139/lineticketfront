/**
 * File kana_fullsize.js
 * Check string is have space fullsize inner or not
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-13
 */

const ENTER_SPACE = 1;
const REMOVE_SPACE = 2;

export default {
  getMessage(field, args) {
    return '';
  },
  validate(value, args) {
    let [type] = args;
    let regex = null;

    if (type == ENTER_SPACE) {
      // Regex check string is katakana fullsize and have space fullsize inside
      regex = /^[ァ-ンー]+(　)[ァ-ンー]+$/g;
    } else if (type == REMOVE_SPACE) {
      // Regex check string is katakana fullsize and not have space inside
      regex = /^[ァ-ンー]+$/g;
    } else {
      // Regex check string is katakana fullsize with have space or not have space
      regex = /^(?=.*[ァ-ン])[ァ-ン　ー]+$/g;
    }

    return value.search(regex) != -1;
  }
}
