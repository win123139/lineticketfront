/**
 * File fullsize.js
 * Check string is fullsize
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-04
 */
export default {
  getMessage(field, args) {
    return '';
  },
  validate(value, args) {
    // Regex check string is matching all full width
    let regex = /^[^ -~｡-ﾟ\x00-\x1f\t]+$/g;

    return  value.search(regex) != -1;
  }
}
