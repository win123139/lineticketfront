import spaceFullSize from './space_fullsize';
import kanaFullSize from './kana_fullsize';
import fullsize from './fullsize';
import phoneNumber from './phone_number';
import passwordRegex from './password_regex';
import textNumberHaftSize from './text_number_haftsize';
import existsMail from './exists_mail';
import existsMemberCode from './exists_member_code';
import existsMailUpdate from './exists_mail_update';
import existsLoginId from './exists_login_id';
import existsOrder from './exists_order';
import afterDate from './after_date';
import mail from './mail';
import dateFormat from './format_date';
import beforeDate from './before_date';
import cardExpired from './card_expired';
import lengthBetween from './length_between';

export default {
  spaceFullSize,
  kanaFullSize,
  fullsize,
  phoneNumber,
  passwordRegex,
  textNumberHaftSize,
  existsMail,
  existsMemberCode,
  existsMailUpdate,
  existsLoginId,
  existsOrder,
  afterDate,
  mail,
  dateFormat,
  beforeDate,
  cardExpired,
  lengthBetween
}
