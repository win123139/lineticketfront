/**
 * File card_expired.js
 * Check card expired
 *
 * @author Rikkei.DungLV
 * @date 2018-12-03
 */

export default {
  validate(value, args) {
    let [date] = args;
    let now = new Date();
    let currentYearMonth = (now.getFullYear() + '' + (now.getMonth() + 1)).substring(2);
    return (parseInt(date) >= parseInt(currentYearMonth));
  }
}
