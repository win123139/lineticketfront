/**
 * File format_date.js
 * Check date with format
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-30
 */

import moment from 'moment';

export default {
  validate(value, args) {
    let [format] = args;

    return moment(value, format).format(format) === value;
  }
}
