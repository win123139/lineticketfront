/**
 * Convert price to JP format
 *
 * @version 1.0
 * @author Rikkei.DuyN
 * @date 2018-11-08
 */

import Vue from "vue";
const ConvertMoney = {
  install(Vue, options = null) {
    Vue.prototype.$currency = (price, hasPrefix = true) => {
      if (typeof(price) == "undefined" || price == null){
        return '';
      } else {
        price = price.toString();
        if (isNaN(price)) return false; //check if not a number
        let length = price.length;
        let result = [];
        let index = 0;
        while (length > 3) {
          result[index] = price.substring(length - 3, length);
          length -= 3;
          index++;
        }
        result[index] = price.substring(0, length);
        result.reverse();
        let strResult = result.join(",");
        if (hasPrefix && strResult != '') {
          strResult += '円';
        }
        return strResult;
      }
    };
  }
};

Vue.use(ConvertMoney);
