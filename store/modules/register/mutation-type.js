/**
 * File mutation-type.js
 * Define action update state in register
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-08
 */

export const SET_MODEL = 'SET_MODEL';
export const REMOVE_MODEL = 'REMOVE_URL';
export const UPDATE_FLAG_STEP_ONE = 'UPDATE_FLAG_STEP_ONE';
export const UPDATE_FLAG_STEP_TWO = 'UPDATE_FLAG_STEP_TWO';
export const SYNC = 'SYNC';

export default {
  SET_MODEL,
  REMOVE_MODEL,
  UPDATE_FLAG_STEP_ONE,
  UPDATE_FLAG_STEP_TWO,
  SYNC
}
