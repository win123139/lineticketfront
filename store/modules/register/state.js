/**
 * File state.js
 * Define state in register
 *
 * @author Rikkei.TriHNM
 * @date 2018-10-08
 */

export default {
  model: '',
  validStepOne: false,
  validStepTwo: false,
}
