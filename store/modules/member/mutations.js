/**
 * File mutations.js
 * Handler mutations set state
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-12
 */

import * as types from './mutation-type';
import constant from '@/constant';

export default {
  [types.SET_MEMBER](state, { member, listGenre }) {
    state.member_info = {};

    member.month = parseInt(member.month) < 10 ? '0' + parseInt(member.month) : parseInt(member.month);
    member.day = parseInt(member.day) < 10 ? '0' + parseInt(member.day) : parseInt(member.day);

    state.member_info.login_id = member.loginId;
    state.member_info.mail_address = member.mail;
    state.member_info.mail_address = member.confirmedMail;
    state.member_info.member_nm = member.fullName;
    state.member_info.member_kn = member.furigana;
    state.member_info.tel_no = member.phoneNumber;
    state.member_info.mobile_no = member.cellPhone;
    state.member_info.birthday = member.year + member.month + member.day;
    state.member_info.post_no = member.postCode;
    state.member_info.prefecture = member.slbCity;
    state.member_info.municipality = member.district;
    state.member_info.address1 = member.address;
    state.member_info.address2 = member.buildingRoom;
    state.member_info.sex_type = member.gender == 'male' ? constant.config.MALE : constant.config.FEMALE;
    state.member_info.member_id = member.memberCode;
    state.member_info.mail_send_flg = member.magazineMail;
    state.member_info.post_send_flg = member.directMail;
    state.member_info.list_genre = member.listGenre;
    state.member_info.member_code_input = member.memberCodeInput;
    state.member_info.flagShowMemberCode = member.flagShowMemberCode;
    state.member_info.flagShowMagazineMail = member.flagShowMagazineMail;
    state.member_info.flagShowDirectMail = member.flagShowDirectMail;
    state.member_info.flagShowGenre = member.flagShowGenre;
    state.member_info.password = member.password;
    state.listGenreInit = listGenre;
  },

  [types.SET_ERROR](state, errors) {
    state.errors = errors;
  },

  [types.UNSET_MEMBER](state) {
    state.member_info = {};
    state.listGenreInit = [];
  },

  [types.UNSET_ERROR](state) {
    state.errors = [];
  },

  [types.SET_MEMBER_SHIP](state, member) {
    state.member_ship = member;
  },

  [types.UNSET_MEMBER_SHIP](state) {
    state.member_ship = {};
    state.validStepConfirm = false;
    state.validStepComplete = false;
    state.action = false;
  },

  [types.UPDATE_FLAG_STEP_CONFIRM](state, data) {
    state.validStepConfirm = data;
  },

  [types.UPDATE_FLAG_STEP_COMPLETE](state, data) {
    state.validStepComplete = data;
  },

  [types.UPDATE_FLAG_ACCESS_PAGE](state, data) {
    state.validAccessPage = data;
  },

  [types.UPDATE_FLAG_ACTION](state, data) {
    state.action = data;
  },

  [types.SYNC](state, data) {
    state.member_info = data.member_info;
    state.errors = data.errors;
    state.listGenreInit = data.listGenreInit;
    state.member_ship = data.member_ship;
    state.validStepConfirm = data.validStepConfirm;
    state.validStepComplete = data.validStepComplete;
    state.validAccessPage = data.validAccessPage;
    state.action = data.action;
  }
}
