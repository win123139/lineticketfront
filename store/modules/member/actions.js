/**
 * File actions.js
 * Handler action call mutations in update member infomation
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-12
 */

import * as types from "./mutation-type";

export const setMember = ({ commit }, data) => {
  commit(types.SET_MEMBER, data);
};

export const unsetMember = ({ commit }) => {
  commit(types.UNSET_MEMBER);
};

export const setError = ({ commit }, data) => {
  commit(types.SET_ERROR, data);
};

export const unsetError = ({ commit }, data) => {
  commit(types.UNSET_ERROR);
};

export const setMemberShip = ({ commit }, data) => {
  commit(types.SET_MEMBER_SHIP, data);
};

export const unsetMemberShip = ({ commit }) => {
  commit(types.UNSET_MEMBER_SHIP);
};

export const updateStepConfirm = ({ commit }, data) => {
  commit(types.UPDATE_FLAG_STEP_CONFIRM, data);
};

export const updateStepComplete = ({ commit }, data) => {
  commit(types.UPDATE_FLAG_STEP_COMPLETE, data);
};

export const updateStepAccess = ({ commit }, data) => {
  commit(types.UPDATE_FLAG_ACCESS_PAGE, data);
};

export const updateAction = ({ commit }, data) => {
  commit(types.UPDATE_FLAG_ACTION, data);
};

export const sync = ({commit}, data) => {
  commit(types.SYNC, data)
};

export default {
  setMember,
  unsetMember,
  setError,
  unsetError,
  setMemberShip,
  unsetMemberShip,
  updateStepConfirm,
  updateStepComplete,
  updateStepAccess,
  updateAction,
  sync
};
