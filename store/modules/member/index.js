/**
 * File index.js
 * all store in member
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-12
 */

import actions from './actions';
import mutations from './mutations';
import state from './state';

export default {
  namespaced: true,
  actions,
  mutations,
  state,
}
