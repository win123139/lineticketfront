/**
 * File state.js
 * Define state member when update
 *
 * @author Rikkei.TriHNM
 * @date 2018-11-12
 */

export default {
  member_info: {},
  member_ship: {},
  action: false,
  validAccessPage: false,
  validStepConfirm: false,
  validStepComplete: false,
  listGenreInit: [],
  errors: []
}
