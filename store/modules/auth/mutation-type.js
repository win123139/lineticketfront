export const CHECK = 'CHECK';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const SET_USER = 'SET_USER';
export const SET_ADMIN = 'SET_ADMIN';
export const LOGOUT_ADMIN = 'LOGOUT_ADMIN';
export const SET_URL = 'SET_URL';
export const SET_URL_BLACKCD = 'SET_URL_BLACKCD';
export const SET_ERROR = 'SET_ERROR';
export const REMOVE_URL = 'REMOVE_URL';
export const SYNC = 'SYNC';

export default {
  CHECK,
  LOGIN,
  LOGOUT,
  SET_USER,
  SET_URL,
  SET_URL_BLACKCD,
  SET_ERROR,
  REMOVE_URL,
  SET_ADMIN,
  LOGOUT_ADMIN,
  SYNC
}
