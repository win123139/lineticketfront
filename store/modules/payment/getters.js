export default {
  getDelivery: state => state.delivery,
  getPaymentMethod: state => state.paymentMethod || null,
  getReceiveMethod: state => state.receiveMethod || null
}
