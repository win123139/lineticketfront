export default {
  /**
   * Object paymentMethod
   *
   * @property {string} name
   * @property {string} code
   * @property {string} settle_info
   */
  paymentMethod: null,

  /**
   * Object receiveMethod
   *
   * @property {string} name
   * @property {string} code
   * @property {string} depart_info
   */
  receiveMethod: null,

  /**
   * Object information of delivery when user select receiveMethod id delivery
   *
   * @property {string} fullName <delivery_nm>
   * @property {string} furigana <delivery_kn>
   * @property {string} noPhone <delivery_tel_no1>
   * @property {string} postCode <delivery_post_no: Post code of user address. Format XXXYYYY, x is postcode1, y is postcode 2>
   * @property {string} postCode1
   * @property {string} postCode2
   * @property {string} prefecture <delivery_prefecture: Name of prefecture>
   * @property {string} prefectureCode <Code of prefecture>
   * @property {string} province <delivery_municipality>
   * @property {string} otherAddress <delivery_address1>
   * @property {string} noRoomBuild <delivery_address2>
   */
  delivery: null
}
