import types from "../payment/mutation-type";
export default {
  /**
   *
   * @param state
   * @param deliveryInfo
   */
  [types.SET_DELIVERY_INFO](state, deliveryInfo) {
    state.delivery = deliveryInfo;
  },

  /**
   * Reset data of delivery info when not setting delivery
   * @param state
   * @param cartId
   */
  [types.RESET_DELIVERY_INFO](state) {
    state.delivery = null;
  },

  /**
   * Setting data for payment method
   * @param state
   * @param paymentMethod
   */
  [types.SET_PAYMENT_METHOD](state, paymentMethod) {
    state.paymentMethod = paymentMethod;
  },

  /**
   * Reset data for payment method
   * @param state
   */
  [types.RESET_PAYMENT_METHOD](state) {
    state.paymentMethod = null;
  },

  /**
   * Setting data for receive method
   * @param state
   * @param receiveMethod
   */
  [types.SET_RECEIVE_METHOD](state, receiveMethod) {
    state.receiveMethod = receiveMethod;
  },

  /**
   * Reset data for receive method
   * @param state
   */
  [types.RESET_RECEIVE_METHOD](state) {
    state.receiveMethod = null;
  }
}

