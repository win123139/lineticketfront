export default {
  // Set info delivery when user choose delivery ship
  SET_DELIVERY_INFO: 'SET_DELIVERY_INFO',
  // Reset info delivery to null
  RESET_DELIVERY_INFO: 'RESET_DELIVERY_INFO',
  // Set payment when user choose payment method
  SET_PAYMENT_METHOD: 'SET_PAYMENT_METHOD',
  RESET_PAYMENT_METHOD: 'RESET_PAYMENT_METHOD',
  // Set receive info when user choose receive method
  SET_RECEIVE_METHOD: 'SET_RECEIVE_METHOD',
  RESET_RECEIVE_METHOD: 'RESET_RECEIVE_METHOD'
}
