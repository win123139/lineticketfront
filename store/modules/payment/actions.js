import types from "../payment/mutation-type";
export default {
  /**
   *
   * @param commit
   * @param state
   * @param data
   */
  setPaymentInfo({commit, state}, data){
    if (data && data.payment) {
      commit(types.SET_PAYMENT_METHOD, data.payment);
    } else {
      commit(types.RESET_PAYMENT_METHOD);
    }

    if (data && data.receive) {
      commit(types.SET_RECEIVE_METHOD, data.receive);
    } else {
      commit(types.RESET_RECEIVE_METHOD);
    }

    if (data && data.delivery) {
      commit(types.SET_DELIVERY_INFO, data.delivery);
    } else {
      commit(types.RESET_DELIVERY_INFO);
    }
  },

  /**
   * Reset data for payment info
   * @param commit
   * @param state
   * @param data
   */
  resetPaymentInfo({commit, state}, data) {
    commit(types.RESET_PAYMENT_METHOD);
    commit(types.RESET_RECEIVE_METHOD);
    commit(types.RESET_DELIVERY_INFO);
  },

  /**
   *
   * @param commit
   * @param state
   */
  resetDeliveryInfo({commit, state}) {
    commit(types.RESET_DELIVERY_INFO);
  },


}
