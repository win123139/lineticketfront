/**
 * File state.js
 * Define state of show
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

import Config from '@/constant/config';

export default {
  isClickBtnSearch: false,
  searchInfo: {},
  searching: false,
  shows: [],
  refresh: false,
  totalRecord: 0,
  paginate: {
    page: 1,
    limit: Config.RECORD_PER_PAGE
  },
  cart: {
    cartId: null,
    showsInsideCart: [],
    showsCheckMethod: []
  }
}
