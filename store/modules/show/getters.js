/**
 * File getters.js
 * Define getters of show
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

export default {
  getKeySearch: (state) => state.keySearch,
  getSearchInfo: (state) => state.searchInfo,
  getResultSearch: (state) => state.resultSearch,
  getSearching: (state) => state.searching,
  getTotalRecordShow: (state) => state.totalRecord
}
