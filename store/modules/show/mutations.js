/**
 * File mutations.js
 * Define action handle show
 *
 * @author Rikkei.DungLV
 * @date 2018-10-25
 */

import types from './mutation-type';
import Config from '@/constant/config';

export default {
  /**
   * Append to list show
   * @param state
   * @param shows
   */
  [types.LIST_SHOWS](state, shows) {
    state.shows = shows;
  },

  /**
   * Clear list show
   * @param state
   */
  [types.CLEAR_SHOW](state) {
    state.shows = [];
  },

  /**
   * Set status search
   * @param state
   * @param bool
   */
  [types.SEARCHING](state, bool) {
    state.searching = bool;
  },

  /**
   * Clear condition search
   * @param state
   */
  [types.RESET_SEARCH](state) {
    state.searchInfo = {};
  },

  /**
   * Add condition to search
   * @param state
   * @param data
   */
  [types.ADD_SEARCH_FORM](state, data) {
    // When user input key search
    if (data.key_search) {
      state.searchInfo.key_search = data.key_search;
    } else {
      if (state.searchInfo.hasOwnProperty('key_search')) {
        delete state.searchInfo['key_search'];
      }
    }
    // When user select genre of show
    if (data.genre_no) {
      state.searchInfo.genre_no = data.genre_no;
    } else {
      if (state.searchInfo.hasOwnProperty('genre_no')) {
        delete state.searchInfo['genre_no'];
      }
    }

    if (data.from_show_date) {
      state.searchInfo.from_show_date = data.from_show_date;
    } else {
      if (state.searchInfo.hasOwnProperty('from_show_date')) {
        delete state.searchInfo['from_show_date'];
      }
    }

    if (data.to_show_date) {
      state.searchInfo.to_show_date = data.to_show_date;
    } else {
      if (state.searchInfo.hasOwnProperty('to_show_date')) {
        delete state.searchInfo['to_show_date'];
      }
    }

    if (data.from_sales_date) {
      state.searchInfo.from_sales_date = data.from_sales_date;
    } else {
      if (state.searchInfo.hasOwnProperty('from_sales_date')) {
        delete state.searchInfo['from_sales_date'];
      }
    }

    if (data.to_sales_date) {
      state.searchInfo.to_sales_date = data.to_sales_date;
    } else {
      if (state.searchInfo.hasOwnProperty('to_sales_date')) {
        delete state.searchInfo['to_sales_date'];
      }
    }

    if (data.start_position) {
      state.searchInfo.start_position = data.start_position;
    }

    if (data.end_position) {
      state.searchInfo.end_position = data.end_position;
    }
  },

  /**
   * Set total record
   * @param state
   * @param recordNum
   */
  [types.SET_TOTAL_RECORD](state, recordNum) {
    state.totalRecord = recordNum;
  },

  /**
   * Set is refresh page
   * @param state
   * @param isRefresh
   */
  [types.REFRESH_HOME_PAGE](state, isRefresh) {
    state.refresh = isRefresh;
  },

  /**
   * Update page show
   * @param state
   * @param paginate
   */
  [types.UPDATE_PAGE](state, paginate) {
    state.paginate.page = paginate.page || 1;
    state.paginate.limit = paginate.limit || Config.RECORD_PER_PAGE;
  },

  /**
   * CHange status of click button search
   * @param state
   * @param bool
   */
  [types.UPDATE_CLICK_SEARCH](state, bool) {
    state.isClickBtnSearch = bool;
  },

  /**
   * Setting show inside of cart
   * @param state
   * @param cart
   */
  [types.SET_SHOW_INSIDE_CART](state, cart) {
    console.log(state);
    state.cart.showsInsideCart = cart;
  },

  /**
   * Clear show inside of cart
   * @param state
   * @param cart
   */
  [types.RESET_SHOW_INSIDE_CART](state) {
    state.cart.showsInsideCart = [];
  },

  /**
   * Setting show check method of cart
   * @param state
   * @param cart
   */
  [types.SET_SHOW_CHECK_METHOD](state, show) {
    state.cart.showsCheckMethod = show;
  },

  /**
   * Clear show check method of cart
   * @param state
   * @param cart
   */
  [types.RESET_SHOW_CHECK_METHOD](state) {
    state.cart.showsCheckMethod = [];
  },

  /**
   * Setting cart id
   * @param state
   * @param cartId
   */
  [types.SET_CART_ID](state, cartId) {
    state.cart.cartId = cartId;
  },

  /**
   * Clear value current cart id
   * @param state
   */
  [types.RESET_CART_ID](state) {
    state.cart.cartId = null;
  }
}
