import * as types from './mutation-type'
import axios from 'axios'
import {SYNC} from "./mutation-type";

export const setPosts = ({commit}, posts) => {
  commit(types.SET_TICKET, posts)
}
/**
 * Add ticket to bookingTickets
 * @param commit
 * @param addTicket
 * @returns {boolean}
 */
export const addTicket = ({commit}, addTicket) => {
  commit(types.ADD_TICKET, addTicket);
  return true;
}

/**
 * choose ticket to bookingTickets
 * @param commit
 * @param addTicket
 * @returns {boolean}
 */
export const chooseTicketType = ({commit}, chooseTicket) => {
  commit(types.CHOOSE_TICKET_TYPE, chooseTicket);
  return true;
}


/**
 * delete ticket in bookingTickets
 * @param commit
 * @param delTicket
 */
export const deleteTicket = ({commit}, delTicket) => {
  commit(types.DELETE_TICKET, delTicket)
}
/**
 * delete ticket designated in bookingTickets
 * @param commit
 * @param delTicket
 */
export const deleteTicketDesignated = ({commit}, delTicket) => {
  commit(types.DELETE_TICKET_DESIGNATED, delTicket)
}
/**
 * set cart id
 * @param commit
 * @param cartId
 */
export const setCart = ({commit}, cartId) => {
  commit(types.SET_CART, cartId)
}

/**
 * clear all state booking
 * @param commit
 * @param cartId
 */
export const clearBooking = ({commit}) => {
  commit(types.CLEAR_BOOKING)
}

/**
 * save cart detail from database
 * @param commit
 * @param cartData
 */
export const setCartDetail = ({commit}, cartData) => {
  commit(types.SET_CART_DETAIL, cartData)
}

/**
 * sync state when change tabs
 * @param commit
 * @param cartData
 */
export const sync = ({commit}, booking) => {
  commit(types.SYNC, booking)
}

/**
 * clear all state booking, cart id, cart detail
 * @param commit
 */
export const clearBookingAll = ({commit}) => {
  commit(types.CLEAR_BOOKING_ALL)
}

/**
 * set all information for payment
 * @param commit
 */
export const setInfoPayment = ({commit}, infoPayment) => {
  commit(types.SET_INFO_PAYMENT, infoPayment)
}

/**
 * reset all information of payment
 * @param commit
 */
export const resetInfoPayment = ({commit}) => {
  commit(types.RESET_INFO_PAYMENT)
}

export default {
  setPosts,
  addTicket,
  deleteTicket,
  deleteTicketDesignated,
  chooseTicketType,
  setCart,
  setCartDetail,
  clearBooking,
  sync,
  clearBookingAll,
  setInfoPayment,
  resetInfoPayment

}
