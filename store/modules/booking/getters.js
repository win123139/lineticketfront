import state from './state'

export default {
  myTickets: (state) => {
    return state.bookingTickets;
  },
  cartId: (state) => {
    return state.cartId;
  },
  cartDetail: (state) => {
    return state.cartDetail;
  },
  reserveNo: (state) => {
    return state.reserveNo;
  },
  urikakeNo: (state) => {
    return state.urikakeNo;
  },
  urikakeType: (state) => {
    return state.urikakeType;
  },
  departEndDtime: (state) => {
    return state.departEndDtime;
  },
  limitPaymentDays: (state) => {
    return state.limitPaymentDays;
  }

}